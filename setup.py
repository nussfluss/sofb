#!/usr/bin/env python3
from setuptools import setup, find_packages

setup(name='sofb',
      description='Slow Orbit Feedback',
      long_description = 'Program to correct the closed orbit of the storage ring at DELTA.',
      version='1.17.0',
      license='MIT',
      author='Stephan Koetter',
      author_email='stephan.koetter@tu-dortmund.de',
      packages = find_packages('src'),
      package_dir = { '': 'src' },
      entry_points= {'console_scripts': ['sofb = sofb.__main__:main']},
      install_requires=['numpy',  # install only works with pip
                        'scipy',
                        'matplotlib',
                        'cvxopt',  # install only works with pip, might require pip
                        'channel_access.server',
                        'ca_client'])

# further requirements:
# ---------------------
# 1. copy persistence/sofb.persistence to /var/lib/sofb
# 2. copy configs/corr_info.csv to /etc/sofb/corr_info.csv
# 3. copy configs/bpm_info.csv to /etc/sofb/bpm_info.csv
# 4. copy configs/sofb.confg to /etc/sofb/sofb.confg
