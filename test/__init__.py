import unittest as ut

#from . import correctors
#from . import R
#from .sim_epics import test as sim_epics
from . import chi
#from . import i2phi
#from . import hw

#from . import test_meas_suite
#from . import test_ml
#from . import sofb_controller


def add_hw(suite):
    suite.addTest(correctors.TestCorrectorsWithSim('test_I_with_sim'))


def add_R(suite):
    suite.addTest(R.MeasDictRRaw('test_meas_dict_R_raw'))
    suite.addTest(R.ConvertDictRRawToRFK('test_convert_dict_R_raw_to_R_FK'))


def main(verb=0):
    #suite = ut.TestSuite()
    #add_hw(suite)
    #add_R(suite)
    #test_ml.add_tests_to_suite(suite)
    #test_meas_suite.add_tests_to_suite(suite)
    #sofb_controller.add_tests_to_suite(suite)
    #ut.TextTestRunner(verbosity=verb).run(suite)
    chi.test()
    #sim_epics.test()
    #i2phi.test()
    #hw.test()
