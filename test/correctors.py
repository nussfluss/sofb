import numpy as np
from timeit import default_timer
from os import path
import logging

from ...hw import config
from ...CONST import *
from ..base import HWBase, HWSeverity
from ... import i2phi


class Steerer(HWBase):

    """
    wraps get, set, comp and and quadrupole current records for a single Delta steerer
    """

    def __init__(self, idstr, plane, use, quad_current_src, mount_type, sextupole_type, prefix='SK-'):
        """
        Parameters:
        -----------
        idstr : string
            String Following the Convention 'XkXX'. The first X is either a 'h' or 'v ' for horizontal or vertical
            correctors. The latter 'XX' is a numer in between '01' and '30' for horizontal correctors or '01' and '26'
            for vertical correctors.
        plane : string
            either 'x' or 'y'
        use : bool
        current_src : str
            epics record name of current source powering quadrupole this steerer is mounted on
        mount_type: str
            large yoke or small yoke
        sextupole_type: str
            integrated sextupole or not
        """

        self.idstr = idstr
        self.plane = plane
        self.use = use
        self.current_src = quad_current_src
        self.mount_type = mount_type
        self.sextupole_type = sextupole_type

        super(Steerer, self).__init__(prefix=prefix, use=use)

        self._cpv_I_get = self.add_cpv(prefix, '{}-i'.format(idstr))
        self._cpv_I_get._pv.MDEL = 0.005
        self._cpv_I_put = self.add_cpv(prefix, '{}-i:set'.format(idstr))
        self._cpv_I_quad = self.add_cpv(prefix, quad_current_src[3:])
        self._cpv_I_comp = self.add_cpv(prefix, '{}-i:comp'.format(idstr))

    # -------------------------------------------------------------------------------------------------------------
    #                                                  properties
    # -------------------------------------------------------------------------------------------------------------

    @property
    def I(self):
        value, severity = self._cpv_I_get.get()
        return value, severity

    @I.setter
    def I(self, value):
        self._cpv_I_put.put(value)

    @property
    def I_ctrl_lims(self):
        self._cpv_I_put.update_metadata()
        return self._cpv_I_put.ctrl_lims

    @property
    def I_quad(self):
        value, severity = self._cpv_I_quad.get()
        return value, severity

    # -------------------------------------------------------------------------------------------------------------
    #                                                public functions
    # -------------------------------------------------------------------------------------------------------------

    def I2phi(self, I, beam_energy):
        return i2phi.I2strength(mount_type=self.mount_type, sextupole_type=self.sextupole_type,
                                plane=self.plane, I=I, I_quad=self.I_quad, beam_energy=beam_energy,
                                integrated_flag=True)

    def phi2I(self, phi, beam_energy):
        return i2phi.strength2I(mount_type=self.mount_type, sextupole_type=self.sextupole_type,
                                plane=self.plane, strength=phi, I_quad=self.I_quad, beam_energy=beam_energy,
                                integrated_flag=True)


class Correctors(HWBase):
    """

    Attributes:
    -----------
    corrs: list
        list of SOFBCorr instances
    K: int
        number of correctors
    Kh: int
        number of vertical correctors
    Kv: int
        number of horizontal correctors
    ready: bool
        flagged by self.check() if self.ready_trigger. Only if flagged new currents can be set.
    ready_trigger: bool
        flagged by callback if ramping has commenced but correctors are still blocked
    """
    def __init__(self, prefix='SK-', config_path='config/'):
        super(Correctors, self).__init__(prefix=prefix)

        self.ar_cor = config.ar_cor_from_list(config.read_csv(path.join(config_path, 'corr_info.csv'), tup=True))
        self.ready = False
        self._ready_trigger = False

        self.corrs = []
        self.K = self.ar_cor.shape[0]
        self.Kh = 0
        self.Kv = 1
        for k in range(self.K):
            if self.ar_cor[k]['type'] == 'std':
                self.corrs.append(self.add_instance(Steerer(idstr=self.ar_cor[k]['idstr'],
                                                            plane=self.ar_cor[k]['plane'],
                                                            use=self.ar_cor[k]['use'],
                                                            quad_current_src=self.ar_cor[k]['current_src'],
                                                            mount_type=self.ar_cor[k]['mount_type'],
                                                            sextupole_type=self.ar_cor[k]['sextupole_type'],
                                                            prefix=prefix)))
                if self.ar_cor[k]['plane'] == 'x':
                    self.Kh += 1
                else:
                    self.Kv += 1

        # ramp
        self._cpv_ramp_start = self.add_cpv(prefix + 'steererramp:start')  # start ramp by putting 1
        self._cpv_ramp_t = self.add_cpv(prefix + 'steererramp-t:set')  # set ramp mode and block by putting -1, release blocking by putting 0, block only by putting 1
        self._cpv_ramp_stat = self.add_cpv(prefix + 'steererramp-stat')  # ramp status: RUNNING 1, STOPPED 0

    # -------------------------------------------------------------------------------------------------------------
    #                                                  properties
    # -------------------------------------------------------------------------------------------------------------

    @property
    def I_bounds_K2(self):
        return np.asarray([self.corrs[k].I_ctrl_lims for k in range(K)], np.float64)

    @property
    def map_Keff(self):
        """
        Returns:
        --------
        map : float (K)-ndarray
            subset of arange(K) which only includes correctors with a severity = HWSeverity.NO_ALARM
        """
        map_Keff = []
        for k in range(K):
            if self.corrs[k].severity == HWSeverity.NO_ALARM:
                map_Keff.append(k)
        return np.asarray(map_Keff, int)

    @property
    def map_Kheff(self):
        map_Kheff = []
        for k in range(Kh):
            if self.corrs[k].severity == HWSeverity.NO_ALARM:
                map_Kheff.append(k)
        return np.asarray(map_Kheff, int)

    @property
    def map_Kveff(self):
        map_Kveff = []
        for k in range(Kh, K):
            if self.corrs[k].severity == HWSeverity.NO_ALARM:
                map_Kveff.append(k)
        return np.asarray(map_Kveff, int)

    @property
    def I_K(self):
        """
        Returns:
        --------
        I_K : float (K)-ndarray
            corrector currents
        severity_K: (K)-tuple
            severities
        """
        I_K = np.zeros(K, float)
        severity_K = []
        for k in range(K):
            I, severity = self.corrs[k].I
            I_K[k] = I
            severity_K.append(severity)
        return I_K, tuple(severity_K)

    @property
    def use_K(self):
        use_K = np.zeros(K, int)
        for k in range(K):
            use_K[k] = self.corrs[k].use
        return use_K

    @use_K.setter
    def use_K(self, values):
        for k in range(K):
            self.corrs[k].use = values[k]

    @property
    def I_quad_K(self):
        I_quad_K = np.zeros(K, np.float64)
        for k in range(K):
            I_quad_K[k] = self.corrs[k].I_quad
        return I_quad_K

    # -------------------------------------------------------------------------------------------------------------
    #                                                 public methods
    # -------------------------------------------------------------------------------------------------------------

    def set_currents(self, Delta_I_K, threshold=0.02):
        """
        1. invoke ramp mode and block correctors
        2. add Delta_I_K to the set current of all current sources if threshold is exceeded
        3. start ramp

        Parameters:
        -----------
        I : float (K)-ndarray
            Vector of Corrector Currents

        Returns:
        --------
        bool: flagged if corrector currents were successfully set
        """
        if self.ready:
            self._cpv_ramp_t.put(-1)  # sets ramp mode and locks steerers
            for k in range(K):
                if abs(Delta_I_K[k]) > threshold:
                    self.corrs[k].I += Delta_I_K[k]
            self._cpv_ramp_start.put(1)  # starts ramping
            self.T_set = default_timer()
            self.ready = False
            return True
        else:
            return False

    def check(self):
        """
        has to be called from the outside because put() cannot be invoked in a callback
        """
        if not self.ready and self._cpv_ramp_stat.value == 0:
            self._cpv_ramp_t.put(0)  # unblocks correctors
            self.ready = True
            self.ready_trigger = False
            logging.info('Correctors: finished ramping')

    def I_K_2_phi_K(self, I_K, beam_energy):
        phi_K = np.zeros(K, np.float64)
        for k in range(K):
            phi_K[k] = self.corrs[k].I2phi(I=I_K[k], beam_energy=beam_energy)
        return phi_K

    def phi_K_2_I_K(self, phi_K, beam_energy):
        I_K = np.zeros(K, np.float64)
        for k in range(K):
            I_K[k] = self.corrs[k].phi2I(phi=phi_K[k], beam_energy=beam_energy)
        return I_K


