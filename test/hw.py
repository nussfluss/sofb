import unittest as ut
import numpy as np

from .. import hw
from ..hw import set
from ..CONST import *
from .i2phi import i2k2py


class HW(ut.TestCase):
    def setUp(self):
        self._hw = hw.HW(verb=4)

    def test_I_K_2_phi_K(self):
        l =set.read_csv('sofb/hw/set/corr_info.csv', tup=True)
        arr_cor = set.ar_cor_from_list(l)
        I_K = self._hw.corrs.I_K
        phi_K = self._hw.I_K_2_phi_K(I_K)
        print(arr_cor)
        for k in range(K):
            id_str = arr_cor[k]['idstr']
            phi_from_I2k = i2k2py.I2phi(id_str, I_K[k], self._hw.corrs[k].I_quad, beam_energy=self._hw.beam_energy, verb=0)
            #print(I_K[k])
            #print(self._hw.corrs[k].I_quad)
            #print(self._hw.corrs[k].idstr)
            #print(self._hw.corrs[k].plane)
            #print(self._hw.beam_energy)
            print(k, phi_K[k], phi_from_I2k, phi_K[k]-phi_from_I2k)
            self.assertAlmostEqual(phi_K[k], phi_from_I2k, places=5, msg=id_str)
        I_out_K = self._hw.phi_K_2_I_K(phi_K)
        self.assertAlmostEqual(np.sum(abs(I_K-I_out_K)), 0.0, places=5)


def test(verb=0):
    print('\n\ntesting package hw:')
    print('-------------------\n')

    print("testing HW.I_K_2_phi_K() and HW.phi_K_2_I_K() from package hw ... \n")
    suite = ut.TestSuite()
    suite.addTest(HW('test_I_K_2_phi_K'))
    ut.TextTestRunner(verbosity=verb).run(suite)

