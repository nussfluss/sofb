import numpy as np

J = 54
F = 108
K = 56
Kh = 30
Kv = 26

# reference orbit
kap_ref_zeros_F = np.zeros(F, dtype=np.float64)
kap_ref_arange_F = np.arange(F, dtype=np.float64)

# corrector currents
I_zeros_K = np.zeros(K, dtype=np.float64)
I_arange_K = np.arange(K, dtype=np.float64)
I_random10_K = np.asarray(np.random.randint(-10, 10, K), dtype=np.float64)

I_bounds_10_K2 = 10*np.asarray([-1*np.ones(K), np.ones(K)], dtype=np.float64)
I_bounds_10_K2 = np.swapaxes(I_bounds_10_K2, axis1=0, axis2=1)

W_eye_FF = np.eye(F, dtype=np.float64)

R_eye_FK = np.zeros((F, K), dtype=np.float64)

np.fill_diagonal(R_eye_FK, 1.0)
