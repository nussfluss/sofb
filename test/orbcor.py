import unittest as ut
import numpy as np
from .. import hw


class TestOrbCor(ut.TestCase):

    def test_orbit_correction_step(self):
        """
        Tests 
            ml.orbcor.orbit_correction_step() 
        """
        M = 2
        F = 108
        K_h = 30
        K_v = 26
        K = K_h + K_v

        R_A_FK = np.zeros((F, K), np.float64)  # A = Ampere
        np.fill_diagonal(R_A_FK, 1.0)
        I_cor_K = np.ones(K, np.float64)
        kap_1_F = np.zeros(F, np.float64)
        kap_1_F[:K] = np.ones(K, np.float64)
        mu_M = np.ones(M)
        kap_ref_F = np.zeros(F, np.float64)
        W_FF = np.eye(F, dtype=np.float64)

        # norm = 'A'
        ring = hw.sim_epics.sim
        ring.R_FK = R_A_FK
        ring.kap_1_F = kap_1_F
        ring.I_cor_K = I_cor_K
        ring.mu_M[:] = mu_M
        hw_ = hw.HW(path_to_corr_info='sofb/debug/set/corr_info_debug.csv',
                    path_to_bpm_info='sofb/debug/set/bpm_info_debug.csv',
                    debug_flag=True,
                    verb=0)
        results = ml.orbcor.orbit_correction_step(hw=hw_,
                                                  kap_ref_F=kap_ref_F,
                                                  R_FK=R_A_FK,
                                                  W_FF=W_FF,
                                                  norm='A',
                                                  rcond=0.0,
                                                  sigmas_to_remove=0,
                                                  correction_in_planes='xy',
                                                  decoupled_flag=False,
                                                  factor=1,
                                                  t_sleep=0.0,
                                                  confirmation_flag=False,
                                                  collect_results_flag=True,
                                                  verb=-1)
        kap_F_should = kap_ref_F
        kap_F_is = results['kap_fin_F']
        self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=4)

        # norm = 'rad'
        R_rad_FK = 10 * R_A_FK
        ring = hw.sim_epics.sim
        ring.R_FK = R_rad_FK
        ring.kap_1_F = kap_1_F
        ring.I_cor_K = I_cor_K
        ring.mu_M[:] = mu_M
        hw_ = hw.HW(path_to_corr_info='sofb/debug/set/corr_info_debug.csv',
                    path_to_bpm_info='sofb/debug/set/bpm_info_debug.csv',
                    debug_flag=True,
                    verb=0)
        results = ml.orbcor.orbit_correction_step(hw=hw_,
                                                  kap_ref_F=kap_ref_F,
                                                  R_FK=R_A_FK,
                                                  W_FF=W_FF,
                                                  norm='rad',
                                                  rcond=0.0,
                                                  sigmas_to_remove=0,
                                                  correction_in_planes='xy',
                                                  decoupled_flag=False,
                                                  factor=1,
                                                  t_sleep=0.0,
                                                  confirmation_flag=False,
                                                  collect_results_flag=True,
                                                  verb=-1)
        kap_F_should = kap_ref_F
        kap_F_is = results['kap_fin_F']
        self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=4)


class Minimize(ut.TestCase):

    def test_minimize_chi_sq_within_bounds_simple(self):
        """
        Tests if
            ml/orbcor/chi.minimize_chi_sq_within_bounds()
        returns an estimated orbit
            kap_est_F
        which matches the reference orbit
            kap_ref_F.
        All involved quantities are predefined.
        """
        verb = 0

        F = 108
        K = 56

        kap_ref_F = np.zeros(F, dtype=np.float64)
        I_K = np.zeros(K, dtype=np.float64)
        Delta_I_0_K = None
        I_bounds_K2 = 10*np.asarray([-1*np.ones(K), np.ones(K)], dtype=np.float64)
        I_bounds_K2 = np.swapaxes(I_bounds_K2, axis1=0, axis2=1)
        W_FF = np.eye(F, dtype=np.float64)
        R_FK = np.zeros((F, K), dtype=np.float64)
        np.fill_diagonal(R_FK, 1.0)

        kap_F = np.ones(F, dtype=np.float64)
        kap_F[K:] = np.zeros(F-K)
        results = chi.minimize_chi_sq_within_bounds(kap_F=kap_F,
                                                    kap_ref_F=kap_ref_F,
                                                    I_K=I_K,
                                                    Delta_I_0_K=Delta_I_0_K,
                                                    I_bounds_K2=I_bounds_K2,
                                                    R_FK=R_FK,
                                                    W_FF=W_FF,
                                                    rcond=0.0,
                                                    maxfun=10e6,
                                                    maxiter=100,
                                                    callback=None,
                                                    verb=verb)
        kap_F_is = results['kap_est_F']
        kap_F_should = kap_ref_F
        self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=2)

    def test_minimize_chi_sq_within_bounds_random(self):
        """
        Tests if
            ml/orbcor/chi.minimize_chi_sq_within_bounds()
        returns an estimated orbit
            kap_est_F
        which matches the reference orbit
            kap_ref_F.
        All involved quantities except W_FF are random.
        """
        verb = 0

        F = 100
        K = 100

        kap_ref_F = np.zeros(F, dtype=float)
        I_K = np.asarray(np.random.random_integers(low=-9, high=9, size=K), dtype=np.float64)
        Delta_I_0_K = None
        I_bounds_K2 = 20*np.asarray([-1*np.ones(K), np.ones(K)], dtype=np.float64)
        I_bounds_K2 = np.swapaxes(I_bounds_K2, axis1=0, axis2=1)
        W_FF = np.eye(F, dtype=np.float64)
        R_FK = np.zeros((F, K))
        for f in range(F):
            if f == K:
                break
            R_FK[f, f] = 100*np.random.rand()
        R_FK = np.asarray(R_FK, dtype=np.float64)

        for n in range(10):
            kap_F = np.random.rand(F)
            results = chi.minimize_chi_sq_within_bounds(kap_F=kap_F,
                                                        kap_ref_F=kap_ref_F,
                                                        I_K=I_K,
                                                        Delta_I_0_K=Delta_I_0_K,
                                                        I_bounds_K2=I_bounds_K2,
                                                        R_FK=R_FK,
                                                        W_FF=W_FF,
                                                        rcond=0.0,
                                                        maxfun=10e6,
                                                        maxiter=100,
                                                        callback=None,
                                                        verb=verb)
            kap_F_is = results['kap_est_F']
            kap_F_should = kap_ref_F
            self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=2)



def add_tests_to_suite(suite):
    suite.addTest(RingTest('test'))
    suite.addTest(PVKap('test_get'))
    suite.addTest(PVQ('test_get'))
    suite.addTest(PVCorr('create_pvs'))
    suite.addTest(PVCorr('test_get'))
    suite.addTest(PVCorr('test_put'))


def test(verb=0):
    print('\n\ntesting sim_epics package ... \n')
    suite = ut.TestSuite()
    add_tests_to_suite(suite)
    ut.TextTestRunner(verbosity=verb).run(suite)
