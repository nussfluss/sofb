import unittest as ut

import numpy as np

from sofb import i2phi
from . import sim, PV
from ...CONST import *


class RingTest(ut.TestCase):

    def test(self):

        # test if properties are set
        R_FK = np.random.random((F, K))
        I_K = np.random.random(K)
        kap_1_F = np.random.random(F)
        kap_2_F = np.random.random(F)
        kap_ref_F = np.random.random(F)
        I_quad_K = np.random.random(K)
        norm = 'rad'
        mu_M = np.random.random(M)
        beam_energy = 500.2
        mount_types_K = ['short' for k in range(K)]
        sextupole_types_K = ['internal' for k in range(K)]

        sim.R_FK = R_FK
        sim.I_K = I_K
        sim.kap_1_F = kap_1_F
        sim.kap_2_F = kap_2_F
        sim.kap_ref_F = kap_ref_F
        sim.I_quad_K = I_quad_K
        sim.norm = norm
        sim.mu_M = mu_M
        sim.beam_energy = beam_energy
        sim.mount_types_K = mount_types_K
        sim.sextupole_types_K = sextupole_types_K

        self.assertEqual(np.sum(sim.kap_1_F-kap_1_F), 0.0)
        self.assertEqual(np.sum(sim.I_K-I_K), 0.0)
        self.assertEqual(np.sum(sim.kap_2_F-kap_2_F), 0.0)
        self.assertEqual(np.sum(sim.kap_ref_F-kap_ref_F), 0.0)
        self.assertEqual(np.sum(sim.I_quad_K-I_quad_K), 0.0)
        self.assertTrue(norm == sim.norm)
        self.assertAlmostEqual(np.sum(sim.mu_M-mu_M), 0.0)
        self.assertEqual(sim.beam_energy, beam_energy)
        self.assertSequenceEqual(sim.mount_types_K, mount_types_K)
        self.assertSequenceEqual(sim.sextupole_types_K, sextupole_types_K)

        # test update_kap_F() for norm = 'rad'
        sim.norm = 'A'
        kap_F = kap_ref_F + kap_1_F + kap_2_F + np.dot(R_FK, I_K)
        self.assertAlmostEqual(np.sum(sim.kap_F - kap_F), 0.0, places=5)

        # test update_kap_F() for norm = 'A'
        sim.norm = 'rad'
        phi_K = np.zeros(K, np.float64)
        for k in range(K):
            if k >= K_h:
                plane = 'v'
            else:
                plane = 'h'
            phi_K[k] = i2phi.I2strength(mount_type=mount_types_K[k],
                                        sextupole_type=sextupole_types_K[k],
                                        plane=plane,
                                        I=I_K[k],
                                        I_quad=I_quad_K[k],
                                        beam_energy=beam_energy,
                                        verb=0)
        self.assertAlmostEqual(np.sum(sim.phi_K - phi_K), 0.0, places=5)
        kap_F = kap_ref_F + kap_1_F + kap_2_F + np.dot(R_FK, phi_K)
        self.assertAlmostEqual(np.sum(sim.kap_F - kap_F), 0.0, places=5)

        # test for norm = 'A':
        # 1. perturb_kap_F_correctable()
        # 2. calculate correction via svd
        # 3. apply correction
        # 4. kap_F == kap_ref_F?
        sim.norm = 'A'
        sim.kap_2_F = np.zeros(F, np.float64)
        sim.perturb_kap_F_correctable(0.5)
        U_FF, S_K, V_KK = np.linalg.svd(R_FK, full_matrices=True)
        S_inv_FF = np.zeros((F, F), np.float64)
        S_inv_FF[np.diag_indices(K)] = 1.0 / S_K
        S_inv_KF = S_inv_FF[:K, :]
        A_inv = np.dot(V_KK.conj().T, np.dot(S_inv_KF, U_FF.conj().T))
        Delta_I_K = -np.dot(A_inv, (sim.kap_F - kap_ref_F))
        avg_deviation_init = np.sum(sim.kap_F - kap_ref_F)
        sim.I_K += Delta_I_K
        self.assertLess(abs(np.sum(sim.kap_F - kap_ref_F)), 0.001 * abs(avg_deviation_init))  # R_FK might have small singular values

        # test for norm = 'rad':
        # 1. perturb_kap_F_correctable()
        # 2. calculate correction via svd
        # 3. apply correction
        # 4. kap_F == kap_ref_F?
        sim.norm = 'rad'
        sim.kap_2_F = np.zeros(F, np.float64)
        sim.perturb_kap_F_correctable(0.5)
        U_FF, S_K, V_KK = np.linalg.svd(R_FK, full_matrices=True)
        S_inv_FF = np.zeros((F, F), np.float64)
        S_inv_FF[np.diag_indices(K)] = 1.0 / S_K
        S_inv_KF = S_inv_FF[:K, :]
        A_inv = np.dot(V_KK.conj().T, np.dot(S_inv_KF, U_FF.conj().T))
        Delta_phi_K = -np.dot(A_inv, (sim.kap_F - kap_ref_F))
        phi_K = sim.phi_K + Delta_phi_K
        I_K = np.zeros(K, np.float64)
        for k in range(K):
            if k >= K_h:
                plane = 'v'
            else:
                plane = 'h'
            I_K[k] = i2phi.strength2I(mount_type=mount_types_K[k],
                                      sextupole_type=sextupole_types_K[k],
                                      plane=plane,
                                      strength=phi_K[k],
                                      I_quad=I_quad_K[k],
                                      beam_energy=beam_energy,
                                      verb=0)
        avg_deviation_init = np.sum(sim.kap_F - kap_ref_F)
        sim.I_K = I_K
        self.assertLess(abs(np.sum(sim.kap_F - kap_ref_F)), 0.01 * abs(avg_deviation_init))  # R_FK might have small singular values


class SimpleRing(ut.TestCase):

    def setUp(self):

        self.R_FK = np.zeros((F, K), np.float64)
        np.fill_diagonal(self.R_FK, 1.0)
        self.I_K = np.zeros(K, np.float64)
        self.kap_ref_F = np.arange(F)
        self.kap_1_F = np.zeros(F, np.float64)
        self.kap_2_F = np.zeros(F, np.float64)

        sim.R_FK = self.R_FK
        sim.I_K = self.I_K
        sim.norm = 'A'
        sim.kap_ref_F = self.kap_ref_F
        sim.kap_1_F = self.kap_1_F
        sim.kap_2_F = self.kap_2_F


class PVKap(SimpleRing):

    def test_get(self):

        pv_kap_x = PV('de-bpm-x')
        pv_kap_y = PV('de-bpm-z')

        kap_0J_should = np.arange(F)[:J]
        kap_1J_should = np.arange(F)[J:]
        kap_0J_is     = pv_kap_x.get()
        kap_1J_is     = pv_kap_y.get()

        self.assertSequenceEqual(list(kap_0J_is), list(kap_0J_should))
        self.assertSequenceEqual(list(kap_1J_is), list(kap_1J_should))


class PVQ(SimpleRing):

    def test_get(self):
        pv_q_0 = PV('de-qana-qx')
        pv_q_1 = PV('de-qana-qy')

        sim.mu_M = 2 * np.pi*np.arange(2)

        self.assertEqual(pv_q_0.get(), 0)
        self.assertEqual(pv_q_1.get(), 1)


class PVCorr(SimpleRing):

    def create_pvs(self):

        pvs = [list(), list()]
        for k_x in range(K_h):
            pvs[0].append(PV('de-hk{:02}-i'.format(k_x + 1)))
        for k_y in range(K_v):
            pvs[1].append(PV('de-vk{:02}-i'.format(k_y + 1)))
        return pvs

    def test_get(self):

        pvs = self.create_pvs()

        k = 0
        for m in range(2):
            for pv in pvs[m]:
                sim.I_K[k] = k
                self.assertEqual(pv.get(), k)
                k += 1

    def test_put(self):

        pvs = self.create_pvs()

        k = 0
        for m in range(2):
            for pv in pvs[m]:
                sim.I_K = np.zeros(K, np.float64)
                pv.put(1.0)
                kap_F_is = sim.kap_F

                I_K = np.zeros(K, np.float64)
                I_K[k] = 1.0
                kap_F_should = self.kap_ref_F + self.kap_1_F + np.dot(self.R_FK, I_K)

                self.assertSequenceEqual(list(kap_F_is), list(kap_F_should))
                k += 1


def add_tests_to_suite(suite):
    suite.addTest(RingTest('test'))
    suite.addTest(PVKap('test_get'))
    suite.addTest(PVQ('test_get'))
    suite.addTest(PVCorr('create_pvs'))
    suite.addTest(PVCorr('test_get'))
    suite.addTest(PVCorr('test_put'))


def test(verb=0):
    print('\n\ntesting sim_epics package ... \n')
    suite = ut.TestSuite()
    add_tests_to_suite(suite)
    ut.TextTestRunner(verbosity=verb).run(suite)

