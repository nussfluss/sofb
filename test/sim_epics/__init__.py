import numpy as np

from ..ring import sim
from ...CONST import *


class _PV:
    def __init__(self, pvname):
        self.pvname = pvname

    def get(self):
        return 0


def PV(pvname):
    """
    returns the appropriate PV for pvname
    :param pvname: epics record [st
    :return: appropriate PV
    """

    if pvname.find('bpm') > -1:
        return PVKap(pvname)
    elif pvname.find('hk') > -1 or pvname.find('vk') > -1:
        return PVCorr(pvname)
    elif pvname.find('qana') > -1:
        return PVQ(pvname)
    elif pvname.find('beam') > -1:
        return _PV(pvname)
    else:
        return PVCorr(pvname)


def caget(record):
    """
    returns the appropriate PV for pvname
    :param pvname: epics record [st
    :return: appropriate PV
    """

    if record.find('bpm') > -1:
        return PVKap(record).get()
    elif record.find('hk') > -1 or record.find('vk') > -1:
        return PVCorr(record)
    elif record.find('qana') > -1:
        return PVQ(record).get()
    elif record.find('beam-i') > -1:
        return _PV(record)
    elif record.find('{}beam-energy'.format(prefix)) > -1:
        return 1.5
    elif record.find('{}q'.format(prefix)) > -1:
        return 0.0
    else:
        return None


class PVQ(object):

    def __init__(self, pvname):
        """
        Creates an interface that mimics an epics PV to the interactive, linear beam dynamics model of a storage ring
        given by class Ring.

        Parameters:
        -----------
        pvname: string
            epics record for tunes
                either 'de-qana-qx' or 'de-qana-qy'.
        """
        self.pvname = pvname
        plane = pvname[-1]
        self.dim = 0
        if plane == 'y':       # its 'de-qana-qy' but 'de-bpm-z'!
            self.dim = 1

    def get(self):
        return sim.mu_M[self.dim] / (2*np.pi)


class PVKap(object):

    def __init__(self, pvname):
        """
        Creates an interface that mimics an epics PV to the interactive, linear beam dynamics model of a storage ring
        given by class Ring.

        Parameters:
        -----------
        pvname: string
            epics record for orbit deviations
                either 'de-bpm-x' or 'de-bpm-z' (Yes, its a 'z'!).
        """
        self._pvname = pvname
        plane = pvname[-1]
        self._dim = 0
        if plane == 'z':       # its 'de-qana-qy' but 'de-bpm-z'!
            self._dim = 1

    def get(self):
        if self._dim == 0:
            kap = np.copy(sim.kap_F[:J])
        else:
            kap = np.copy(sim.kap_F[J:])
        return kap


class PVCorr(object):

    def __init__(self, pvname):
        """
        Creates an interface that mimics an epics PV to the interactive, linear beam dynamics model of a storage ring
        given by class Ring.

        Parameters:
        -----------
        pvname: string
            epics record for corrector currents
                'de-[h, v]k[h: 1-30, v: 1-26]-i'      or
                'de-[h, v]k[h: 1-30, v: 1-26]-i:set'
        """
        self.pvname = pvname
        plane = pvname[3]
        num   = int(pvname[5:7])
        if plane == 'h':
            self.k = num - 1
        else:
            self.k = K_h + num - 1

    def put(self, I):
        # Ring.I_K[self.k] = I does not work!
        I_K = sim.I_K
        I_K[self.k] = I
        sim.I_K = I_K

    def get(self):
        return sim.I_K[self.k]

