import unittest as ut

import numpy as np

from sofb import R
from ..hw import HW
from ..hw.sim_epics import sim


class RandomTestRing(ut.TestCase):

    """
    Test fixture owning a instance of
        hw/sim/ring.Ring().

    Search space dimensions are defined manually.

    All other attributes are random.
    """

    def setUp(self):
        self.M = 2
        self.F = 108
        self.K_x = 30
        self.K_y = 26
        self.K = self.K_x + self.K_y

        self.R_FK = np.zeros((self.F, self.K), np.float64)
        for f in range(self.F):
            if f == self.K:
                break
            self.R_FK[f, f] = np.random.randint(low=0, high=100)
        self.I_cor_K = np.asarray(np.random.randint(low=-10, high=10, size=self.K), dtype=np.float)
        self.kap_1_F = np.asarray(np.random.randint(low=0, high=100, size=self.F), dtype=np.float64)
        self.mu_M = np.asarray(np.random.randint(low=0, high=100, size=2), dtype=np.float64)

        self.ring = sim

        self.ring.R_FK = self.R_FK
        self.ring.kap_1_F = self.kap_1_F
        self.ring.I_cor_K = self.I_cor_K
        self.ring.mu_M[:] = self.mu_M


class MeasDictRRaw(RandomTestRing):

    def test_meas_dict_R_raw(self):

        """
        Test case testing
            R.meas_dict_R_raw().
        """

        verb = 0

        K = 56
        Delta_I = 0.1
        buf_len = 60
        t_intv = 0
        t_end = 0

        hw = HW(path_to_corr_info='sofb/debug/set/corr_info_debug.csv',
                path_to_bpm_info='sofb/debug/set/bpm_info_debug.csv',
                debug_flag=True,
                verb=0)
        dict_R_raw = R.meas_dict_R_raw(hw=hw,
                                       ks=np.arange(K),
                                       Delta_I=Delta_I,
                                       t_intv=t_intv,
                                       t_end=t_end,
                                       buf_len=buf_len,
                                       verb=verb)

        for k in range(K):
            for identifier in ['pos', 'neg']:

                if identifier == 'pos':
                    I_should_should = self.I_cor_K[k] + Delta_I
                else:
                    I_should_should = self.I_cor_K[k] - Delta_I
                I_should_is = dict_R_raw[k][identifier]['I_should']
                self.assertAlmostEqual(abs(I_should_is - I_should_should), 0.0, places=2)

                for n in range(buf_len):

                    if identifier == 'pos':
                        I_is_should = self.I_cor_K[k] + Delta_I
                    else:
                        I_is_should = self.I_cor_K[k] - Delta_I
                    I_is_is = dict_R_raw[k][identifier]['I_is_K'][n]
                    self.assertAlmostEqual(abs(I_is_is - I_is_should), 0.0, places=2)

                    q_M_should = self.mu_M / (2*np.pi)
                    q_M_is = dict_R_raw[k][identifier]['q_M'][n]
                    self.assertEqual(q_M_is[0], q_M_should[0])
                    self.assertEqual(q_M_is[1], q_M_should[1])

                    I_cor_K = np.copy(self.I_cor_K)
                    if identifier == 'pos':
                        I_cor_K[k] += Delta_I
                    else:
                        I_cor_K[k] -= Delta_I
                    kap_F_should = self.kap_1_F + np.dot(self.R_FK, I_cor_K)
                    kap_F_is = dict_R_raw[k][identifier]['kap_F'][n]
                    self.assertSequenceEqual(list(kap_F_is), list(kap_F_should))


class ConvertDictRRawToRFK(RandomTestRing):

    def test_convert_dict_R_raw_to_R_FK(self):

        """
        Test case testing
            R.convert_dict_R_raw_to_R_FK().
        """
        verb = 0
        K = 56
        F = 108
        Delta_I = 0.1
        buf_len = 60
        t_intv = 0
        t_end = 0
        hw = HW(path_to_corr_info='sofb/debug/set/corr_info_debug.csv',
                path_to_bpm_info='sofb/debug/set/bpm_info_debug.csv',
                debug_flag=True,
                verb=0)
        dict_R_raw = R.meas_dict_R_raw(hw=hw,
                                       ks=np.arange(K),
                                       Delta_I=Delta_I,
                                       t_intv=t_intv,
                                       t_end=t_end,
                                       buf_len=buf_len,
                                       verb=verb)

        # norm = 'A'
        R_FK_is, _, mu_M_is, _, norm = R.convert_dict_R_raw_to_R_FK(dict_R_raw, K=K, F=F, t_orb=0, norm='A', verb=verb)
        R_FK_should = self.R_FK
        self.assertAlmostEqual(np.sum(R_FK_is - R_FK_should), 0.0, places=3)
        mu_M_should = self.mu_M
        self.assertAlmostEqual(np.sum(mu_M_is - mu_M_should), 0.0, places=5)

        # norm = 'rad'
        R_FK_is, _, mu_M_is, _, norm = R.convert_dict_R_raw_to_R_FK(dict_R_raw, K=K, F=F, t_orb=0, norm='rad', verb=verb)
        R_FK_should = 0.1 * self.R_FK
        self.assertAlmostEqual(np.sum(R_FK_is - R_FK_should), 0.0, places=3)
        mu_M_should = self.mu_M
        self.assertAlmostEqual(np.sum(mu_M_is - mu_M_should), 0.0, places=5)


if __name__ == '__main__':
    suite = ut.TestSuite()
    suite.addTest(MeasDictRRaw('test_meas_dict_R_raw'))
    ut.TextTestRunner(verbosity=2).run(suite)
