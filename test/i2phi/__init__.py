from ... import i2phi
from ...CONST import *
from ...hw import set
from . import i2k2py

def test():
    l = set.read_csv('sofb/hw/set/corr_info.csv', tup=True)
    arr_cor = set.ar_cor_from_list(l)
    for k in range(K):
        id_str = arr_cor[k]['idstr']
        mount_type = arr_cor[k]['mount_type']
        sextupole_type = arr_cor[k]['sextupole_type']
        if 'h' in id_str:
            plane = 'h'
        else:
            plane = 'v'
        for I in [5.0, -5.0]:
            for I_quad in [55.0, -55.0]:
                phi = i2phi.I2strength(mount_type, sextupole_type, plane, I, I_quad, integrated_flag=True)
                one_over_R = i2phi.I2strength(mount_type, sextupole_type, plane, I, I_quad, integrated_flag=False)
                I_from_phi = i2phi.strength2I(mount_type, sextupole_type, plane, phi, I_quad, integrated_flag=True)
                I_from_one_over_R = i2phi.strength2I(mount_type, sextupole_type, plane, one_over_R, I_quad, integrated_flag=False)
                phi_from_I2k = i2k2py.I2phi(id_str, I, I_quad, verb=0)
                print(id_str, ':')
                print('I       = {:2.2f}'.format(I))
                print('I_quad  = {:2.2f}'.format(I_quad))
                print('yoke    = {}'.format(mount_type))
                print('sext    = {}'.format(sextupole_type))
                print('phi     = {:2.7f}'.format(phi))
                print('1/R     = {:2.7f}'.format(one_over_R))
                print('I(phi)  = {:2.2f}'.format(I_from_phi))
                print('I(1/R)  = {:2.2f}'.format(I_from_one_over_R))
                print('i2k.phi = {:2.7f}\n'.format(phi_from_I2k))

                if 0.00001 <= abs(I_from_phi - I):
                    raise ValueError('I - I(phi) = {}'.format(abs(I_from_phi - I)))
                if 0.00001 <= abs(I_from_one_over_R - I):
                    raise ValueError('I - I(1/R) = {}'.format(abs(I_from_one_over_R - I)))
                if 0.00001 <= abs(phi_from_I2k - phi):
                    raise ValueError('phi - i2K.phi) = {}'.format(abs(phi_from_I2k - phi)))
