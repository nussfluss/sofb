import subprocess
import numpy as np
from ...CONST import *


def I2phi(idstr_corr, I_corr, I_quad=0.0, beam_energy=1.5, verb=3):
    """
    Determines the correction applied by a horizontal or vertical steerer in rad for a corrector current in A via the i2k program.

    i2k syntax can be looked up via i2k -help.

    i2k help does not say much about the SUPHI. I simply deleted it from the return value using split().
    ➜  i2k_python i2k +de-hk01 -10.0 1.5 70.0
       -0.0019633649635667654 SUPHI

    Parameters:
    -----------
    idstr_corr : str
        [h or v]k[01 - 30 or 01 - 26]
    I_corr : float
        corrector current in A
    I_quad : float
        quadrupole current in A
    beam_energy : float
        beam energy in GeV
    verb : int
        verbosity
            >= 3: print results
            >= 4: print command issued to i2k
    """
    I_corr = float(I_corr)
    I_quad = float(I_quad)
    beam_energy = float(beam_energy)
    if verb >= 4:
        print('phi2I: command issued to i2k:')
        print('       {} {} {} {} {}'.format('i2k', '+' + idstr_corr, str(I_corr), str(beam_energy), str(I_quad)))
    phi_corr = float(subprocess.check_output(['i2k', '+' + idstr_corr, str(I_corr), str(beam_energy), str(I_quad)]).decode('utf-8').split()[0])
    if verb >= 3:
        print('I2phi: results:')
        print('       --------')
        print('          {} @ {:.02} A'.format(idstr_corr, I_corr))
        print('          quadrupole @ {:.02} A'.format(I_quad))
        print('          beam_energy @ {:.02} GeV'.format(beam_energy))
        print('       => {} @ {:.02} rad'.format(idstr_corr, phi_corr))
    return phi_corr


def phi2I(idstr_corr, phi_corr, I_quad=0.0, beam_energy=1.5, verb=3):
    """
    Determines the corrector current in A causing an orbit deflection phi_corr via the i2k program.

    i2k syntax can be looked up via i2k -help.

    Parameters:
    -----------
    idstr_corr : str
        [h or v]k[01 - 30 or 01 - 26]
    I_corr : float
        corrector current in A
    I_quad : str
        quadrupole current in A
    beam_energy : float
        beam energy in GeV
    verb : int
        verbosity
            >= 3: print results
            >= 4: print command issued to i2k
    """
    phi_corr = float(phi_corr)
    I_quad = float(I_quad)
    beam_energy = float(beam_energy)
    if verb >= 4:
        print('phi2I: command issued to i2k:')
        print('       {} {} {} {} {}'.format('i2k', '-+' + idstr_corr, str(phi_corr), str(beam_energy), str(I_quad)))
    I_corr = float(subprocess.check_output(['i2k', '-+' + idstr_corr, str(phi_corr), str(beam_energy), str(I_quad)]).decode('utf-8').split()[0])
    if verb >= 3:
        print('phi2I: results:')
        print('       --------')
        print('          {} @ {:.02} rad'.format(idstr_corr, phi_corr))
        print('          quadrupole @ {:.02} A'.format(I_quad))
        print('          beam_energy @ {:.02} GeV'.format(beam_energy))
        print('       => {} @ {:.02} A'.format(idstr_corr, I_corr))
    return I_corr

def I_K_2_phi_K(I_K, I_quad_K, beam_energy):
    phi_K = np.zeros(K, np.float64)
    for k in range(K):
        if k < 30:
            idstr = 'hk{:02}'.format(k+1)
        else:
            idstr = 'vk{:02}'.format(k+1-30)
        phi_K[k] = I2phi(idstr, I_K[k], I_quad_K[k], beam_energy, verb=0)
    return phi_K

if __name__ == '__main__':
    # test both functions declared above
    n = 0
    diff = 0
    for I_corr_in in range(-10,11):
        for I_quad in range(0, 200, 10):
            I_corr_in = float(I_corr_in)
            phi_corr = I2phi('hk01', I_corr_in, I_quad, 1.5, verb=4)
            I_corr_out = phi2I('hk01', phi_corr, I_quad, 1.5, verb=4)
            n += 1
            diff += abs(I_corr_in - I_corr_out)
            print('n = {:003}, diff = {:.03}, I_corr_in = {:.03}, I_corr_out = {:.03}'.format(n, diff, I_corr_in, I_corr_out))

    diff /= n
    print(diff)
