import unittest as ut
import numpy as np
from .. import hw
from .. import SOFBController as SOFBController_


class SOFBController(ut.TestCase):

    def orbit_correction_step_simple(self):
        M = 2
        F = 108
        K_h = 30
        K_v = 26
        K = K_h + K_v

        R_A_FK = np.zeros((F, K), np.float64)  # A = Ampere
        np.fill_diagonal(R_A_FK, 1.0)
        I_cor_K = np.ones(K, np.float64)
        kap_1_F = np.zeros(F, np.float64)
        kap_1_F[:K] = np.ones(K, np.float64)
        mu_M = np.ones(M)
        kap_ref_F = np.zeros(F, np.float64)
        W_FF = np.eye(F, dtype=np.float64)

        # norm = 'A'
        ring = hw.sim_epics.sim
        ring.R_FK = R_A_FK
        ring.kap_1_F = kap_1_F
        ring.I_cor_K = I_cor_K
        ring.mu_M[:] = mu_M

        sofb_controller = SOFBController_(R_FK=R_A_FK,
                                          kap_ref_F=kap_ref_F,
                                          W_FF=W_FF,
                                          path_to_corr_info='sofb/debug/set/corr_info_debug.csv',
                                          path_to_bpm_info='sofb/debug/set/bpm_info_debug.csv',
                                          debug_flag=True,
                                          verb=0)
        sofb_controller.norm = 'A'
        sofb_controller.decoupled_flag = False
        sofb_controller.correction_in_planes = 'xy'
        sofb_controller.sigmas_to_remove = 0
        sofb_controller.factor = 1.0

        # test whether property works correctly
        sofb_controller.sigmas_to_remove = 56
        sofb_controller.sigmas_to_remove = 0

        sofb_controller.update_kap()
        results = sofb_controller.orbit_correction_step()

        kap_F_should = kap_ref_F
        kap_F_is = sofb_controller.kap_est_F
        self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=4)

    def orbit_correction_step_rand(self):
        M = 2
        F = 108
        K_h = 30
        K_v = 26
        K = K_h + K_v

        R_A_FK = np.zeros((F, K), np.float64)  # A = Ampere
        np.fill_diagonal(R_A_FK, 1.0)
        I_cor_K = np.ones(K, np.float64)
        kap_1_F = np.dot(R_A_FK, np.random.randint(-2, 2, K))
        mu_M = np.ones(M)
        kap_ref_F = np.zeros(F, np.float64)
        W_FF = np.eye(F, dtype=np.float64)

        # norm = 'A'
        ring = hw.sim_epics.sim
        ring.R_FK = R_A_FK
        ring.kap_1_F = kap_1_F
        ring.I_cor_K = I_cor_K
        ring.mu_M[:] = mu_M

        sofb_controller = SOFBController_(R_FK=R_A_FK,
                                          kap_ref_F=kap_ref_F,
                                          W_FF=W_FF,
                                          path_to_corr_info='sofb/debug/set/corr_info_debug.csv',
                                          path_to_bpm_info='sofb/debug/set/bpm_info_debug.csv',
                                          debug_flag=True,
                                          verb=0)
        sofb_controller.norm = 'A'
        sofb_controller.decoupled_flag = False
        sofb_controller.correction_in_planes = 'xy'
        sofb_controller.sigmas_to_remove = 0
        sofb_controller.factor = 1.0

        # test whether property works correctly
        sofb_controller.sigmas_to_remove = 56
        sofb_controller.sigmas_to_remove = 0

        sofb_controller.update_kap()
        results = sofb_controller.orbit_correction_step()

        kap_F_should = kap_ref_F
        kap_F_is = sofb_controller.kap_est_F
        self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=4)


def add_tests_to_suite(suite):
    suite.addTest(SOFBController('orbit_correction_step_simple'))
    suite.addTest(SOFBController('orbit_correction_step_rand'))
