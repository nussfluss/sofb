import unittest as ut
import numpy as np
from ..orbcor import chi
from . import PARAM_DB

J = 54
F = 108
K = 56
Kh = 30
Kv = 26

class MinimizeChiSqWithinBoundsLBFGSB(ut.TestCase):

    def test_simple(self):
        """
        Tests if
            minimize_chi_sq(method='l-bfgs-b')
        returns an optimized orbit
            kap_opt_F
        which matches the reference orbit
            kap_ref_F
        for a simple, predifined optimization problem.
        """
        verb = 0
        I_K = np.zeros(K, dtype=np.float64)
        kap_F = np.ones(F, dtype=np.float64)
        kap_F[K:] = np.zeros(F-K)

        results = chi.minimize_chi_sq(kap_F=kap_F,
                                      kap_ref_F=PARAM_DB.kap_ref_zeros_F,
                                      I_K=I_K,
                                      Delta_I_0_K=None,
                                      I_bounds_K2=PARAM_DB.I_bounds_10_K2,
                                      R_FK=PARAM_DB.R_eye_FK,
                                      W_FF=PARAM_DB.W_eye_FF,
                                      rcond=0.0,
                                      maxfun=10e6,
                                      maxiter=100,
                                      callback=None,
                                      method_name='l-bfgs-b',
                                      verb=verb)
        self.assertAlmostEqual(np.sum(results['kap_opt_F'] - PARAM_DB.kap_ref_zeros_F), 0.0, places=2)

    def test_random(self):
        """
        Tests if
            minimize_chi_sq(method='qpcone')
        returns an optimized orbit
            kap_opt_F
        which matches the reference orbit
            kap_ref_F
        for ten random optimization problems.
        """
        verb = 0

        for n in range(10):
            kap_ref_F = np.random.rand(F)
            I_K = np.asarray(np.random.random_integers(low=-9, high=9, size=K))
            R_FK = np.random.random((F, K))
            kap_F = kap_ref_F + np.dot(R_FK, I_K)

            for n in range(10):
                results = chi.minimize_chi_sq(kap_F=kap_F,
                                              kap_ref_F=kap_ref_F,
                                              I_K=I_K,
                                              Delta_I_0_K=None,
                                              I_bounds_K2=PARAM_DB.I_bounds_10_K2,
                                              R_FK=R_FK,
                                              W_FF=PARAM_DB.W_eye_FF,
                                              rcond=0.0,
                                              maxfun=10e6,
                                              maxiter=100,
                                              callback=None,
                                              method_name='l-bfgs-b',
                                              verb=verb)
                self.assertAlmostEqual(np.sum(results['kap_opt_F'] - kap_ref_F), 0.0, places=2)


class MinimizeChiSqWithinBoundsQPCONE(ut.TestCase):

    def test_simple(self):
        """
        Tests if
            minimize_chi_sq(method='qpcone')
        returns an optimized orbit
            kap_opt_F
        which matches the reference orbit
            kap_ref_F
        for a simple, predifined optimization problem.
        """
        verb = 0
        I_K = np.zeros(K, dtype=np.float64)
        kap_F = np.ones(F, dtype=np.float64)
        kap_F[K:] = np.zeros(F-K)

        results = chi.minimize_chi_sq(kap_F=kap_F,
                                      kap_ref_F=PARAM_DB.kap_ref_zeros_F,
                                      I_K=I_K,
                                      Delta_I_0_K=None,
                                      I_bounds_K2=PARAM_DB.I_bounds_10_K2,
                                      R_FK=PARAM_DB.R_eye_FK,
                                      W_FF=PARAM_DB.W_eye_FF,
                                      method_name='qpcone',
                                      verb=verb)
        self.assertAlmostEqual(np.sum(results['kap_opt_F'] - PARAM_DB.kap_ref_zeros_F), 0.0, places=0)

    def test_random(self):
        """
        Tests if
            minimize_chi_sq(method='qpcone')
        returns an optimized orbit
            kap_opt_F
        which matches the reference orbit
            kap_ref_F
        for ten random optimization problems.
        """
        verb = 0

        for n in range(10):
            kap_ref_F = np.random.rand(F)
            I_K = np.asarray(np.random.random_integers(low=-9, high=9, size=K))
            R_FK = np.random.random((F, K))
            kap_F = kap_ref_F + np.dot(R_FK, I_K)

            for n in range(10):
                results = chi.minimize_chi_sq(kap_F=kap_F,
                                              kap_ref_F=kap_ref_F,
                                              I_K=I_K,
                                              Delta_I_0_K=None,
                                              I_bounds_K2=PARAM_DB.I_bounds_10_K2,
                                              R_FK=R_FK,
                                              W_FF=PARAM_DB.W_eye_FF,
                                              rcond=0.0,
                                              maxfun=10e6,
                                              maxiter=100,
                                              callback=None,
                                              method_name='qpcone',
                                              verb=verb)
                self.assertAlmostEqual(np.sum(abs(results['kap_opt_F'] - kap_ref_F)), 0.0, places=2)

    def test_constraints(self):
        """
        Tests if
            minimize_chi_sq(method='qpcone')
        returns an optimized orbit
            kap_opt_F
        which matches the reference orbit
            kap_ref_F
        and restricts the orbit to orbit constraints 
            constraint_F
        for a 10 random optimization problems.
        """
        verb = 0
        F = 40
        K = 40
        I_K = np.zeros(K, dtype=np.float64)
        R_FK = np.eye(F, dtype=np.float64)

        for a in range(10):
            kap_ref_F = np.asarray(np.random.randint(-2, 3, F), np.float64)
            kap_F = np.asarray(np.random.randint(-2, 3, F), np.float64)
            kap_should_F = np.zeros(F, np.float64)
            constraint_F2 = np.zeros((F, 2), np.float64)
            for f in range(F):
                i = np.random.random(1)[0]
                if i < 0.4:
                    constraint_F2[f, :] = -np.inf, np.inf
                    kap_should_F[f] = kap_ref_F[f]
                elif i > 0.9:
                    random = np.random.randint(-1, 2, 1)
                    kap_should_F[f] = random
                    constraint_F2[f] = random, random
                else:
                    random_2 = np.asarray([np.random.randint(-2, 0, 1)[0], np.random.randint(0, 3, 1)[0]])
                    constraint_F2[f] = random_2
                    if kap_ref_F[f] <= random_2[0]:
                        kap_should_F[f] = random_2[0]
                    elif kap_ref_F[f] >= random_2[1]:
                        kap_should_F[f] = random_2[1]
                    else:
                        kap_should_F[f] = kap_ref_F[f]

            W_FF = np.eye(F, dtype=np.float64)
            I_bounds_K2 = np.asarray([(-10, 10) for k in range(K)], np.float64)

            results = chi.minimize_chi_sq(kap_F=kap_F,
                                          kap_ref_F=kap_ref_F,
                                          I_K=I_K,
                                          I_bounds_K2=I_bounds_K2,
                                          R_FK=R_FK,
                                          W_FF=W_FF,
                                          constraint_F2=constraint_F2,
                                          method_name='qpcone',
                                          verb=verb)

            if False:
                import matplotlib.pyplot as plt
                plt.plot(kap_should_F, '*', color='b', label='kap_should_F')
                plt.fill_between(np.arange(F), constraint_F2[:, 0], -10, color=(0.5, 0.5, 0.5), label='aperture')
                plt.fill_between(np.arange(F), constraint_F2[:, 1], 10, color=(0.5, 0.5, 0.5))
                plt.plot(constraint_F2[:, 0], color=(0.2, 0.2, 0.2))
                plt.plot(constraint_F2[:, 1], color=(0.2, 0.2, 0.2))
                plt.plot(kap_ref_F, '--', color='y', label='kap_ref_F')
                plt.plot(kap_F, color='r', label='kap_F')
                plt.plot(results['kap_opt_F'], '--', color='g', label='kap_opt_F')
                plt.ylim((-2.1, 2.1))
                plt.legend()
                plt.show()

            self.assertAlmostEqual(np.sum(abs(results['kap_opt_F'] - kap_should_F)), 0.0, places=0)


def test(verb=0):
    print('\n\ntesting module orbcor.chi.py:')
    print('-------------------------------\n')

    print("testing chi.minimize_chi_sq(method='l-bfgs-b') ... \n")
    suite = ut.TestSuite()
    suite.addTest(MinimizeChiSqWithinBoundsLBFGSB('test_simple'))
    suite.addTest(MinimizeChiSqWithinBoundsLBFGSB('test_random'))
    ut.TextTestRunner(verbosity=verb).run(suite)

    print('\ntesting chi.minimize_chi_sq(method_name="qpcone") ... \n')
    suite = ut.TestSuite()
    suite.addTest(MinimizeChiSqWithinBoundsQPCONE('test_simple'))
    suite.addTest(MinimizeChiSqWithinBoundsQPCONE('test_random'))
    suite.addTest(MinimizeChiSqWithinBoundsQPCONE('test_constraints'))
    ut.TextTestRunner(verbosity=verb).run(suite)

