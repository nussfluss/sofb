import unittest as ut

import numpy as np

from sofb.orbcor import chi
from .. import hw
from .. import ml
from ..ml import be_d_model


class TestBEDModel(ut.TestCase):

    def setUp(self):
        self.M = 1
        self.F = 5
        self.K = 5
        self.mu_M = np.asarray([np.pi], dtype=np.float64)
        self.S_FK = np.ones((self.F, self.K), dtype=np.float64)
        for k in range(self.K):
            f = k
            self.S_FK[f, :k] *= -1

    def test_S_FK(self):
        cor_pos = np.zeros(self.K)
        bpm_pos = np.ones(self.F)
        S_FK_should = np.ones((self.F, self.K), dtype=np.float64)
        S_FK_is = be_d_model.S_FK(bpm_pos=bpm_pos, cor_pos=cor_pos)
        self.assertEqual(np.sum(S_FK_is - S_FK_should), 0.0)

    def test_E_MFK(self):
        E_MFK_is = be_d_model.E_MFK(mu_M=self.mu_M, S_FK=self.S_FK)
        E_MFK_should = 1.j*np.copy(self.S_FK)
        self.assertAlmostEqual(abs(np.sum(E_MFK_is - E_MFK_should)), 0.0, places=10)

    def test_R_FK_simple(self):
        Z_MF = np.ones((self.M, self.F), dtype=np.complex128) + 1.j*np.ones((self.M, self.F), dtype=np.complex128)
        A_MK = np.ones((self.M, self.K), dtype=np.complex128) - 1.j*np.ones((self.M, self.K), dtype=np.complex128)
        R_FK_should = np.zeros((self.F, self.K), dtype=np.float64)
        R_FK_is = be_d_model.R_FK(Z_MF=Z_MF, A_MK=A_MK, mu_M=self.mu_M, S_FK=self.S_FK)
        self.assertAlmostEqual(np.sum(R_FK_is - R_FK_should), 0.0, places=10)

    def test_R_FK_with_modes_and_dispersion(self):
        M = 3
        Z_MF = np.ones((M, self.F), dtype=np.complex128) + 1.j*np.ones((M, self.F), dtype=np.complex128)
        A_MK = np.ones((M, self.K), dtype=np.complex128) - 1.j*np.ones((M, self.K), dtype=np.complex128)
        d_F = np.ones((self.F), dtype=np.float64)
        b_K = np.ones((self.K), dtype=np.float64)
        mu_M = np.asarray([np.pi, np.pi, np.pi], dtype=np.float64)
        R_FK_should = np.ones((self.F, self.K), dtype=np.float64)  # = np.outer(d_F, b_K) !
        R_FK_is = be_d_model.R_FK(Z_MF=Z_MF, A_MK=A_MK, mu_M=mu_M, S_FK=self.S_FK, d_F=d_F, b_K=b_K)
        self.assertAlmostEqual(np.sum(R_FK_is - R_FK_should), 0.0, places=10)


class TestOrbCor(ut.TestCase):

    def test_orbit_correction_step(self):
        """
        Tests 
            ml.orbcor.orbit_correction_step() 
        """
        M = 2
        F = 108
        K_h = 30
        K_v = 26
        K = K_h + K_v

        R_A_FK = np.zeros((F, K), np.float64)  # A = Ampere
        np.fill_diagonal(R_A_FK, 1.0)
        I_cor_K = np.ones(K, np.float64)
        kap_1_F = np.zeros(F, np.float64)
        kap_1_F[:K] = np.ones(K, np.float64)
        mu_M = np.ones(M)
        kap_ref_F = np.zeros(F, np.float64)
        W_FF = np.eye(F, dtype=np.float64)

        # norm = 'A'
        ring = hw.sim_epics.sim
        ring.R_FK = R_A_FK
        ring.kap_1_F = kap_1_F
        ring.I_cor_K = I_cor_K
        ring.mu_M[:] = mu_M
        hw_ = hw.HW(path_to_corr_info='sofb/debug/set/corr_info_debug.csv',
                    path_to_bpm_info='sofb/debug/set/bpm_info_debug.csv',
                    debug_flag=True,
                    verb=0)
        results = ml.orbcor.orbit_correction_step(hw=hw_,
                                                  kap_ref_F=kap_ref_F,
                                                  R_FK=R_A_FK,
                                                  W_FF=W_FF,
                                                  norm='A',
                                                  rcond=0.0,
                                                  sigmas_to_remove=0,
                                                  correction_in_planes='xy',
                                                  decoupled_flag=False,
                                                  factor=1,
                                                  t_sleep=0.0,
                                                  confirmation_flag=False,
                                                  collect_results_flag=True,
                                                  verb=-1)
        kap_F_should = kap_ref_F
        kap_F_is = results['kap_fin_F']
        self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=4)

        # norm = 'rad'
        R_rad_FK = 10 * R_A_FK
        ring = hw.sim_epics.sim
        ring.R_FK = R_rad_FK
        ring.kap_1_F = kap_1_F
        ring.I_cor_K = I_cor_K
        ring.mu_M[:] = mu_M
        hw_ = hw.HW(path_to_corr_info='sofb/debug/set/corr_info_debug.csv',
                    path_to_bpm_info='sofb/debug/set/bpm_info_debug.csv',
                    debug_flag=True,
                    verb=0)
        results = ml.orbcor.orbit_correction_step(hw=hw_,
                                                  kap_ref_F=kap_ref_F,
                                                  R_FK=R_A_FK,
                                                  W_FF=W_FF,
                                                  norm='rad',
                                                  rcond=0.0,
                                                  sigmas_to_remove=0,
                                                  correction_in_planes='xy',
                                                  decoupled_flag=False,
                                                  factor=1,
                                                  t_sleep=0.0,
                                                  confirmation_flag=False,
                                                  collect_results_flag=True,
                                                  verb=-1)
        kap_F_should = kap_ref_F
        kap_F_is = results['kap_fin_F']
        self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=4)


class TestOrbCorChi(ut.TestCase):

    def test_minimize_chi_sq_within_bounds_simple(self):
        """
        Tests if
            ml/orbcor/chi.minimize_chi_sq_within_bounds()
        returns an estimated orbit
            kap_est_F
        which matches the reference orbit
            kap_ref_F.
        All involved quantities are predefined.
        """
        verb = 0

        F = 108
        K = 56

        kap_ref_F = np.zeros(F, dtype=np.float64)
        I_K = np.zeros(K, dtype=np.float64)
        Delta_I_0_K = None
        I_bounds_K2 = 10*np.asarray([-1*np.ones(K), np.ones(K)], dtype=np.float64)
        I_bounds_K2 = np.swapaxes(I_bounds_K2, axis1=0, axis2=1)
        W_FF = np.eye(F, dtype=np.float64)
        R_FK = np.zeros((F, K), dtype=np.float64)
        np.fill_diagonal(R_FK, 1.0)

        kap_F = np.ones(F, dtype=np.float64)
        kap_F[K:] = np.zeros(F-K)
        results = chi.minimize_chi_sq_within_bounds(kap_F=kap_F,
                                                    kap_ref_F=kap_ref_F,
                                                    I_K=I_K,
                                                    Delta_I_0_K=Delta_I_0_K,
                                                    I_bounds_K2=I_bounds_K2,
                                                    R_FK=R_FK,
                                                    W_FF=W_FF,
                                                    rcond=0.0,
                                                    maxfun=10e6,
                                                    maxiter=100,
                                                    callback=None,
                                                    verb=verb)
        kap_F_is = results['kap_est_F']
        kap_F_should = kap_ref_F
        self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=2)

    def test_minimize_chi_sq_within_bounds_random(self):
        """
        Tests if
            ml/orbcor/chi.minimize_chi_sq_within_bounds()
        returns an estimated orbit
            kap_est_F
        which matches the reference orbit
            kap_ref_F.
        All involved quantities except W_FF are random.
        """
        verb = 0

        F = 100
        K = 100

        kap_ref_F = np.zeros(F, dtype=float)
        I_K = np.asarray(np.random.random_integers(low=-9, high=9, size=K), dtype=np.float64)
        Delta_I_0_K = None
        I_bounds_K2 = 20*np.asarray([-1*np.ones(K), np.ones(K)], dtype=np.float64)
        I_bounds_K2 = np.swapaxes(I_bounds_K2, axis1=0, axis2=1)
        W_FF = np.eye(F, dtype=np.float64)
        R_FK = np.zeros((F, K))
        for f in range(F):
            if f == K:
                break
            R_FK[f, f] = 100*np.random.rand()
        R_FK = np.asarray(R_FK, dtype=np.float64)

        for n in range(10):
            kap_F = np.random.rand(F)
            results = chi.minimize_chi_sq_within_bounds(kap_F=kap_F,
                                                        kap_ref_F=kap_ref_F,
                                                        I_K=I_K,
                                                        Delta_I_0_K=Delta_I_0_K,
                                                        I_bounds_K2=I_bounds_K2,
                                                        R_FK=R_FK,
                                                        W_FF=W_FF,
                                                        rcond=0.0,
                                                        maxfun=10e6,
                                                        maxiter=100,
                                                        callback=None,
                                                        verb=verb)
            kap_F_is = results['kap_est_F']
            kap_F_should = kap_ref_F
            self.assertAlmostEqual(np.sum(kap_F_is - kap_F_should), 0.0, places=2)


def add_tests_to_suite(suite):
    suite.addTest(TestOrbCorChi('test_minimize_chi_sq_within_bounds_simple'))
    suite.addTest(TestOrbCorChi('test_minimize_chi_sq_within_bounds_random'))
    suite.addTest(TestOrbCor('test_orbit_correction_step'))
    suite.addTest(TestBEDModel('test_S_FK'))
    suite.addTest(TestBEDModel('test_E_MFK'))
    suite.addTest(TestBEDModel('test_R_FK_simple'))
    suite.addTest(TestBEDModel('test_R_FK_with_modes_and_dispersion'))
