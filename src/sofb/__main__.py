#!/home/operator/personal/koetter/python_34/bin/python3

import argparse
import logging
from logging.handlers import RotatingFileHandler
from os import path
import sys
from sofb import driver


def main():
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                                 initialize parser and subparsers
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    parser = argparse.ArgumentParser(prog='sofb', description='\n SOFB backend service', formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--verbose', '-v', action='count', help='Verbosity: add -v, -vv, or -vv to set logging level to WARNING, INFO or DEBUG.', default=0)
    parser.add_argument('--logPath', '-l', type=str, help='Path to log files.', default='/home/operator/personal/koetter/projects/sofb/logs')
    parser.add_argument('--configPath', '-c', type=str, help='Configuration directory.', default='/home/operator/personal/koetter/projects/sofb/configs/sofb.confg_local_venv')
    parser.add_argument('--debug', '-d', action='store_true', help='Enables communication with debug service by prepending "SK-" in all client-side record names (example: "SK-de-bpm01-x").', default=False)
    parser.add_argument('--unarm', '-u', action='store_true', help='Prevents any client-side records from being be written. Intended to test backend without interfering with machine operation.', default=False)
    parser.add_argument('--DR', action='store_true', help='Use "DR-" as prefix for bpm record names. All Liberas remain with "de" prefix.', default=False)
    parser.add_argument('--prefix', '-p', type=str, help='Prefix for server-side record names (example: "<prefix>-sofb-cmd"). Intended to simultaneously run two backends.', default='SK')
    parser.add_argument('--pvs', action='store_true', help='List PVs and write them to path <out_path> + "pv_list.txt".', default=False)

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                                         parse arguments
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    args = parser.parse_args(sys.argv[1:])
    kwargs = vars(args)

    verb = kwargs['verbose']
    log_path = kwargs['logPath']
    log_fpath = path.join(log_path, 'sofb.log')
    if verb == 0:
        level=logging.WARNING
    elif verb == 1:
        level=logging.INFO
    elif verb >= 2:
        level = logging.DEBUG
    if log_fpath is not None:
        handlers=[RotatingFileHandler(log_fpath, 'a', maxBytes=1024*1024, backupCount=10),
                logging.StreamHandler()]
    else:
        handlers=[logging.StreamHandler()]
    logging.basicConfig(level=level,
                        format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        handlers=handlers)
    logging.info('verbosity level is {}'.format(logging.getLevelName(logging.getLogger().level)))

    # set debug/normal mode
    # In debug mode "SK-" will be prepended to all record names
    debug_flag = kwargs['debug']
    unarm_flag = kwargs['unarm']
    dr_flag = kwargs['DR']
    record_name_prefix = kwargs['prefix']
    list_pvs_flag = kwargs['pvs']
    if debug_flag:
        logging.warning('DEBUG MODE')
    if unarm_flag:
        logging.warning('UNARM MODE')
    if dr_flag:
        logging.warning('USING DR RECORDS INSTEAD OF DE RECORDS FROM ORBIT DEVIATIONS')
    if record_name_prefix:
        if record_name_prefix is not 'SK':
            logging.warning('USING {} SERVER-SIDE PREFIX FOR RECORD NAMES'.format(record_name_prefix))

    config_fpath = kwargs['configPath']
    logging.debug('starting SOFB ...')

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                                       execute program
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    # initialize driver
    driver.main(debug_flag=debug_flag,
                dr_flag=dr_flag,
                unarm_flag=unarm_flag,
                config_fpath=config_fpath,
                record_name_prefix=record_name_prefix,
                list_pvs_flag=list_pvs_flag)

if __name__ == '__main__':
    main()
