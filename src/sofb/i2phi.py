r"""
Reverse engineered from program I2k originally authored by Marc Grewe.

All "coefficient" functions calculate the derivation

.. math:: \frac{\partial (Bl_\mathrm{eff})}{\partial I}(I_\mathrm{Q})=...

of the integrated magnetic field :math:`Bl_\mathrm{eff}` with respect to the steerer current :math:`I` for a specific quadrupole current :math:`I_\mathrm{Q}`. There are six separate functions for horizontal and vertical steerers ("h" and "v"), "long" or "short" quadrupoles and "internal" or "external" sextupoles. The results were compared to I2k output for all available steerers.
"""

import logging
import numpy as np

L_EFF_SHORT_QUAD = 0.234    # effective length of a short quad in m
L_EFF_LONG_QUAD = 0.434     # effective length of a long quad in m
c = 299792458               # speed of light in m/s


def coefficient_of_hk_on_long_quad_with_external_sext(Iq):
    Iq = abs(Iq)
    a =  0.00569237
    b = -0.00123253
    c =  0.171922
    d = 52.0779
    e = -b*c/(1+c*c*d*d)
    coefficient = (a+b*np.arctan(c*(Iq-d))+e*Iq)/5.0
    return coefficient


def coefficient_of_hk_on_long_quad_with_internal_sext(Iq):
    return 1.0465 * coefficient_of_hk_on_long_quad_with_external_sext(Iq)


def coefficient_of_hk_on_short_quad_with_external_sext(Iq):
    Iq = abs(Iq)
    a =  0.00627931
    b = -0.00070959
    c =  0.297092
    d = 46.9315
    f = -8.22494e-9
    e = -b*c/(1+c*c*d*d)
    coefficient = (a+b*np.arctan(c*(Iq-d))+e*Iq+f*Iq*Iq*Iq)/5.0
    return coefficient


def coefficient_of_hk_on_short_quad_with_internal_sext(Iq):
    return 1.0853 * coefficient_of_hk_on_short_quad_with_external_sext(Iq)


def coefficient_of_vk_on_short_quad_with_external_sext(Iq):
    Iq = abs(Iq)
    a =  0.00237445
    b = -0.000366162
    c =  0.21655
    d = 47.3607
    f = -2.39366e-07
    e = -b*c/(1+c*c*d*d)
    coefficient = (a+b*np.arctan(c*(Iq-d))+e*Iq+f*Iq*Iq)/5.0
    return coefficient


def coefficient_of_vk_on_short_quad_with_internal_sext(I):
    return 0.9669 * coefficient_of_vk_on_short_quad_with_external_sext(I)


def strength2I(mount_type, sextupole_type, plane, strength, I_quad=0.0, beam_energy=1.5, integrated_flag=True, verb=3):
    """
    converts dipole field strength into corrector current

    Attributes
    ----------
    e_over_p: float
        Result of :math:`e/p` where :math:`e` ist the elementary charge and :math:`p` is particle momentum.

    Parameters
    ----------
    mount_type: str
        Either 'long' or 'short'.
    sextupole_type: str
        Either 'internal' or 'external'.
    plane: str
        Either 'h' or 'v'.
    strenth: float
        Either :math:`1/R` or :math:`l_\mathrm{eff}/R`.
    I_quad: float, optional
        Quadrupole current.
    beam_energy: float, optional
        Beam energy in GeV.
    integrated_flag: bool, optional
        Assume integrated strength :math:`l_\mathrm{eff}/R` if True.

    Returns
    -------
    I: float
        corrector current
    """

    if integrated_flag:
        phi = strength
    else:
        if mount_type == 'long':
            phi = strength * L_EFF_LONG_QUAD
        elif mount_type == 'short':
            phi = strength * L_EFF_SHORT_QUAD

    e_over_p = c * 1e-9 / beam_energy
    B_times_l_eff = phi / e_over_p

    if mount_type == 'long':
        if plane in ['h', 'x']:
            if sextupole_type == 'external':
                coefficient = coefficient_of_hk_on_long_quad_with_external_sext(I_quad)
            elif sextupole_type == 'internal':
                coefficient = coefficient_of_hk_on_long_quad_with_internal_sext(I_quad)
    if mount_type == 'short':
        if plane in ['h', 'x']:
            if sextupole_type == 'external':
                coefficient = coefficient_of_hk_on_short_quad_with_external_sext(I_quad)
            elif sextupole_type == 'internal':
                coefficient = coefficient_of_hk_on_short_quad_with_internal_sext(I_quad)
        elif plane in ['v', 'z', 'y']:
            if sextupole_type == 'external':
                coefficient = coefficient_of_vk_on_short_quad_with_external_sext(I_quad)
            elif sextupole_type == 'internal':
                coefficient = coefficient_of_vk_on_short_quad_with_internal_sext(I_quad)

    try:
        return B_times_l_eff / coefficient  # = I
    except UnboundLocalError:
        logging.warning('strength2I failed for ', mount_type, sextupole_type, plane, strength, I_quad, beam_energy)


def I2strength(mount_type, sextupole_type, plane, I, I_quad=0.0, beam_energy=1.5, integrated_flag=True, verb=3):
    """
    converts corrector current I into dipole field strength

    Attributes
    ----------
    e_over_p: float
        Result of :math:`e/p` where :math:`e` ist the elementary charge and :math:`p` is particle momentum.

    Parameters
    ----------
    mount_type: str
        Either 'long' or 'short'.
    sextupole_type: str
        Either 'internal' or 'external'.
    plane: str
        Either 'h' or 'v'.
    I: float
        Steerer current.
    I_quad: float, optional
        Quadrupole current.
    beam_energy: float, optional
        Beam energy in GeV.
    integrated_flag: bool, optional

    Return
    ------
    strenght: float
        Return :math:`l_\mathrm{eff}/R` if integrated_flag is True or :math:`1/R`.
    """

    if verb >= 5:
        print('I2strength: {}, {}, {}, {}, {}, {}'.format(mount_type, sextupole_type, plane, I, I_quad, beam_energy))

    e_over_p = c * 1e-9 / beam_energy

    if mount_type == 'long':
        if plane in ['h', 'x']:
            if sextupole_type == 'external':
                coefficient = coefficient_of_hk_on_long_quad_with_external_sext(I_quad)
            elif sextupole_type == 'internal':
                coefficient = coefficient_of_hk_on_long_quad_with_internal_sext(I_quad)
    if mount_type == 'short':
        if plane in ['h', 'x']:
            if sextupole_type == 'external':
                coefficient = coefficient_of_hk_on_short_quad_with_external_sext(I_quad)
            elif sextupole_type == 'internal':
                coefficient = coefficient_of_hk_on_short_quad_with_internal_sext(I_quad)
        elif plane in ['v', 'z', 'y']:
            if sextupole_type == 'external':
                coefficient = coefficient_of_vk_on_short_quad_with_external_sext(I_quad)
            elif sextupole_type == 'internal':
                coefficient = coefficient_of_vk_on_short_quad_with_internal_sext(I_quad)

    B_times_l_eff = coefficient * I
    phi = B_times_l_eff * e_over_p

    if verb >= 5:
        logging.debug('I2strength: phi = {}'.format(phi))

    if integrated_flag:
        return phi
    else:
        if mount_type == 'long':
            return phi / L_EFF_LONG_QUAD
        elif mount_type == 'short':
            return phi / L_EFF_SHORT_QUAD


def I_K_2_strength_K(mount_type_K, sextupole_type_K, plane_K, I_K, I_quad_K, beam_energy, integrated_flag=True):
    """
    Wraps I2strength.

    Parameters
    ----------
    mount_type_K: list
        Either 'long' or 'short' for all steerers.
    sextupole_type_K: list
        Either 'internal' or 'external' for all steerers.
    plane_K
        Either 'h' or 'v' for all steerers.
    I_K
        Currents for all Steeres.
    I_quad_K
        Quadrupole currents for all steerers.
    beam_energy
        Beam energy in GeV
    integrated_flag: bool, optional
        Returns integrated strength if True.

    Return
    ------
    strength_K: np.float64 (K,)-np.ndarray
        Either strength or integrated strength.
    """
    return np.asarray([I2strength(mount_type=mount_type_K[k], sextupole_type=sextupole_type_K[k],
                       plane=plane_K[k], I=I_K[k], I_quad=I_quad_K[k], beam_energy=beam_energy,
                       integrated_flag=integrated_flag) for k in range(I_K.shape[0])], dtype=np.float64)


def strength_K_2_I_K(mount_type_K, sextupole_type_K, plane_K, phi_K, I_quad_K, beam_energy, integrated_flag=True):
    """
    Wraps strength2I.

    Parameters
    ----------
    mount_type_K: list
        Either 'long' or 'short' for all steerers.
    sextupole_type_K: list
        Either 'internal' or 'external' for all steerers.
    plane_K
        Either 'h' or 'v' for all steerers.
    phi_K
        Angles for all Steeres.
    I_quad_K
        Quadrupole currents for all steerers.
    beam_energy
        Beam energy in GeV
    integrated_flag: bool, optional, default=True
        Assume phi_K contains integrated strengths (angles).

    Return
    ------
    I_K: np.float64 (K,)-np.ndarray
        Steerer currents.
    """
    return np.asarray([strength2I(mount_type=mount_type_K[k], sextupole_type=sextupole_type_K[k],
                                  plane=plane_K[k], strength=phi_K[k], I_quad=I_quad_K[k], beam_energy=beam_energy,
                                  integrated_flag=integrated_flag) for k in range(phi_K.shape[0])], dtype=np.float64)


def decorate_I_K_2_strength_K(mount_type_K, sextupole_type_K, plane_K, I_quad_K, beam_energy, integrated_flag=True):
    """Create a decorator which calculates steeerer strengths from steerer currents."""
    def func(I_K):
        return I_K_2_strength_K(mount_type_K, sextupole_type_K, plane_K, I_K, I_quad_K, beam_energy, integrated_flag=integrated_flag)
    return func


def decorate_strength_K_2_I_K(mount_type_K, sextupole_type_K, plane_K, I_quad_K, beam_energy, integrated_flag=True):
    """Create a decorator which calculates steeerer currents from steerer strengths."""
    def func(strength_K):
        return strength_K_2_I_K(mount_type_K, sextupole_type_K, plane_K, strength_K, I_quad_K, beam_energy, integrated_flag=integrated_flag)
    return func


if __name__ == '__main__':
    import i2k2py
    mount_type = 'short'
    sextupole_type = 'internal'
    plane = 'h'
    I = 5.0
    I_quad = 55.0
    phi = I2strength(mount_type, sextupole_type, plane, I, I_quad, integrated_flag=True)
    one_over_R = I2strength(mount_type, sextupole_type, plane, I, I_quad, integrated_flag=False)
    print('I       = {:2.2f}'.format(I))
    print('phi     = {:2.7f}'.format(phi))
    print('1/R     = {:2.7f}'.format(one_over_R))
    print('I(phi)  = {:2.2f}'.format(strength2I(mount_type, sextupole_type, plane, phi, I_quad, integrated_flag=True)))
    print('I(1/R)  = {:2.2f}'.format(strength2I(mount_type, sextupole_type, plane, one_over_R, I_quad, integrated_flag=False)))
    print('i2k.phi = {:2.7f}'.format(i2k2py.I2phi('hk07', I, I_quad, verb=0)))
