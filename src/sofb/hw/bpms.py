import numpy as np
import threading
import logging
import copy
import sched
import time

from sofb.hw.base import HWBase, ThreadSafeProperty, Severity


PV_ORBIT_X = 'de-bpm-x'
PV_ORBIT_Y = 'de-bpm-z'


class BPMs(HWBase):

    def __init__(self, client, configs):
        self.configs = configs
        super(BPMs, self).__init__()
        self._cpv_kap_Jx = self.add_cpv(client, PV_ORBIT_X)
        self._cpv_kap_Jy = self.add_cpv(client, PV_ORBIT_Y)
        logging.debug('BPMs: initialized')

    def get_kap_F(self):
        if self._cpv_kap_Jx.value is None:
            kap_Jx = np.zeros(self.configs.Jx)
        else:
            kap_Jx = self._cpv_kap_Jx.value
        if self._cpv_kap_Jy.value is None:
            kap_Jy = np.zeros(self.configs.Jy)
        else:
            kap_Jy = self._cpv_kap_Jy.value
        kap_F = np.concatenate([
            kap_Jx,
            kap_Jy
            ])
        severities = tuple(
            [self._cpv_kap_Jx.severity for j in range(self.configs.Jx)] \
            + [self._cpv_kap_Jy.severity for j in range(self.configs.Jy)]
        )
        return kap_F, severities


class BufferedBPMs(HWBase):

    def __init__(self, client, configs):

        self.configs = configs

        super(BufferedBPMs, self).__init__()

        self.bpms = BPMs(client, configs)

        # orbit monitor
        self.buf_len = 5
        self.time_interval = 0.1
        self._buffer_kap_F = []
        self._buffer_severity_F = []

        # threading attributes
        self._exit_orbit_monitor = threading.Event()
        self._exit_orbit_monitor.clear()
        self._eid = threading.Event()
        self._lock = threading.Lock()
        self._tid = threading.Thread(target=self._run_orbit_monitor)
        self._tid.setDaemon(True)
        self._tid.start()

        logging.debug('BufferedBPMs: initialized')

    buf_len = ThreadSafeProperty('buf_len')
    time_interval = ThreadSafeProperty('time_interval')

    def close(self):
        self._exit_orbit_monitor.set()
        self._tid.join()

    def _run_orbit_monitor(self):

        def measurement_event(scheduler):

            # schedule next event
            if not self._exit_orbit_monitor.isSet():
                scheduler.enter(self.time_interval, 1, measurement_event, argument=(scheduler,))

            # update buffer
            with self._lock:
                kap_F, severity_F = self.bpms.get_kap_F()
                self._buffer_kap_F.append(kap_F)
                self._buffer_severity_F.append(severity_F)
                if len(self._buffer_kap_F) > self.buf_len:
                    self._buffer_kap_F = self._buffer_kap_F[len(self._buffer_kap_F)-self.buf_len:]
                    self._buffer_severity_F = self._buffer_severity_F[len(self._buffer_severity_F)-self.buf_len:]

        scheduler = sched.scheduler(time.time, self._eid.wait)
        scheduler.enter(self.time_interval, 1, measurement_event, argument=(scheduler,))
        scheduler.run()

    def reset_buffer(self):
        with self._lock:
            self._buffer_kap_F = []
            self._buffer_severity_F = []
            logging.debug('BufferedBPMs: buffers were reset')

    def get_buffers(self, buf_len=None, reset_flag=False):
        old_buf_len = self.buf_len
        if buf_len is not None:
            self.buf_len = buf_len
        if reset_flag:
            self.reset_buffer()

        # copy buffers
        while True:
            with self._lock:
                buffer_kap_F = copy.deepcopy(self._buffer_kap_F)
                buffer_severity_F = copy.deepcopy(self._buffer_severity_F)
            if len(buffer_kap_F) < self.buf_len:
                self._eid.wait(0.5)
                logging.debug('BufferedBPMs: buffer too small (buffer_len = {} / {})'.format(len(buffer_kap_F), self.buf_len))
            else:
                break

        # reset buf_len
        self.buf_len = old_buf_len

        return buffer_kap_F, buffer_severity_F

    def get_averaged_kap_F(self):
        buffer_kap_F, buffer_severity_F = self.get_buffers()
        err_kap_F = np.std(buffer_kap_F, axis=0)
        averaged_kap_F = np.average(buffer_kap_F, axis=0)
        max_severity_F = tuple([Severity(max(np.asarray(buffer_severity_F)[:, f])) for f in range(self.configs.F)])
        return averaged_kap_F, err_kap_F, max_severity_F


class SynchronizedBufferedBPMs(HWBase):

    """
    will return a a single orbit measurement when the injection chain is running

    Attributes:
    -----------
    injection_running: bool
        flagged if the linac trigger is switched on
    kap_synchronous_F: float (F,)-ndarray
        a measurement of kap when the currents of the BoDo dipoles are minimal
    """

    def __init__(self, client, configs):
        super(SynchronizedBufferedBPMs, self).__init__()
        
        self.F = configs.F

        self.buffered_bpms = BufferedBPMs(client, configs)

        # threading attributes
        self.synchronous_orbit_initialized = threading.Event()
        self.synchronous_orbit_initialized.clear()
        self.injection_running = threading.Event()
        self.injection_running.clear()

        # bo_b: signal if BoDo dipole currents are at minimum
        self._kap_synchronous_F = np.zeros(configs.F, np.float64)
        self._severity_synchronous_F = tuple([Severity.INVALID for f in range(configs.F)])
        def callback_bo_b(pv, value_dict, *args, **kwargs):
            value = value_dict['value']
            if value and self.injection_running:
                self._kap_synchronous_F, self._severity_synchronous_F = self.buffered_bpms.bpms.get_kap_F()
                self.synchronous_orbit_initialized.set()
        self._cpv_bo_b = self.add_cpv(client, configs.prefix + 'bo-b-i:low', monitor_callback=callback_bo_b)
        #callback_bo_b(self._cpv_bo_b.get()[0])  # initialize

        # rftrig_sw: signals if linac trigger is turned on or off
        def callback_boramp_sw(pv, value_dict, *args, **kwargs):
            value = value_dict['value']
            if value and not self.injection_running.isSet():
                self.injection_running.set()
                self.synchronous_orbit_initialized.clear()
                logging.debug('BoDo ramp switched on. Switching BPM measurements to synchronized mode.')
            elif not value and self.injection_running: 
                self.injection_running.clear()
                self.synchronous_orbit_initialized.clear()
                self._severity_synchronous_F = tuple([Severity.INVALID for f in range(configs.F)])
                logging.debug('BoDo ramp switched off. Switching BPM measurements to normal mode.')
        self._cpv_boramp_sw = self.add_cpv(client, configs.prefix + 'bo-ramp-sw:set', monitor_callback=callback_boramp_sw)
        # callback_boramp_sw(self._cpv_boramp_sw.get()[0])  # initialize

    kap_synchronous_F = ThreadSafeProperty('kap_synchronous_F')
    severity_synchronous_F = ThreadSafeProperty('severity_synchronous_F')

    def get_synchronized_kap_F(self):
        """get the synchronized orbit deviation. The severity will be set to INVALID if its not initialized, yet."""
        if self.injection_running.isSet():
            logging.debug('returning injection synchronized orbit measurment')
            return self.kap_synchronous_F, np.full((self.F,), np.nan), self._severity_synchronous_F
        else:
            return self.buffered_bpms.get_averaged_kap_F()
