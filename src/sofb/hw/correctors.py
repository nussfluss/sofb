import numpy as np
from timeit import default_timer
import logging
import threading

from .base import HWBase, Severity
from .. import i2phi


class Steerer(HWBase):

    """
    wraps get, set, comp and and quadrupole current records for a single Delta steerer
    """

    def __init__(self, client, configs, idstr, plane, quad_current_src):
        """
        Parameters
        ----------
        configs: configs.Parameters instance
        idstr: str
            String Following the Convention 'XkXX'. The first X is either a 'h' or 'v ' for horizontal or vertical
            correctors. The latter 'XX' is a numer in between '01' and '30' for horizontal correctors or '01' and '26'
            for vertical correctors.
        plane: str
            either 'x' or 'y'
        """
        self.configs = configs
        self.idstr = idstr
        self.plane = plane

        super(Steerer, self).__init__(unarm=configs.unarm_flag)

        self._cpv_I_get = self.add_cpv(client, configs.prefix + 'de-{}-i'.format(idstr))
        self._cpv_I_get._pv.MDEL = 0.0024  # A = resolution of current sources
        self._cpv_I_put = self.add_cpv(client, configs.prefix + 'de-{}-i:set'.format(idstr))
        self._cpv_I_quad = self.add_cpv(client, configs.prefix + quad_current_src)
        self._cpv_I_comp = self.add_cpv(client, configs.prefix + 'de-{}-i:comp'.format(idstr))
        self._cpv_switch = self.add_cpv(client, configs.prefix + 'de-{}-sw'.format(idstr))

    @property
    def at_set_value(self):
        if self._cpv_I_comp.value == 1:
            return True
        elif self._cpv_I_comp.value == 0:
            return False

    @property
    def turned_on(self):
        if self._cpv_switch.value is None:
            return False
        else:
            if self._cpv_switch.value >= 1:
                return True
            elif self._cpv_switch.value == 0:
                return False

    @property
    def I_ctrl_lims(self):
        return self._cpv_I_put.ctrl_lims

    def get_I(self, readback=True):
        if readback:
            return self._cpv_I_get.value, self._cpv_I_get.severity
        else:
            return self._cpv_I_put.value, self._cpv_I_put.severity

    def put_I(self, value, timeout, guard=False):
        return self._cpv_I_put.put(value, timeout, guard)

    def get_I_quad(self):
        return self._cpv_I_quad.value, self._cpv_I_quad.severity

    def check(self):
        return bool(self._cpv_I_comp.value)


class FastCorr(HWBase):

    def __init__(self, client, configs, record_name, plane, *args, **kwargs):

        self.record_name = record_name
        self.plane = plane

        super(FastCorr, self).__init__(unarm=configs.unarm_flag)

        self._cpv_I_get = self.add_cpv(client, configs.prefix + self.record_name)
        self._cpv_I_put = self.add_cpv(client, configs.prefix + self.record_name + ':set')
        self._cpv_switch = self.add_cpv(client, configs.prefix + self.record_name.replace('-i', '-sw:set'))

    @property
    def I_ctrl_lims(self):
        self._cpv_I_put.update_metadata()
        return self._cpv_I_put.ctrl_lims

    def get_I(self):
        return self._cpv_I_get.value

    def put_I(self, value, control=True):
        self._cpv_I_put.put(value, control)
        if value == 0.0:
            if self._cpv_switch.value == 1.0:
                self._cpv_switch.put(0, control)
                logging.debug('FastCorr({}) turned OFF.'.format(self.record_name))
        else:
            if self._cpv_switch.value == 0.0:
                self._cpv_switch.put(1, control)
                logging.debug('FastCorr({}) turned ON.'.format(self.record_name))
        logging.debug('FastCorr({}) set to {:2.2f}.'.format(self.record_name, value))

    def check(self):
        """check whether steerer is at set value"""
        return True


class Correctors(HWBase):
    """
    Attributes:
    -----------
    I_mask_K: np.ndarray or None
        masks the steerer set currents if self.put_I_K() fails
    corrs: list
        list of SOFBCorr instances
    ready: bool
        flagged by self.check() if self.ready_trigger. Only if flagged new currents can be set.
    _triggered: bool
        flagged by callback if ramping is commencing. This is neccessary because sometimes de-steererramp-stat lags behind a few seconds. Without the arrmed flag Correctors would then assume that ramping has already commenced although it has not even started yet.
    """
    def __init__(self, client, configs, ramp_timeout=30.0):
        super(Correctors, self).__init__(unarm=configs.unarm_flag)

        self.K = configs.K
        self._I_mask_K = None
        self._unblock_after_ramp = True
        self._ramp_timeout = ramp_timeout
        self._triggered = threading.Event()
        self.T_ready = default_timer()
        self._T_ramp_start = None
        self.ready = False
        self.corrs = []
        for k in range(configs.K):
            if configs.ar_cor[k]['type'] == 'std':
                self.corrs.append(self.add_instance(Steerer(client,
                                                            configs=configs,
                                                            idstr=configs.ar_cor[k]['idstr'],
                                                            plane=configs.ar_cor[k]['plane'],
                                                            quad_current_src=configs.ar_cor[k]['current_src'])))
            elif configs.ar_cor[k]['type'] == 'fast':
                self.corrs.append(self.add_instance(FastCorr(client,
                                                             configs=configs,
                                                             record_name=configs.ar_cor[k]['record_name'],
                                                             plane=configs.ar_cor[k]['plane'])))

        # ramp
        self._cpv_ramp_start = self.add_cpv(client, configs.prefix + 'de-steererramp:start')  # start ramp by putting 1
        self._cpv_ramp_t     = self.add_cpv(client, configs.prefix + 'de-steererramp-t:set')  # set ramp mode and block by putting -1, release blocking by putting 0, block only by putting 1
        def arm(value, *args):
            """see above for details"""
            if bool(value):
                self._triggered.set()
        self._cpv_ramp_stat  = self.add_cpv(client, configs.prefix + 'de-steererramp-stat', arm)  # ramp status: RUNNING 1, STOPPED 0

    def close(self):
        self.unblock()

    @property
    def I_bounds_K2(self):
        """
        Control limits for all registered steerer magnets.

        Return
        ------
        I_bounds_K2: np.ndarray, dtype=np.float64
            Either pairs of upper and lower control current limits (HOPR, LOPR) or pairs of Nones (None, None) for all K correctors.
            example: ((-10.0, 10.0), (None, None), (-10.0, 10.0), ... , (None, None))
        """
        I_bounds = []
        for k in range(self.K):
            I_ctrl_lims = self.corrs[k].I_ctrl_lims
            if I_ctrl_lims is None:
                I_bounds.append((None, None))
            else:
                I_bounds.append(I_ctrl_lims)
        return np.asarray(I_bounds, np.float64)

    def check(self):
        """
        Check stereer state.

        This method has to be called from the main thread because pv.put() cannot be invoked in a callback.
        """

        def redeem_steerers():
            """reinstate set values of steerer currents from before ramping was aborted"""
            I_K, severity_I_K = self.get_I_K(unmask=True)
            if np.allclose(I_K, self._I_mask_K, atol=0.0001):
                self._I_mask_K = None
                if self._unblock_after_ramp:
                    self.unblock()
            else:
                for k in range(self.K):
                    if abs(I_K[k] - self._I_mask_K[k]) > 0.0001:
                        self.corrs[k].put_I(self._I_mask_K[k])

        def check_if_ramping_commenced():
            """check whether ramping driver signals that ramping has commenced"""
            if self._unarm and self._triggered.isSet():
                return True
            elif not self.ready and self._cpv_ramp_stat.value == 0 and self._triggered.isSet():
                return True
            else:
                if self._T_ramp_start is not None:
                    if default_timer() - self._T_ramp_start >= self._ramp_timeout:
                        self._T_ramp_start = None
                        logging.warning('de-steererramp-stat never signaled ramp start. Assuming ramping commenced anyways.')
                        return True
                return False

        if check_if_ramping_commenced():
            if self._I_mask_K is not None:
                redeem_steerers()
            if self._unblock_after_ramp:
                self.unblock()
            if not self.ready:
                self.ready = True
                self._triggered.clear()
                self.T_ready = default_timer()  # start counting deadtime
                logging.debug('Finished ramping.')

    def get_I_K(self, readback=False, unmask=False):
        """
        Collect currents from steerer magnets.

        Returns
        -------
        I_K : float (K)-ndarray
            Steerer currents.
        severity_K: (K)-tuple
            Severities of steerer currents.
        """
        Is = []
        severities = []
        for corr in self.corrs:
            I, severity = corr.get_I(readback)
            Is.append(I)
            severities.append(severity)
        if self._I_mask_K is None or unmask:
            return np.asarray(Is, np.float64), tuple(severities)
        else:
            return self._I_mask_K, tuple(severities)

    def put_I_K(self, I_K, unblock_after_ramp=True):
        """
        Set steerer currents.

        Arm ramp controller, write steerer currents to PVs and start ramp. If a write operation times out, ramping will not commence:
          1. If auto correcion is enabled, the ramp controller will remain armed (steerers are blocked) until a correction step succeeds.
          2. If put_I_K() was invoked for a single correction step, the steerers will be unblocked after steerers were redeemed.
        """
        self._unblock_after_ramp = unblock_after_ramp

        # determine and check required steerers
        I_now_K, severity_I_now_K = self.get_I_K(readback=False)
        steerers_to_use = []
        for k in range(self.K):
            if abs(I_K[k]-I_now_K[k]) > 0.00001:
                steerers_to_use.append(k)
                if severity_I_now_K[k] >= Severity.INVALID:
                    logging.warning('steerer {} is in an error state'.format(self.corrs[k].idstr))
                    return False

        # set currents
        aborting = False
        used_steerers = []
        if self._cpv_ramp_t.put(-1, timeout=3.0):  # sets ramp mode and locks steerers
            for k in steerers_to_use:
                if self.corrs[k].put_I(I_K[k], timeout=1.0, guard=True):
                    used_steerers.append(k)
                else:
                    Delta_I = I_K[k] - I_now_K[k]
                    if abs(Delta_I) > 0.01:
                        aborting = True
                        logging.warning('failed to set {} to {:.04f} A'.format(self.corrs[k].idstr, Delta_I))
                        break
                if not aborting:
                    if not self._cpv_ramp_start.put(1, timeout=6.0):  # starts ramping
                        logging.warning('failed to start ramp')
                        aborting = True
                if aborting:
                    logging.warning('reinstating old steerer currents')
                    for k in used_steerers:
                        self.corrs[k].put_I(I_now_K[k], timeout=0.1)
                    self._I_mask_K = I_now_K
                    self.T_ready = default_timer()  # start counting deadtime
                    return False
        else:
            logging.warning('failed to initialize ramp')
            return False

        # make steerers unavailable if ramp was successfully started
        self.ready = False
        self._T_ramp_start = default_timer()
        self._I_mask_K = None
        return True


    def get_I_quad_K(self):
        I_quads = []
        severities = []
        for corr in self.corrs:
            I_quad, severity = corr.get_I_quad()
            I_quads.append(I_quad)
            severities.append(severity)
        return np.asarray(I_quads, np.float64), tuple(severities)

    def unblock(self):
        if self._cpv_ramp_t.put(0, timeout=0.2):  # unblocks correctors
            logging.debug('Unblocked correctors.')
        else:
            logging.warning('Failed to unblock correctors.')
