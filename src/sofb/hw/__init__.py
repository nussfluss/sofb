from channel_access.client import Client
from . import correctors, bpms
from .base import HWBase, ThreadSafeProperty
import logging

class HW(HWBase):

    """
    HW manages all hardware connections
    """

    def __init__(self, configs):
        super(HW, self).__init__(unarm=configs.unarm_flag)
        self.client = Client()
        self.corrs = self.add_instance(correctors.Correctors(self.client, configs))
        self.bpms = self.add_instance(bpms.SynchronizedBufferedBPMs(self.client, configs))
        self._cpv_q_x = self.add_cpv(self.client, configs.prefix + 'de-qana-qx')
        self._cpv_q_z = self.add_cpv(self.client, configs.prefix + 'de-qana-qy')
        self._cpv_beam_current = self.add_cpv(self.client, configs.prefix + 'de-beam-i')
        self._cpv_beam_energy = self.add_cpv(self.client, configs.prefix + 'de-beam-energy')
        self._cpv_f_get = self.add_cpv(self.client, configs.prefix + 'bo-rfmaster-f')
        self._cpv_f_put = self.add_cpv(self.client, configs.prefix + 'bo-rfmaster-f:set')
        logging.debug('HW: initialized')

    def close(self):
        self.corrs.close()
        self.bpms.buffered_bpms.close()
        self.client.shutdown()
        logging.debug('HW: closed ca client')

    def get_q_x(self):
        return self._cpv_q_x.get()

    def get_q_z(self):
        return self._cpv_q_z.get()

    def get_beam_current(self):
        return self._cpv_beam_current.get()

    def get_beam_energy(self):
        energy, severity = self._cpv_beam_energy.get()
        if energy is not None:
            return energy/1000.0, severity
        else:
            return 1.492, 0

    def get_f(self):
        f_get, severity_f_get = self._cpv_f_get.get()
        f_put, severity_f_put = self._cpv_f_put.get()
        return f_get, severity_f_get if severity_f_get >= severity_f_put else severity_f_put

    def put_f(self, value, timeout=1.0):
        return self._cpv_f_put.put(value, timeout)

