from channel_access.common import Severity as CASeverity
from threading import Lock
from enum import Enum, IntEnum
import logging
from timeit import default_timer


class Severity(IntEnum):
    """
    abstracts epics severity lvls and ca channel disconnects into a severity scale

    States:
    -------
    NO_ALARM       PV connection and data source are all right
    MINOR          PV is connected but data source of record has encountered a minor alarm
    MAJOR          PV is connected but data source of record has encountered a major alarm
    INVALID        PV is connected but data source of record is invalid
    NETWORK        PV is disconnected and there is no knowledge about the state of the source
    """
    NO_ALARM = 0
    MINOR_ALARM = 1
    MAJOR_ALARM = 2
    INVALID = 3
    NETWORK = 4


class ThreadSafeProperty(object):

    def __init__(self, name, callback=None):
        self.name = "_" + name
        self._lock = Lock()
        self._callback = callback

    def __get__(self, obj, objtype, *args, **kwargs):
        if self._callback is not None:
            self._callback()
        with self._lock:
            return getattr(obj, self.name)

    def __set__(self, obj, value, *args, **kwargs):
        with self._lock:
            setattr(obj, self.name, value)


class CPV(object):
    """
    Wraps ca_client.PV.
    """
    def __init__(self, client, name, unarm=True, monitor_callback=True, deadtime=60):
        self.name = name
        self._unarm = unarm
        self._pv = client.createPV(name, monitor=monitor_callback)
        self._deadtime = deadtime
        self._T_timeout = default_timer() - deadtime

    @property
    def units(self):
        return self._pv.units

    @property
    def ctrl_lims(self):
        return self._pv.control_limits

    @property
    def warn_lims(self):
        return self._pv.warn_limits

    @property
    def alarm_lims(self):
        return self._pv.alarm_limits

    @property
    def severity(self):
        if self._pv.connected:
            if self._pv.severity is None or self._T_timeout + self._deadtime > default_timer():
                return Severity.INVALID
            else:
                return {CASeverity.NO_ALARM: Severity.NO_ALARM,
                        CASeverity.MINOR: Severity.MINOR_ALARM,
                        CASeverity.MAJOR: Severity.MAJOR_ALARM,
                        CASeverity.INVALID: Severity.INVALID}[self._pv.severity]
        else:
            return Severity.NETWORK

    @property
    def value(self):
        return self._pv.value

    def get(self):
        return self.value, self.severity

    def put(self, value, timeout, guard=False):
        """
        Write value to PV.

        Parameters
        ----------
        guard: flag to invalidate pv for a time after a timeout

        Returns
        -------
        True if value is changed on server or False if value is not changed on the server.
        """
        if timeout < 0.1:
            timeout = 0.1
        if not self._unarm:
            answ = self._pv.put(value, block=timeout)
            if answ is None:
                if guard:
                    self.T_timeout = default_timer()
                    logging.warning('Record {} timed out during write operation. Disabling pv for {} s.'.format(self.name, self._deadtime))
                else:
                    logging.warning('Record {} timed out during write operation.'.format(self.name))
                return False
                return answ
            else:
                return answ
        else:
            return True

class HWBase(object):

    """
    base class for all classes that require connecting to epics records
    """

    def __init__(self, unarm=False):
        self._unarm = unarm

        self._cpvs = []
        self._instances = []

    @property
    def severity(self):
        """
        determine biggest severity of all registered instances and cpvs
        """
        return Severity(max([cpv.severity.value for cpv in self._cpvs] + [instance.severity.value for instance in self._instances]))

    def add_cpv(self, client, name, monitor_callback=True):
        cpv = CPV(client,  name, unarm=self._unarm, monitor_callback=monitor_callback)
        self._cpvs.append(cpv)
        return cpv

    def add_instance(self, instance):
        """
        Arguments:
            instance
                any instance of a class derived from HWBase
        """
        if isinstance(instance, HWBase):
            self._instances.append(instance)
        else:
            raise TypeError('{} is not derived from HWBase'.format(type(instance).__name__))
        return instance
