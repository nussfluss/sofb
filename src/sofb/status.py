import logging
from enum import Enum
import numpy as np


class ErrorCodes(Enum):

    ERROR = 0
    OK = 1
    INVALID = 2  # not initialized yet


class BaseStatus(object):

    def __init__(self):
        self.status = ErrorCodes.INVALID
        self.statusstr = 'INVALID'
        self.error_count = 0


class Status(BaseStatus):

    def __init__(self):
        super().__init__()

    def set(self, status, statusstr):
        if statusstr is not self.statusstr:
            if status == ErrorCodes.ERROR:
                self.error_count += 1
        self.statusstr = statusstr
        self.status = status


class CorrStati(BaseStatus):

    def __init__(self, N):
        super().__init__()

        self.N = N
        self.stati = [ErrorCodes.INVALID for n in range(N)]
        self.statusstrs = ['INVALID' for n in range(N)]
        self.error_count_N = np.zeros((N,), np.int)

    def set(self, stati, statusstrs):
        self.stati = stati
        self.statusstrs = statusstrs
        N_unavailable = 0
        for n in range(self.N):
            if stati[n] == ErrorCodes.ERROR:
                self.error_count_N[n] += 1
                N_unavailable += 1
        availability = (self.N-N_unavailable)/self.N*100
        status = ErrorCodes.OK
        self.statusstr = '{}% AVAILABLE'.format(int(availability))
        if availability < 70:
            status = ErrorCodes.ERROR
            if status is not self.status:
                self.error_count += 1
        else:
            status = ErrorCodes.OK
        self.status = status


class BPMStati(BaseStatus):

    def __init__(self, N):
        super().__init__()

        self.N = N
        self.stati = [ErrorCodes.ERROR for n in range(N)]
        self.statusstrs = ['INVALID' for n in range(N)]
        self.error_count_N = np.zeros((N,), np.int)

    def set(self, stati, statusstrs, weights):
        self.stati = stati
        self.statusstrs = statusstrs
        N_unavailable = 0
        weight_unavailable = 0
        for n in range(self.N):
            if stati[n] == ErrorCodes.ERROR:
                self.error_count_N[n] += 1
                N_unavailable += 1
                weight_unavailable += weights[n]
        availability = (self.N-N_unavailable)/self.N*100
        weight_availability = (sum(weights)-weight_unavailable)/sum(weights)*100
        status = ErrorCodes.OK
        self.statusstr = '{}% AVAILABLE'.format(int(availability))
        if availability < 70:
            status = ErrorCodes.ERROR
            if status is not self.status:
                self.error_count += 1
        # elif weight_availability < 80:
        #     status = ErrorCodes.ERROR
        #     if status is not self.status:
        #         self.error_count += 1
        #     self.statusstr = 'weighted BPMs unavailable'
        else:
            status = ErrorCodes.OK
        self.status = status


class SOFBStati(object):

    def __init__(self):
        self._registry = {}
        self._msgs = []
        logging.debug('intialized.')

    def register(self, name, number=1):
        if number == 1:
            self._registry.update({name: Status()})
            logging.debug('registered Status for {}'.format(name))
        else:
            if 'corr' in name:
                self._registry.update({name: CorrStati(number)})
                logging.debug('registered {} CorrStati for {}'.format(number, name))
            else:
                self._registry.update({name: BPMStati(number)})
                logging.debug('registered {} BPMStati for {}'.format(number, name))

    def set(self, name, *args, **kwargs):
        self._registry[name].set(*args, **kwargs)

    def get(self, name):
        return self._registry[name]

    def add_status_msg(self, msg):
        self._msgs.append(msg)

    def get_status_msg(self):
        if len(self._msgs) > 0:
            msg = self._msgs[0]
            self._msgs.pop(0)
        else:
            msg = None
        return msg

    @property
    def sofb_status(self):
        status = ErrorCodes.OK
        for _, item in self._registry.items():
            if item.status is ErrorCodes.INVALID:
                status = ErrorCodes.INVALID
                break
            elif item.status is ErrorCodes.ERROR:
                status = ErrorCodes.ERROR
        return status
