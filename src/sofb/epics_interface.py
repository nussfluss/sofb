import logging
import time

logging.basicConfig(level=logging.INFO)


class SOFBInterface(object):

    def __init__(self, client):

        self.client = client

        self.pv_steererramp_stat = client.createPV('de-steererramp-stat', monitor=True)
        self.pv_sofb_cmd = client.createPV('SK-sofb-cmd', monitor=True)
        self.pv_sofb_sub_cmd = client.createPV('SK-sofb-subcmd', monitor=True)
        self.pv_sofb_active = client.createPV('SK-sofb-active', monitor=True)
        self.pv_sofb_wrms_x = client.createPV('SK-sofb-wrms:x', monitor=True)
        self.pv_sofb_wrms_y = client.createPV('SK-sofb-wrms:y', monitor=True)
        self.pv_sofb_autocorrect = client.createPV('SK-sofb-set:autocorrect', monitor=True)
        self.pv_sofb_reference_fname = client.createPV('SK-sofb-set:reference:fname', monitor=True)
        self.pv_sofb_response_fname = client.createPV('SK-sofb-set:response:fname', monitor=True)
        self.pv_sofb_bpm_ref_x = client.createPV('SK-sofb-set:bpms:ref:x', monitor=True)
        self.pv_sofb_bpm_ref_y = client.createPV('SK-sofb-set:bpms:ref:y', monitor=True)
        self.pv_sofb_bpm_weights_x = client.createPV('SK-sofb-set:bpms:weights:x', monitor=True)
        self.pv_sofb_bpm_weights_y = client.createPV('SK-sofb-set:bpms:weights:y', monitor=True)

        time.sleep(3.0)  # give pvs some time to connect

    @property
    def wrms_x(self):
        """horizontal weighted orbit root mean square (WRMS)"""
        return self.pv_sofb_wrms_x.value

    @property
    def wrms_y(self):
        """vertical weighted orbit root mean square (WRMS)"""
        return self.pv_sofb_wrms_y.value

    @property
    def orbit_ref_x(self):
        return self.pv_sofb_bpm_ref_x.value

    @property
    def orbit_ref_y(self):
        return self.pv_sofb_bpm_ref_y.value

    @property
    def orbit_weights_x(self):
        return self.pv_sofb_bpm_weights_x.value

    @property
    def orbit_weights_y(self):
        return self.pv_sofb_bpm_weights_y.value

    def correct(self, target_wrms_x=5.0, target_wrms_y=5.0):
        """correct orbit until orbit is sufficiently close to orbit reference"""
        logging.info('correcting orbit ...')
        self.pv_sofb_autocorrect.put(1, block=0.2)  # start autocorrect
        time.sleep(2.0)
        while True:
            if self.pv_sofb_wrms_x.value < target_wrms_x and self.pv_sofb_wrms_y.value < target_wrms_y:
                time.sleep(2.0)
                # wrms can jitter greatly for a short duration after a step. So check again...
                if self.pv_sofb_wrms_x.value < target_wrms_x and self.pv_sofb_wrms_y.value < target_wrms_y:
                    break
            elif not self.pv_sofb_active.value == 1 or not self.pv_sofb_autocorrect.value == 1:
                self.pv_sofb_autocorrect.put(1, block=0.2)  # start autocorrect again
            time.sleep(2.0)
            try:
                logging.info('(wrms_x, wrms_y) = ({:.3f}, {:.3f})'.format(self.pv_sofb_wrms_x.value, self.pv_sofb_wrms_y.value))
            except:
                pass
        self.pv_sofb_autocorrect.put(0, block=0.2)  # stop autocorrect
        time.sleep(2.0)
        while self.pv_steererramp_stat.value == 1:
            if self.pv_sofb_autocorrect.value == 1:
                self.pv_sofb_autocorrect.put(0, block=0.2)  # make sure autocorrect is off
            time.sleep(2.0)
        logging.info('corrected orbit')

    def load_response(self, name):
        if self.pv_sofb_response_fname.put(name, block=1.0):
            if self.pv_sofb_sub_cmd.put(3, block=1.0):
                logging.info('loaded {}'.format(name))
                return True
        return False

    def load_reference(self, name):
        if self.pv_sofb_reference_fname.put(name, block=1.0):
            if self.pv_sofb_sub_cmd.put(2, block=1.0):
                logging.info('loaded {}'.format(name))
                return True
        return False

    def set_orbit_reference(self, orbit_ref_x, orbit_ref_y):
        if self.pv_sofb_bpm_ref_x.put(orbit_ref_x, block=1.0):
            if self.pv_sofb_bpm_ref_y.put(orbit_ref_y, block=1.0):
                logging.info('wrote orbit reference')
                return True
        logging.info('did not write orbit reference')
        return False

    def set_orbit_weights(self, orbit_weights_x, orbit_weights_y):
        if self.pv_sofb_bpm_weights_x.put(orbit_weights_x, block=1.0):
            if self.pv_sofb_bpm_weights_y.put(orbit_weights_y, block=1.0):
                logging.info('wrote orbit weights')
                return True
        logging.info('did not write orbit weights')
        return False


if __name__ == '__main__':

    import channel_access.client as ca_client
    import numpy as np

    with ca_client.Client() as client:

        # create interface
        sofb = SOFBInterface(client)

        # load response
        sofb.load_response('response.160311-1')
        print('\n\n')

        # use own reference and weights
        sofb.set_orbit_reference(np.zeros(54, dtype=np.float), np.zeros(54, dtype=np.float))
        sofb.set_orbit_weights(np.ones(54, dtype=np.float), np.ones(54, dtype=np.float))
        time.sleep(2.0)
        print('\nreference:')
        print(sofb.orbit_ref_x)
        print(sofb.orbit_ref_y)
        print('\nweights:')
        print(sofb.orbit_weights_x)
        print(sofb.orbit_weights_y)
        print('\n\n')

        # load reference from file
        sofb.load_reference('reference.210316-1')
        time.sleep(2.0)
        print('\nreference:')
        print(sofb.orbit_ref_x)
        print(sofb.orbit_ref_y)
        print('\nweights:')
        print(sofb.orbit_weights_x)
        print(sofb.orbit_weights_y)

        # correct orbit
        print('\n\nwrms before:')
        print(sofb.wrms_x)
        print(sofb.wrms_y)
        print('\n')
        sofb.correct(5, 5)
        print('\nwrms after:')
        print(sofb.wrms_x)
        print(sofb.wrms_y)





