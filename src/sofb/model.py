import logging
from os import path
import numpy as np
import collections
import copy
from enum import Enum

from . import rw
from .status import ErrorCodes
from .orbcor import Solvers, PathLength


class CTuple(object):
    def __init__(self, **kwargs):
        for key, item in kwargs.items():
            if isinstance(item, collections.abc.Iterable) and not isinstance(item, str):
                self.__dict__.update({key: np.asarray(item)})
            else:
                self.__dict__.update({key: item})


Configs = collections.namedtuple('Configs', ('debug_flag', 'unarm_flag', 'dr_flag',
                                             'config_path', 'persistence_path', 'out_path', 'reference_path', 'response_path', 'settings_path',
                                             'ar_cor', 'ar_bpm',
                                             'F', 'Jx', 'Jy', 'K', 'Kh', 'Kv',
                                             'prefix'))


def read_csv(fpath, spacer=',', tup=False):
    """
    Reads in a csv-file which fulfills:
        1. comment lines start with #
        2. comma (or spacer) is used as separator

    Parameters:
    -----------
    fpath : str
        filepath to csv
    spacer : str
        separator of csv file. Default: spacer = ','.

    Returns:
    --------
    dat : list
        csv file as nested list
    """

    # fpath = abspath(path=fpath)

    dat = list()
    with open(fpath, 'r') as f:
        for line in f.readlines():
            if not line[0] == '#':
                line = line.split(spacer)
                for i in range(len(line)):
                    line[i] = line[i].strip()

                    # converts value to float if possible
                    # => ensures that asarray() doesnt produce casting.warnings
                    try:
                        line[i] = float(line[i])
                    except:
                        pass

                if tup:
                    dat.append(tuple(line))
                else:
                    dat.append(line)
    return dat


# custom dtype to save bpm_list.csv in
dt_ar_bpm = np.dtype([('idstr', np.str_, 6),
                      ('plane', np.str_, 1),
                      ('type', np.str_, 6),
                      ('spos', float),
                      ('record', np.str_, 10),
                      ('offrecord', np.str_, 17),
                      ('comment', np.str_, 40)])


def ar_bpm_from_list(bpm_list, dr_flag=False):
    """
    Creates a numpy custom array
        ar_bpm
    with dtype dt_ar_bpm (specified above) from a a nested list
        bpm_list.

    The 'num' column (number of bpm) in bpm_info.csv and bpm_list is replaced with a 'j' column (number of bpm in array
    indexing):

        j = num(j) - 1

    Parameters:
    -----------
    bpm_list : list
        list created from bpm_info.csv

    Returns:
    --------
    ar_bpm : custom ndarray
        Numpy custom ndarray (dtype=dt_ar_bpm) created from bpm_list with keys
            'idstr'     := str - unique idstr of bpm
            'plane'     := str - either "x" or "y"
            'type'      := str - either 'std' or 'fast'
            'spos'      := float - spos of bpm along the ring
            'comment'   := str - comment
    """

    ar_bpm = np.array(bpm_list, dtype=dt_ar_bpm)  # bpm_list needs to be a list of tuples!

    return ar_bpm


# custom dtype to convert list_cor to structured nd-array
dt_ar_cor = np.dtype([('idstr', str, 10),
                      ('plane', str, 1),
                      ('type', str, 4),
                      ('spos', float),
                      ('after_j', int),
                      ('record_name', str, 20),
                      ('current_src', str, 20),
                      ('mount_type', str, 10),
                      ('sextupole_type', str, 12),
                      ('comment', str, 40)])


def ar_cor_from_list(list_cor):
    """
    Creates a numpy custom array
        ar_cor
    with dtype dt_ar_cor (specified above) from a nested list
        cor_list.

    Converts column 'after_bpm' from corr_info.csv to 'after_j' column by substracting 1 from each entry.

    Parameters:
    -----------
    cor_list : list
        list created from corr_info.csv

    Returns:
    --------
    ar_bpm : custom ndarray
        Numpy custom ndarray (dtype=dt_ar_bpm) created from bpm_list with keys
            'idstr'         := str - unique identifier of corrector
            'plane'         := str - either 'x' or 'y'
            'type'          := str - either 'std' or 'fast'
            'after_j'       := int - last bpm j coming in front of corrector k in terms of spos.
            'current_src'   := str - epics record quadrupole of current source
            'mount_type'    := str - dimensions of quadrupole yoke
            'sextupole_type':= str - dimensions of quadrupole yoke
            'comment'       := str - comment
    """

    # convert to numpy custom dtype
    ar_cor = np.array(list_cor, dtype=dt_ar_cor)  # list_cor needs to be a LIST of TUPLES!

    # correct after_j from num to array indexing var j
    for k in range(len(ar_cor)):
        ar_cor[k]['after_j'] -= 1

    return ar_cor


class SOFBModel(object):

    def __init__(self, sofb_stati, debug_flag, unarm_flag, dr_flag, config_path='configs', settings_path='parameters', persistence_path='persistence',
                 out_path='parameters', reference_path='parameters', response_path='parameters'):
        """
        Attributes:
        -----------
        K: int
            number of correctors
        Kh: int
            number of vertical correctors
        Kv: int
            number of horizontal correctors
        """
        if debug_flag:
            prefix = 'SK-'
            logging.warning('debug mode')
        else:
            prefix = ''

        # load configuration
        # ------------------

        # load corrector-magnet information
        ar_cor_path = path.join(config_path, 'corr_info.csv')
        ar_cor = ar_cor_from_list(read_csv(ar_cor_path, tup=True))
        logging.info('loaded corrector configuration from {}'.format(ar_cor_path))
        K = ar_cor.shape[0]
        logging.debug('K = {}'.format(K))
        Kh, Kv = 0, 0
        for k in range(K):
            if ar_cor[k]['plane'] == 'x':
                Kh += 1
            else:
                Kv += 1
        Kh = Kh
        logging.debug('Kh = {}'.format(Kh))
        Kv = Kv
        logging.debug('Kv = {}'.format(Kv))

        # load BPM information
        ar_bpm_path = path.join(config_path, 'bpms.csv')
        ar_bpm = ar_bpm_from_list(read_csv(ar_bpm_path, tup=True))
        logging.info('loaded bpm configuration from {}'.format(ar_bpm_path))
        F = ar_bpm.shape[0]
        logging.debug('F = {}'.format(F))
        Jx, Jy = 0, 0
        for f in range(F):
            if ar_bpm[f]['plane'] == 'x':
                Jx += 1
            else:
                Jy += 1
        logging.debug('Jx = {}'.format(Jx))
        logging.debug('Jy = {}'.format(Jy))
        if not Jx == Jy:
            raise Exception('number of vertical BPMs does not match number of horizontal BPMs')

        if dr_flag:
            for f in range(F):
                if ar_bpm[f]['type'] == 'bergoz':
                    ar_bpm[f]['record'] = ar_bpm[f]['record'].replace('de-', 'DR-')
                    ar_bpm[f]['offrecord'] = ar_bpm[f]['record'] + '.EOFF'
        if debug_flag:
            for f in range(F):
                ar_bpm[f]['record'] = 'SK-' + ar_bpm[f]['record']
                ar_bpm[f]['offrecord'] = 'SK-' + ar_bpm[f]['offrecord']

        self.configs = Configs(debug_flag=debug_flag,
                               unarm_flag=unarm_flag,
                               dr_flag=dr_flag,
                               config_path=config_path,
                               persistence_path=persistence_path,
                               out_path=out_path,
                               reference_path=reference_path,
                               response_path=response_path,
                               settings_path=settings_path,
                               ar_cor=ar_cor,
                               ar_bpm=ar_bpm,
                               F=F,
                               Jx=Jx,
                               Jy=Jy,
                               K=K,
                               Kh=Kh,
                               Kv=Kv,
                               prefix=prefix)

        # initialize parameters
        # ---------------------
        self.parameters = CTuple(R_FK=np.zeros((F, K), np.float64),
                                 Rd_F=np.zeros((F,), np.float64),
                                 sig_R_FK=np.zeros((F, K), np.float64),
                                 norm=None,
                                 kap_ref_F=np.zeros((F,), np.float64),
                                 W_FF=np.zeros((F, F), np.float64),
                                 kap_ref_original_F=np.zeros((F,), np.float64),
                                 W_original_FF=np.zeros((F, F), np.float64),
                                 constraint_F2=np.asarray([[-np.inf, np.inf] for f in range(F)]))

        # initialize settings
        # -------------------
        self.settings = CTuple(settings_fname='HARD-CODED DEFAULTs',
                               response_fname='response.160311-1',
                               reference_fname='reference.201207-1',
                               sigmas_to_remove_h=2,
                               sigmas_to_remove_v=3,
                               decoupled_flag=True,
                               factor = 0.5,
                               weighted_rms_threshold_nostep_x = 1.0,
                               weighted_rms_threshold_nostep_y = 1.0,
                               weighted_rms_threshold_micado_x = 10.0,
                               weighted_rms_threshold_micado_y = 10.0,
                               limit_err_kap = 0.01,
                               I_beam_min = 1.0,
                               use_thikonov = False,
                               remove_pincushion_dist = False,
                               dispersion_correction = PathLength.IGNORE.value,
                               minimize_currents = True,
                               orbit_slack = 0.000,
                               orbit_slack_factor = 0.0,
                               t_sleep = 3.0,
                               buf_len = 10,
                               Delta_I_cut = 0.003,
                               Delta_I_max = 2.0,
                               Delta_phi_cut = 1e-6,
                               Delta_f = 0.001,
                               I_bounds_factor = 0.98,
                               method=Solvers.QPCONE,
                               use_F = np.ones((F,), np.int),  # loaded as a setting AND a parameter via a reference file
                               use_K = np.ones((K,), np.int),
                               correction_step_history_length = 100,
                               status_history_length = 100,
                               status_history_time_interval = 5)  # seconds

        # stati
        # -----
        sofb_stati.register('reference')
        sofb_stati.register('response')
        self.load_persistence(sofb_stati)
        self.load_response(self.settings.response_fname, sofb_stati)
        self.load_reference(self.settings.reference_fname, sofb_stati)
        logging.debug('SOFBModel initialized')

    def load_settings(self, fname, sofb_stati):
        fpath = path.join(self.configs.settings_path, fname)
        logging.debug('loading settings from {}'.format(fpath))
        try:
            settings_dat = rw.load(fpath)
            settings = CTuple(**settings_dat['settings'])
            for key in self.settings.__dict__.keys():
                if not key in settings.__dict__.keys():
                    raise AttributeError('settings do not contain {}'.format(key))
            self.settings = settings
        except AttributeError:
            logging.warning('AttributeError while loading {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'AttributeError')
        except FileNotFoundError:
            logging.warning('FileNotFoundError while loading {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'FileNotFoundError')
        except IsADirectoryError:
            logging.warning('IsADirectoryError while loading {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IsADirectoryError')
        except IOError:
            logging.warning('IOError while loading {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IOError')
        else:
            logging.info('loaded settings from {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.OK, 'loaded {}'.format(fname))
            sofb_stati.add_status_msg('loaded {}'.format(fname))

    def save_settings(self, fname, sofb_stati):
        fpath = path.join(self.configs.settings_path,  fname)
        logging.debug('saving settings to {}'.format(fpath))
        settings = {'settings': copy.deepcopy(self.settings.__dict__)}
        settings['settings']['settings_fname'] = fname
        try:
            rw.save(settings, fpath)
        except FileExistsError:
            logging.warning('FileExistsError while writing {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'FileExistsError')
        except IsADirectoryError:
            logging.warning('IsADirectoryError while writing {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IsADirectoryError')
        except IOError:
            logging.warning('IOError while writing {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IOError')
        else:
            logging.info('saved settings to {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.OK, 'saved {}'.format(fname))
            sofb_stati.add_status_msg('saved {}'.format(fname))
            self.settings.settings_fname = fname

    def load_persistence(self, sofb_stati):
        """The persistence simply is the name of a settings file."""
        fpath = path.join(self.configs.persistence_path, 'sofb.persistent')
        logging.debug('loading persistence from {}'.format(fpath))
        try:
            persistence = rw.load(fpath)
        except FileNotFoundError:
            logging.warning('FileNotFoundError while loading {}'.format(fpath))
        except IsADirectoryError:
            logging.warning('IsADirectoryError while loading {}'.format(fpath))
        except IOError:
            logging.warning('IOError while loading {}'.format(fpath))
        else:
            logging.info('loaded persistence from {}'.format(fpath))
            self.load_settings(persistence['settings_fname'], sofb_stati)

    def save_persistence(self, fname='sofb.persistent'):
        """The persistence simply is the name of a settings file."""
        fpath = path.join(self.configs.persistence_path, fname)
        logging.debug('saving persistence to {}'.format(fpath))
        try:
            rw.save({'settings_fname': self.settings.settings_fname}, fpath)
        except FileExistsError:
            logging.warning('FileExistsError while writing {}'.format(fpath))
        except IsADirectoryError:
            logging.warning('IsADirectoryError while writing {}'.format(fpath))
        except IOError:
            logging.warning('IOError while writing {}'.format(fpath))
        else:
            logging.info('saved persistence to {}'.format(fpath))

    def load_response(self, fname, sofb_stati):
        fpath = path.join(self.configs.response_path, fname)
        logging.debug('loading R_FK and norm from {}'.format(fpath))
        try:
            self.parameters.R_FK[:, :], self.parameters.Rd_F[:], self.parameters.sig_R_FK[:, :], self.parameters.norm = rw.load_response(fpath, corr_idstr_K=self.configs.ar_cor['idstr'], bpm_idstr_F=self.configs.ar_bpm['idstr'])

        except FileNotFoundError:
            logging.warning('FileNotFoundError while loading {}'.format(fpath))
            sofb_stati.set('response', ErrorCodes.ERROR, 'FileNotFoundError')
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'FileNotFoundError')
        except IsADirectoryError:
            logging.warning('IsADirectoryError while loading {}'.format(fpath))
            sofb_stati.set('response', ErrorCodes.ERROR, 'IsADirectoryError')
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IsADirectoryError')
        except IOError:
            logging.warning('IOError while loading {}'.format(fpath))
            sofb_stati.set('response', ErrorCodes.ERROR, 'IOError')
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IOError')
        else:
            self.settings.response_fname = fname
            sofb_stati.set('response', ErrorCodes.OK, fname)
            sofb_stati.set('cmd', ErrorCodes.OK, 'loaded {}'.format(fname))
            sofb_stati.add_status_msg('loaded {}'.format(fname))

    def save_response(self, fname, sofb_stati):
        fpath=path.join(self.configs.response_path, fname)
        try:
            rw.save_response(R_FK=self.parameters.R_FK,
                             Rd_F=self.parameters.Rd_F,
                             sig_R_FK=self.parameters.sig_R_FK,
                             bpm_idstr_F=self.configs.ar_bpm['idstr'],
                             corr_idstr_K=self.configs.ar_cor['idstr'],
                             norm=self.parameters.norm,
                             saw_flag=None, fuertsch_flag=None, q_M=None,
                             path=path.join(self.configs.response_path, fname))
        except FileExistsError:
            logging.warning('FileExistsError while writing {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'FileExistsError')
        except IsADirectoryError:
            logging.warning('IsADirectoryError while writing {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IsADirectoryError')
        except IOError:
            logging.warning('IOError while writing {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IOError')
        else:
            self.settings.reference_fname = fname
            sofb_stati.set('cmd', ErrorCodes.OK, 'saved {}'.format(fname))
            sofb_stati.add_status_msg('saved {}'.format(fname))
            sofb_stati.set('response', ErrorCodes.OK, fname)

    def load_reference(self, fname, sofb_stati, check_subdir=True):
        _check_subdir = False
        fpath = path.join(self.configs.reference_path, fname)
        logging.debug('loading kap_ref_F and W_FF from {}'.format(fpath))
        try:
            self.parameters.kap_ref_F[:], self.parameters.W_FF[:, :], use_F = rw.load_reference(fpath, bpm_idstr_F=self.configs.ar_bpm['idstr'])
            # self.parameters.W_FF = np.sqrt(self.parameters.W_FF)
            self.parameters.kap_ref_original_F[:] = np.copy(self.parameters.kap_ref_F)
            self.parameters.W_original_FF[:] = np.copy(self.parameters.W_FF)
        except FileNotFoundError:
            if check_subdir:
                _check_subdir = True
            logging.warning('FileNotFoundError while loading {}'.format(fpath))
            sofb_stati.set('reference', ErrorCodes.ERROR, 'FileNotFoundError')
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'FileNotFoundError')
        except IsADirectoryError:
            logging.warning('IsADirectoryError while loading {}'.format(fpath))
            sofb_stati.set('reference', ErrorCodes.ERROR, 'IsADirectoryError')
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IsADirectoryError')
        except IOError:
            logging.warning('IOError while loading {}'.format(fpath))
            sofb_stati.set('reference', ErrorCodes.ERROR, 'IOError')
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IOError')
        else:
            self.settings.reference_fname = fname
            if use_F is not None:
                self.settings.use_F[:] = use_F
                logging.info('loaded kap_ref_F, W_FF and use_F from {}'.format(fpath))
                sofb_stati.set('reference', ErrorCodes.OK, fname)
                sofb_stati.set('cmd', ErrorCodes.OK, 'loaded {}'.format(fname))
                sofb_stati.add_status_msg('loaded {}'.format(fname))
            else:
                logging.info('loaded kap_ref_F and W_FF from {}'.format(fpath))
                sofb_stati.set('reference', ErrorCodes.PROBLEM, fname)
                sofb_stati.set('cmd', ErrorCodes.OK, 'loaded {} without use_F'.format(fname))
                sofb_stati.add_status_msg('loaded {} without use_F'.format(fname))
        if _check_subdir:
            self.load_reference(path.join('sofb', fname), sofb_stati, False)

    def save_reference(self, fname, sofb_stati):
        fpath = path.join(self.configs.reference_path, 'sofb', path.basename(fname))
        logging.debug('saving kap_ref_F, W_FF and use_F to {}'.format(fpath))
        try:
            rw.save_reference(kap_ref_F=np.copy(self.parameters.kap_ref_F),
                              W_FF=np.copy(self.parameters.W_FF),
                              use_F=np.copy(self.settings.use_F),
                              bpm_idstr_F=self.configs.ar_bpm['idstr'],
                              path=fpath)
        except FileExistsError:
            logging.warning('FileExistsError while writing {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'FileExistsError')
        except IsADirectoryError:
            logging.warning('IsADirectoryError while writing {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IsADirectoryError')
        except IOError:
            logging.warning('IOError while writing {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.ERROR, 'IOError')
        else:
            self.settings.reference_fname = path.join('sofb', path.basename(fname))
            logging.info('saved kap_ref_F, W_FF and use_F to {}'.format(fpath))
            sofb_stati.set('cmd', ErrorCodes.OK, 'saved {}'.format(fname))
            sofb_stati.add_status_msg('saved {}'.format(fname))
            sofb_stati.set('reference', ErrorCodes.OK, fname)



