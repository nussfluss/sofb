import json
import threading
import signal
import sys
from os import path
import os
import traceback
import logging
import multiprocessing
import copy
from datetime import datetime
from enum import Enum
import numpy as np
import time
import channel_access.server as ca_server
import channel_access.common as ca
# logging.getLogger('ca_server').setLevel(logging.WARNING)

from . import SOFBController, correction_guess, check_orbit, plot_key_quantities, orbcor
from .status import ErrorCodes, SOFBStati
from . import rw
from .model import SOFBModel
from .history import SOFBHistory


class CMDs(Enum):
    STANDBY = 0
    CORRECT = 1
    STEP_BACK = 2
    FLATTEN_INJ_BUMP = 3
    MEAS_DISP = 4


class SubCMDs(Enum):
    STANDBY = 0
    AUTOSAVE_SETTINGS = 1
    AUTOSAVE_REFERENCE = 2
    AUTOSAVE_RESPONSE = 3
    RESTORE_REFERENCE = 4


class ReferenceCMDs(Enum):
    STANDBY = 0
    ZERO_ORBIT_X = 1
    ZERO_ORBIT_Y = 2
    ONE_WEIGHTS_X = 3
    ONE_WEIGHTS_Y = 4
    ASSUME_ORBIT_X = 5
    ASSUME_ORBIT_Y = 6


class Modes(Enum):
    STANDBY = 0
    CORRECTION = 1
    NOISE = 2
    ORBIT_BUMP = 3
    MEAS_DISP = 4


class PlotReference(Enum):
    NONE = 0
    STANDARD = 1
    CUSTOM = 2

class PlotXAxis(Enum):
    BPM_NUMBER = 0
    BPM_POSITION = 1

def serve_correction_guess(pipe, log_level):
    logging.getLogger().setLevel(log_level)
    logging.debug('Subprocess started. Verbosity level is {}.'.format(logging.getLevelName(log_level)))
    try:
        while True:
            machine_status, corr_status, bpm_status, orbit_status, parameters, settings, configs = pipe.recv()
            logging.debug('Subprocess triggered ...')
            if machine_status and corr_status and bpm_status and orbit_status and parameters and settings and configs:
                opt_results = correction_guess(machine_status=machine_status,
                                               corr_status=corr_status,
                                               bpm_status=bpm_status,
                                               orbit_status=orbit_status,
                                               parameters=parameters,
                                               settings=settings,
                                               configs=configs)
                pipe.send(opt_results)
    except (EOFError, KeyboardInterrupt):
        logging.info('Subprocess shut down.')
        return


def update_model(pv_dict, sofb_stati, sofb_model):

    F = sofb_model.configs.F
    Jx = sofb_model.configs.Jx
    Jy = sofb_model.configs.Jy
    K = sofb_model.configs.K
    Kh = sofb_model.configs.Kh
    Kv = sofb_model.configs.Kv

    # settings
    # --------
    sofb_model.settings.sigmas_to_remove_h = pv_dict['set:sigrem:x'].value
    sofb_model.settings.sigmas_to_remove_v = pv_dict['set:sigrem:y'].value
    sofb_model.settings.decoupled_flag = not bool(pv_dict['set:coupled'].value)
    sofb_model.settings.dispersion_correction = pv_dict['set:disp:correction'].value
    sofb_model.settings.factor = pv_dict['set:factor'].value
    sofb_model.settings.limit_err_kap = pv_dict['set:limiterrkap'].value
    sofb_model.settings.weighted_rms_threshold_nostep_x = pv_dict['set:wrms:threshold:nostep:x'].value
    sofb_model.settings.weighted_rms_threshold_nostep_y = pv_dict['set:wrms:threshold:nostep:y'].value
    sofb_model.settings.weighted_rms_threshold_micado_x = pv_dict['set:wrms:threshold:micado:x'].value
    sofb_model.settings.weighted_rms_threshold_micado_y = pv_dict['set:wrms:threshold:micado:y'].value
    sofb_model.settings.I_beam_min = pv_dict['set:Ibeam:min'].value
    sofb_model.settings.remove_pincushion_dist = bool(pv_dict['set:prem'].value)
    sofb_model.settings.minimize_currents = bool(pv_dict['set:remlindep'].value)
    sofb_model.settings.t_sleep = pv_dict['set:tsleep'].value
    sofb_model.settings.buf_len = pv_dict['set:buflen'].value
    sofb_model.settings.Delta_I_cut = pv_dict['set:corrs:DeltaI:cut'].value
    sofb_model.settings.Delta_I_max = pv_dict['set:corrs:DeltaI:max'].value
    sofb_model.settings.Delta_phi_cut = 1e-3*pv_dict['set:corrs:Deltaphi:cut'].value
    sofb_model.settings.method = 'qpcone'
    use_Jx = pv_dict['set:bpms:use:x'].value
    use_Jy = pv_dict['set:bpms:use:y'].value
    sofb_model.settings.use_F[:] = np.concatenate((use_Jx, use_Jy))
    use_Kh = pv_dict['set:corrs:use:x'].value
    use_Kv = pv_dict['set:corrs:use:y'].value
    sofb_model.settings.use_K[:] = np.concatenate((use_Kh, use_Kv))
    sofb_model.settings.Delta_f = pv_dict['set:disp:Deltaf'].value
    if not pv_dict['set:settings:fname:load'].value == '':
        sofb_model.load_settings(pv_dict['set:settings:fname:load'].value, sofb_stati)
        sofb_model.load_reference(sofb_model.settings.reference_fname, sofb_stati)
        sofb_model.load_response(sofb_model.settings.response_fname, sofb_stati)
        update_settings_pvs(pv_dict, sofb_model.parameters, sofb_model.settings, sofb_model.configs)
        pv_dict['settings:fname'].value = sofb_model.settings.settings_fname
        pv_dict['reference:fname'].value = sofb_model.settings.reference_fname
        pv_dict['response:fname'].value = sofb_model.settings.response_fname
    if not pv_dict['set:reference:fname:load'].value == '':
        sofb_model.load_reference(pv_dict['set:reference:fname:load'].value, sofb_stati)
        update_settings_pvs(pv_dict, sofb_model.parameters, sofb_model.settings, sofb_model.configs)
        pv_dict['reference:fname'].value = sofb_model.settings.reference_fname
    if not pv_dict['set:response:fname:load'].value == '':
        sofb_model.load_response(pv_dict['set:response:fname:load'].value, sofb_stati)
        update_settings_pvs(pv_dict, sofb_model.parameters, sofb_model.settings, sofb_model.configs)
        pv_dict['response:fname'].value = sofb_model.settings.response_fname
    if not pv_dict['set:settings:fname:save'].value == '':
        sofb_model.save_settings(pv_dict['set:settings:fname:save'].value, sofb_stati)
        update_settings_pvs(pv_dict, sofb_model.parameters, sofb_model.settings, sofb_model.configs)
        pv_dict['settings:fname'].value = sofb_model.settings.settings_fname
    if not pv_dict['set:reference:fname:save'].value == '':
        sofb_model.save_reference(pv_dict['set:reference:fname:save'].value, sofb_stati)
        update_settings_pvs(pv_dict, sofb_model.parameters, sofb_model.settings, sofb_model.configs)
        pv_dict['reference:fname'].value = sofb_model.settings.reference_fname
    if not pv_dict['set:response:fname:save'].value == '':
        sofb_model.save_response(pv_dict['set:response:fname:save'].value, sofb_stati)
        update_settings_pvs(pv_dict, sofb_model.parameters, sofb_model.settings, sofb_model.configs)
        pv_dict['response:fname'].value = sofb_model.settings.response_fname

    # parameters
    # ----------
    sofb_model.parameters.kap_ref_F[:Jx] = pv_dict['set:bpms:ref:x'].value
    sofb_model.parameters.kap_ref_F[Jx:] = pv_dict['set:bpms:ref:y'].value
    W_Jx = pv_dict['set:bpms:weights:x'].value
    W_Jy = pv_dict['set:bpms:weights:y'].value
    sofb_model.parameters.W_FF[:, :] = np.diag(np.concatenate((W_Jx, W_Jy), axis=0))
    constraint_J2 = np.zeros((Jx, 2), np.float64)
    kap_ref_J = pv_dict['set:bpms:ref:x'].value
    aperture_J2 = np.zeros((Jx, 2), np.float64)
    aperture_J2[:, 0] = pv_dict['set:bpms:min:x'].value
    aperture_J2[:, 1] = pv_dict['set:bpms:max:x'].value
    for j in range(Jx):
        constraint_J2[j] = aperture_J2[j]
    sofb_model.parameters.constraint_F2[:Jx] = constraint_J2
    constraint_J2 = np.zeros((Jy, 2), np.float64)
    kap_ref_J = pv_dict['set:bpms:ref:y'].value
    aperture_J2 = np.zeros((Jy, 2), np.float64)
    aperture_J2[:, 0] = pv_dict['set:bpms:min:y'].value
    aperture_J2[:, 1] = pv_dict['set:bpms:max:y'].value
    for j in range(Jy):
        constraint_J2[j] = aperture_J2[j]
    sofb_model.parameters.constraint_F2[Jx:] = constraint_J2


def update_settings_pvs(pv_dict, parameters, settings, configs):
    Kh = configs.Kh
    Jx = configs.Jx
    F = configs.F

    # bpms
    pv_dict['set:bpms:use:x'].value = settings.use_F[:Jx]
    pv_dict['set:bpms:use:y'].value = settings.use_F[Jx:]
    pv_dict['set:bpms:ref:x'].value = parameters.kap_ref_F[:Jx]
    pv_dict['set:bpms:ref:y'].value = parameters.kap_ref_F[Jx:]
    pv_dict['set:bpms:weights:x'].value = np.diag(parameters.W_FF)[:Jx]
    pv_dict['set:bpms:weights:y'].value = np.diag(parameters.W_FF)[Jx:]
    mode_F = np.zeros((F,), int)
    aperture_F2 = np.zeros((F, 2), np.float64)
    constraint_F2 = parameters.constraint_F2
    kap_ref_F = parameters.kap_ref_F
    for f in range(F):
        if np.isneginf(constraint_F2[f, 0]) and np.isposinf(constraint_F2[f, 1]):
            # unconstrained
            mode_F[f] = 0
            aperture_F2[f] = -20.0, 20.0
        elif abs(constraint_F2[f, 0] - constraint_F2[f, 1]) < 0.006:
            # locked
            mode_F[f] = 1
            aperture_F2[f] = constraint_F2[f]
        else:
            # constrained
            mode_F[f] = 2
            aperture_F2[f] = constraint_F2[f]
    pv_dict['set:bpms:min:x'].value = aperture_F2[:Jx, 0]
    pv_dict['set:bpms:max:x'].value = aperture_F2[:Jx, 1]
    pv_dict['set:bpms:min:y'].value = aperture_F2[Jx:, 0]
    pv_dict['set:bpms:max:y'].value = aperture_F2[Jx:, 1]

    # corrs
    pv_dict['set:corrs:use:x'].value = settings.use_K[:Kh]
    pv_dict['set:corrs:use:y'].value = settings.use_K[Kh:]

    # misc
    pv_dict['set:settings:fname:save'].value = ''
    pv_dict['set:reference:fname:save'].value = ''
    pv_dict['set:response:fname:save'].value = ''
    pv_dict['set:settings:fname:load'].value = ''
    pv_dict['set:reference:fname:load'].value = ''
    pv_dict['set:response:fname:load'].value = ''
    pv_dict['set:corrs:DeltaI:cut'].value = settings.Delta_I_cut
    pv_dict['set:corrs:DeltaI:max'].value = settings.Delta_I_max
    pv_dict['set:corrs:Deltaphi:cut'].value = 1e3*settings.Delta_phi_cut
    pv_dict['set:Ibeam:min'].value = settings.I_beam_min
    pv_dict['set:factor'].value = settings.factor
    pv_dict['set:disp:correction'].value = settings.dispersion_correction
    pv_dict['set:tsleep'].value = settings.t_sleep
    pv_dict['set:coupled'].value = not settings.decoupled_flag
    pv_dict['set:prem'].value = settings.remove_pincushion_dist
    pv_dict['set:remlindep'].value = settings.minimize_currents
    pv_dict['set:sigrem:x'].value = settings.sigmas_to_remove_h
    pv_dict['set:sigrem:y'].value = settings.sigmas_to_remove_v
    pv_dict['set:wrms:threshold:nostep:x'].value = settings.weighted_rms_threshold_nostep_x
    pv_dict['set:wrms:threshold:nostep:y'].value = settings.weighted_rms_threshold_nostep_y
    pv_dict['set:wrms:threshold:micado:x'].value = settings.weighted_rms_threshold_micado_x
    pv_dict['set:wrms:threshold:micado:y'].value = settings.weighted_rms_threshold_micado_y
    pv_dict['set:buflen'].value = settings.buf_len
    pv_dict['set:disp:Deltaf'].value = settings.Delta_f


def update_status_pvs(pv_dict, sofb_stati, orbit_status, bpm_status, corr_status, sofb_model, sofb_history, kap_ui_F, kap_ref_ui_F):

    Jx = sofb_model.configs.Jx
    Kh = sofb_model.configs.Kh

    # sofb
    if not np.allclose(sofb_model.parameters.kap_ref_F, sofb_model.parameters.kap_ref_original_F, atol=1e-4) or \
       not np.allclose(sofb_model.parameters.W_FF.diagonal(), sofb_model.parameters.W_original_FF.diagonal(), atol=1e-2):
        sofb_stati.set('reference', ErrorCodes.OK, '{} (altered)'.format(sofb_model.settings.reference_fname))
    status_reference = sofb_stati.get('reference')
    pv_dict['status:reference'].value = status_reference.status.value
    pv_dict['statusstr:reference'].value = status_reference.statusstr
    pv_dict['errcount:reference'].value = status_reference.error_count
    status_response = sofb_stati.get('response')
    pv_dict['status:response'].value = status_response.status.value
    pv_dict['statusstr:response'].value = status_response.statusstr
    pv_dict['errcount:response'].value = status_response.error_count
    status_laststep = sofb_stati.get('laststep')
    pv_dict['status:laststep'].value = status_laststep.status.value
    pv_dict['statusstr:laststep'].value = status_laststep.statusstr
    pv_dict['errcount:laststep'].value = status_laststep.error_count
    status_cmd = sofb_stati.get('cmd')
    pv_dict['status:cmd'].value = status_cmd.status.value
    pv_dict['statusstr:cmd'].value = status_cmd.statusstr
    pv_dict['errcount:cmd'].value = status_cmd.error_count
    status_correction = sofb_stati.get('correction')
    pv_dict['status:correction'].value = status_correction.status.value
    pv_dict['statusstr:correction'].value = status_correction.statusstr
    pv_dict['errcount:correction'].value = status_correction.error_count
    status_bpms_x = sofb_stati.get('bpms:x')
    pv_dict['status:bpms:x'].value = status_bpms_x.status.value
    pv_dict['statusstr:bpms:x'].value = status_bpms_x.statusstr
    pv_dict['errcount:bpms:x'].value = status_bpms_x.error_count
    pv_dict['stati:bpms:x'].value = [status.value for status in status_bpms_x.stati]
    status_bpms_y = sofb_stati.get('bpms:y')
    pv_dict['status:bpms:y'].value = status_bpms_y.status.value
    pv_dict['statusstr:bpms:y'].value = status_bpms_y.statusstr
    pv_dict['errcount:bpms:y'].value = status_bpms_y.error_count
    pv_dict['stati:bpms:y'].value = [status.value for status in status_bpms_y.stati]
    status_corrs_h = sofb_stati.get('corrs:x')
    pv_dict['status:corrs:x'].value = status_corrs_h.status.value
    pv_dict['statusstr:corrs:x'].value = status_corrs_h.statusstr
    pv_dict['errcount:corrs:x'].value = status_corrs_h.error_count
    pv_dict['stati:corrs:x'].value = [status.value for status in status_corrs_h.stati]
    status_corrs_v = sofb_stati.get('corrs:y')
    pv_dict['status:corrs:y'].value = status_corrs_v.status.value
    pv_dict['statusstr:corrs:y'].value = status_corrs_v.statusstr
    pv_dict['errcount:corrs:y'].value = status_corrs_v.error_count
    pv_dict['stati:corrs:y'].value = [status.value for status in status_corrs_v.stati]
    status_beam_energy = sofb_stati.get('beam_energy')
    pv_dict['status:beam_energy'].value = status_beam_energy.status.value
    pv_dict['statusstr:beam_energy'].value = status_beam_energy.statusstr
    pv_dict['errcount:beam_energy'].value = status_beam_energy.error_count
    status_beam_current = sofb_stati.get('beam_current')
    pv_dict['status:beam_current'].value = status_beam_current.status.value
    pv_dict['statusstr:beam_current'].value = status_beam_current.statusstr
    pv_dict['errcount:beam_current'].value = status_beam_current.error_count
    status_f = sofb_stati.get('f')
    pv_dict['status:f'].value = status_f.status.value
    pv_dict['statusstr:f'].value = status_f.statusstr
    pv_dict['errcount:f'].value = status_f.error_count

    # bpm status
    pv_dict['kap:x'].value = bpm_status.kap_eff_F[:Jx]
    pv_dict['kap:y'].value = bpm_status.kap_eff_F[Jx:]
    pv_dict['kap:err:x'].value = bpm_status.err_kap_F[:Jx]
    pv_dict['kap:err:y'].value = bpm_status.err_kap_F[Jx:]
    pv_dict['rms:err:x'].value = orbit_status.err_rms_eff_x
    pv_dict['rms:err:y'].value = orbit_status.err_rms_eff_y
    pv_dict['wrms:err:x'].value = orbit_status.err_weighted_rms_eff_x
    pv_dict['wrms:err:y'].value = orbit_status.err_weighted_rms_eff_y
    pv_dict['rms:x'].value = orbit_status.rms_eff_x
    pv_dict['rms:y'].value = orbit_status.rms_eff_y
    pv_dict['wrms:x'].value = orbit_status.weighted_rms_eff_x
    pv_dict['wrms:y'].value = orbit_status.weighted_rms_eff_y

    # corr status
    pv_dict['I:x'].value = corr_status.I_eff_K[:Kh]
    pv_dict['I:y'].value = corr_status.I_eff_K[Kh:]
    pv_dict['I:sum:abs:x'].value = np.sum(np.abs(corr_status.I_K[:Kh]))
    pv_dict['I:sum:abs:y'].value = np.sum(np.abs(corr_status.I_K[Kh:]))
    pv_dict['I:rms:x'].value = np.linalg.norm(corr_status.I_K[:Kh]/np.sqrt(sofb_model.configs.Kh))
    pv_dict['I:rms:y'].value = np.linalg.norm(corr_status.I_K[Kh:]/np.sqrt(sofb_model.configs.Kv))
    pv_dict['I:min:x'].value = corr_status.I_bounds_K2[:Kh, 0]
    pv_dict['I:max:x'].value = corr_status.I_bounds_K2[:Kh, 1]
    pv_dict['I:min:y'].value = corr_status.I_bounds_K2[Kh:, 0]
    pv_dict['I:max:y'].value = corr_status.I_bounds_K2[Kh:, 1]

    # user interface
    pv_dict['ui:kap:x'].value = kap_ui_F[:Jx]
    pv_dict['ui:kap:y'].value = kap_ui_F[Jx:]
    pv_dict['ui:kap:ref:x'].value = kap_ref_ui_F[:Jx]
    pv_dict['ui:kap:ref:y'].value = kap_ref_ui_F[Jx:]
    pv_dict['ui:disp:x'].value = sofb_model.parameters.Rd_F[:Jx]
    pv_dict['ui:disp:y'].value = sofb_model.parameters.Rd_F[Jx:]

    # correction-step data
    data = sofb_history.get_data_of_last_correction_step(sofb_model.settings)
    if data is not None:
        timestamp, Delta_kap_F, Delta_phi_K = data
        # pv_dict['step:timestamp'].value = timestamp
        pv_dict['step:Deltakap:x'].value = Delta_kap_F[:Jx]
        pv_dict['step:Deltakap:y'].value = Delta_kap_F[Jx:]
        pv_dict['step:Deltaphi:x'].value = Delta_phi_K[:Kh]
        pv_dict['step:Deltaphi:y'].value = Delta_phi_K[Kh:]

class SOFBDriver(object):

    def __init__(self, configs, *args, **kwargs):

        self._kap_subs_ui_F = np.zeros(configs.F, np.float64)
        self._mode = Modes.STANDBY
        self._finalize_mode = False
        self._initialize_mode = False
        self._plot_reference = PlotReference.NONE
        self._step_counter = 0
        self._active_flag = False
        self._auto_correct = 0


        # subprocess
        ctx = multiprocessing.get_context('spawn')
        self.pipe_parent, pipe_child = ctx.Pipe()
        log_level = logging.getLevelName(logging.getLogger().getEffectiveLevel())
        self.opt_prog = ctx.Process(target=serve_correction_guess, args=(pipe_child, log_level), daemon=True)
        self.opt_prog.start()

        logging.debug('SOFBDriver initialized.')

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        self._mode = value
        self._initialize_mode = True

    def process(self, pv_dict, sofb_stati, sofb_model, sofb_history, sofb_controller):
        """main loop"""

        # check steereres and update model
        sofb_controller.hw.corrs.check()
        update_model(pv_dict, sofb_stati, sofb_model)

        # -------------
        # monitor stati
        # -------------
        status = sofb_controller.check(parameters=sofb_model.parameters, settings=sofb_model.settings, configs=sofb_model.configs, sofb_stati=sofb_stati)
        sofb_history.append_status(status, sofb_model.settings)

        # turn off autocorrection if beam current is below threshold
        if status.machine_status.beam_current_below_threshold_flag:
            pv_dict['set:autocorrect'].value = 0

        # ----------------
        # process commands
        # ----------------
        sub_cmd_int = pv_dict['subcmd'].value
        try:
            sub_cmd = SubCMDs(sub_cmd_int)
        except ValueError:
            logging.warning('sub cmd not valid!')
            sub_cmd = SubCMDs.STANDBY
        if sub_cmd is SubCMDs.AUTOSAVE_SETTINGS:
            sofb_model.save_settings('settings.' + datetime.utcnow().strftime('%y%m%d-%H:%M:%S'), sofb_stati)
            pv_dict['settings:fname'].value = sofb_model.settings.settings_fname
        elif sub_cmd is SubCMDs.AUTOSAVE_REFERENCE:
            sofb_model.save_reference('reference.' + datetime.utcnow().strftime('%y%m%d-%H:%M:%S'), sofb_stati)
            pv_dict['reference:fname'].value = sofb_model.settings.reference_fname
        elif sub_cmd is SubCMDs.AUTOSAVE_RESPONSE:
            sofb_model.save_response('response.' + datetime.utcnow().strftime('%y%m%d-%H:%M:%S'), sofb_stati)
            pv_dict['response:fname'].value = sofb_model.settings.response_fname
        elif sub_cmd is SubCMDs.RESTORE_REFERENCE:
            sofb_model.load_reference(sofb_model.settings.reference_fname, sofb_stati)
            pv_dict['reference:fname'].value = sofb_model.settings.reference_fname

        ref_cmd_int = pv_dict['refcmd'].value
        try:
            ref_cmd = ReferenceCMDs(ref_cmd_int)
        except ValueError:
            logging.warning('reference cmd not valid!')
            ref_cmd = ReferenceCMDs.STANDBY
        if ref_cmd is ReferenceCMDs.ZERO_ORBIT_X:
            kap_ref_new_Jx = np.zeros(sofb_model.configs.Jx)
            pv_dict['set:bpms:ref:x'].value = kap_ref_new_Jx
            sofb_stati.set('refcmd', ErrorCodes.OK, 'Set horizontal zero orbit.')
        elif ref_cmd is ReferenceCMDs.ZERO_ORBIT_Y:
            kap_ref_new_Jy = np.zeros(sofb_model.configs.Jy)
            pv_dict['set:bpms:ref:y'].value = kap_ref_new_Jy
            sofb_stati.set('refcmd', ErrorCodes.OK, 'Set vertical zero orbit.')
        elif ref_cmd is ReferenceCMDs.ONE_WEIGHTS_X:
            W_new_Jx = np.ones(sofb_model.configs.Jx)
            pv_dict['set:bpms:weights:x'].value = W_new_Jx
            sofb_stati.set('refcmd', ErrorCodes.OK, 'Set horizontal weights to one.')
        elif ref_cmd is ReferenceCMDs.ONE_WEIGHTS_Y:
            W_new_Jy = np.ones(sofb_model.configs.Jy)
            pv_dict['set:bpms:weights:y'].value = W_new_Jy
            sofb_stati.set('refcmd', ErrorCodes.OK, 'Set vertical weights to one.')
        elif ref_cmd is ReferenceCMDs.ASSUME_ORBIT_X:
            pv_dict['set:bpms:ref:x'].value = status.bpm_status.kap_F[:sofb_model.configs.Jx]
            sofb_stati.set('refcmd', ErrorCodes.OK, 'Referenced horizontal orbit.')
        elif ref_cmd is ReferenceCMDs.ASSUME_ORBIT_Y:
            pv_dict['set:bpms:ref:y'].value = status.bpm_status.kap_F[sofb_model.configs.Jx:]
            sofb_stati.set('refcmd', ErrorCodes.OK, 'Referenced vertical orbit.')


        cmd_int = pv_dict['cmd'].value
        try:
            cmd = CMDs(cmd_int)
        except ValueError:
            logging.warning('cmd not valid!')
            cmd = CMDs.STANDBY
        if cmd is CMDs.CORRECT:
            self.mode = Modes.CORRECTION
            sofb_stati.set('cmd', ErrorCodes.OK, 'Orbit correction initiated.')

        elif cmd is CMDs.STEP_BACK:
            if self.mode is Modes.STANDBY:
                I_previous_K = sofb_history.get_previous_steerer_currents()
                if I_previous_K is not None:
                    sofb_controller.hw.corrs.put_I_K(I_previous_K, unblock_after_ramp=True)
                    sofb_stati.set('cmd', ErrorCodes.OK, 'Step back initiated.')
                    sofb_stati.add_status_msg('Stepped back.')
                else:
                    sofb_stati.set('cmd', ErrorCodes.ERROR, 'Step back unavailable. Empty list.')
            else:
                sofb_stati.set('cmd', ErrorCodes.ERROR, 'Step back unavailable. Program busy.')
                logging.error('Busy. Cannot take a step back right now.')

        elif cmd is CMDs.FLATTEN_INJ_BUMP:
            # 1. set horizontal reference orbit in injection area to zero
            # 2. set horizontal weights in injection area to one
            kap_ref_new_Jx = sofb_model.parameters.kap_ref_F[:sofb_model.configs.Jx]
            kap_ref_new_Jx[23:31] = np.zeros((31-23,), np.float64)
            pv_dict['set:bpms:ref:x'].value = kap_ref_new_Jx
            W_new_Jx = np.copy(np.diag(sofb_model.parameters.W_FF[:sofb_model.configs.Jx, :sofb_model.configs.Jx]))
            W_new_Jx[23:31] = np.ones((31-23,), np.float64)
            pv_dict['set:bpms:weights:x'].value = W_new_Jx
            sofb_stati.set('cmd', ErrorCodes.OK, 'Altered reference in injection area.')

        elif cmd is CMDs.MEAS_DISP:
            self.mode = Modes.MEAS_DISP
            sofb_stati.set('cmd', ErrorCodes.OK, 'Dispersion measurement initiated.')
            sofb_model.parameters.Rd_F = sofb_controller.meas_disp(pv_dict['set:disp:Deltaf'].value)
            sofb_stati.add_status_msg('Measured dispersion.')

        # autocorrect
        # - not a command because Andres UI widgets work differently than OPI
        if pv_dict['set:autocorrect'].value == 1 and self._auto_correct == 0:
            self.mode = Modes.CORRECTION
            sofb_stati.set('cmd', ErrorCodes.OK, 'Entered auto correction mode.')
            self._auto_correct = 1
            sofb_stati.add_status_msg('started auto correction')
        elif pv_dict['set:autocorrect'].value == 0 and self._auto_correct == 1:
            sofb_stati.set('cmd', ErrorCodes.OK, 'Exiting auto correction mode.')
            sofb_stati.add_status_msg('stopped auto correction')
            self._auto_correct = 0
            sofb_controller.hw.corrs.unblock()


        # --------------------
        # prepare orbit for UI
        # --------------------
        #  - contains only BPMs which are in use
        #  - BPMs which have been excluded from analysis due to other reasons are still displayed (prevents orbit jitter)
        map_inuse_Feff = []
        for f in range(sofb_model.configs.F):
            if sofb_model.settings.use_F[f] == 1:
                map_inuse_Feff.append(f)
        kap_inuse_ui_F = np.zeros(sofb_model.configs.F, np.float64)
        plot_reference = PlotReference(pv_dict['set:ui:plot:ref'].value)
        if plot_reference is PlotReference.NONE:
            self._kap_subs_ui_F = np.zeros((sofb_model.configs.F,), np.float64)
        elif plot_reference is PlotReference.STANDARD:
            self._kap_subs_ui_F = np.copy(sofb_model.parameters.kap_ref_F)
        elif plot_reference is PlotReference.CUSTOM:
            if self._plot_reference is not plot_reference:
                self._kap_subs_ui_F = np.copy(status.bpm_status.kap_F)
        kap_inuse_ui_F[map_inuse_Feff] = status.bpm_status.kap_F[map_inuse_Feff] - self._kap_subs_ui_F[map_inuse_Feff]
        kap_ref_inuse_ui_F = np.zeros(sofb_model.configs.F, np.float64)
        if plot_reference is PlotReference.NONE:
            kap_ref_inuse_ui_F[map_inuse_Feff] = sofb_model.parameters.kap_ref_F[map_inuse_Feff]
        self._plot_reference = plot_reference
        if PlotXAxis(pv_dict['set:ui:plot:xaxis'].value) is PlotXAxis.BPM_POSITION:
            pv_dict['ui:xaxis'].value = sofb_model.configs.ar_bpm['spos']
        else:
            pv_dict['ui:xaxis'].value = np.asarray(np.arange(sofb_model.configs.Jx) + 1, float)

        # -------------
        # process modes
        # -------------
        if self.mode is Modes.CORRECTION:

            # correct orbit
            # * * * * * * *

            # initialize modie
            if self._initialize_mode:
                self._active_flag = True
                self._initialize_mode = False
                if status.correction_available:
                    if self.opt_prog.is_alive:
                        self.pipe_parent.send([status.machine_status, status.corr_status, status.bpm_status, status.orbit_status, sofb_model.parameters, sofb_model.settings, sofb_model.configs])
                    else:
                        sofb_stati.set('laststep', ErrorCodes.ERROR, 'dead subproc: RESTART REQUIRED!')
                        self._finalize_mode = True
                else:
                    self._finalize_mode = True

            # do stuff
            if self.pipe_parent.poll():
                opt_results = self.pipe_parent.recv()
                if isinstance(opt_results, orbcor.CorrectionFailedError):
                    sofb_stati.set('laststep', ErrorCodes.OK, 'Looking for suitable steerers.')
                elif opt_results is not None:
                    if opt_results:
                        if pv_dict['set:autocorrect'].value:
                            correction_status = sofb_controller.correct_orbit(opt_results=opt_results,
                                                                              status=status,
                                                                              settings=sofb_model.settings,
                                                                              configs=sofb_model.configs,
                                                                              sofb_stati=sofb_stati,
                                                                              unblock_after_ramp=False)
                        else:
                            correction_status = sofb_controller.correct_orbit(opt_results=opt_results,
                                                                              status=status,
                                                                              settings=sofb_model.settings,
                                                                              configs=sofb_model.configs,
                                                                              sofb_stati=sofb_stati,
                                                                              unblock_after_ramp=True)
                        if correction_status is not None:
                            if correction_status.steerer_currents_set_flag:
                                if correction_status.frequency_set_flag is None or correction_status.frequency_set_flag:
                                    sofb_history.append_correction_status(correction_status, sofb_model.settings)
                                    sofb_stati.set('laststep', ErrorCodes.OK, 'OK')
                                    sofb_stati.add_status_msg('Corrected orbit.')
                                    self._step_counter += 1
                                else:
                                    sofb_stati.set('laststep', ErrorCodes.ERROR, 'bo-rfmaster-f:set timeout.')
                            else:
                                sofb_stati.set('laststep', ErrorCodes.ERROR, 'Steerer or ramp timeout.')
                        else:
                            sofb_stati.set('laststep', ErrorCodes.OK, 'Temporarily unavailable.')
                    else:
                        sofb_stati.set('laststep', ErrorCodes.OK, 'WRMS is fine. Nothing to do.')
                else:
                    sofb_stati.set('laststep', ErrorCodes.ERROR, 'calc failed. Unknown error.')
                self._finalize_mode = True

            # break condition
            if self._finalize_mode:
                self._finalize_mode = False
                if bool(pv_dict['set:autocorrect'].value):
                    self._initialize_mode = True
                else:
                    self._active_flag = False
                    self.mode = Modes.STANDBY
                    sofb_stati.set('cmd', ErrorCodes.OK, 'Finished orbit correction.')

        elif self.mode is Modes.ORBIT_BUMP:

            # drive bump
            # * * * * *

            pass


        # -------------
        # update status
        # -------------
        update_status_pvs(pv_dict, sofb_stati, status.orbit_status, status.bpm_status, status.corr_status, sofb_model, sofb_history, kap_ui_F=kap_inuse_ui_F, kap_ref_ui_F=kap_ref_inuse_ui_F)
        pv_dict['status:sum'].value = sofb_stati.sofb_status.value
        pv_dict['cmd'].value = CMDs.STANDBY.value
        pv_dict['subcmd'].value = SubCMDs.STANDBY.value
        pv_dict['refcmd'].value = ReferenceCMDs.STANDBY.value
        pv_dict['stepcounter'].value = self._step_counter
        pv_dict['active'].value = int(self._active_flag)
        msg = sofb_stati.get_status_msg()
        if msg is not None:
            pv_dict['status:msg'].value = msg

        time.sleep(0.1)


def main(debug_flag, dr_flag, unarm_flag, config_fpath, list_pvs_flag, record_name_prefix='SK', **kwargs):

    # load general configs
    # --------------------
    config_path = path.split(config_fpath)[0] + '/'
    config = rw.load(config_fpath, mode='json')
    persistence_path = config['persistence_path']
    out_path = config['out_path']
    settings_path = config['settings_path']
    reference_path = config['reference_path']
    response_path = config['response_path']
    logging.info('config_path is {}.'.format(config_path))
    logging.info('persistence_path is {}.'.format(persistence_path))
    logging.info('out_path is {}.'.format(out_path))
    logging.info('settings_path is {}.'.format(settings_path))
    logging.info('reference_path is {}.'.format(reference_path))
    logging.info('response_path is {}.'.format(response_path))

    # status
    # ------
    sofb_stati = SOFBStati()
    sofb_stati.register('cmd')
    sofb_stati.register('refcmd')
    sofb_stati.set('refcmd', ErrorCodes.OK, 'nothing yet')

    # model
    # -----
    # -> load parameters, settings and configs
    sofb_model = SOFBModel(sofb_stati=sofb_stati, debug_flag=debug_flag, unarm_flag=unarm_flag, dr_flag=dr_flag,
                           config_path=config_path, settings_path=settings_path, persistence_path=persistence_path, out_path=out_path,
                           reference_path=reference_path, response_path=response_path)

    # history
    # -------
    sofb_history = SOFBHistory()

    # controller
    # ----------
    # -> establish client-side epics interface
    sofb_controller = SOFBController(configs=sofb_model.configs, sofb_stati=sofb_stati)

    # start driver
    logging.debug('Starting driver ...')
    sofb_driver = SOFBDriver(sofb_model.configs)
    logging.debug('Driver started.')

    # controller backend
    # ------------------
    # -> establish server-side epics interface
    F = sofb_model.configs.F
    Jx = sofb_model.configs.Jx
    Jy = sofb_model.configs.Jy
    K = sofb_model.configs.K
    Kh = sofb_model.configs.Kh
    Kv = sofb_model.configs.Kv
    server_prefix = '{}-sofb-'.format(record_name_prefix)
    logging.debug('Starting server ...')
    with ca_server.Server() as server:
        pv_dict = {}
        pv_dict.update({'cmd': server.createPV(server_prefix + 'cmd', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'display_limits': (0, 6), 'control_limits': (0, 6), 'enum_strings': CMDs._member_names_})})  # command
        pv_dict.update({'subcmd': server.createPV(server_prefix + 'subcmd', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'display_limits': (0, 6), 'control_limits': (0, 6), 'enum_strings': SubCMDs._member_names_})})  # sub command
        pv_dict.update({'mode': server.createPV(server_prefix + 'mode', ca.Type.SHORT, attributes={'value': 0, 'precision': 0, 'display_limits': (0, 2), 'control_limits': (0, 2), 'enum_strings': Modes._member_names_})})  # mode
        pv_dict.update({'active': server.createPV(server_prefix + 'active', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'enmum_strings': ['True', 'False'], 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flagged if sofb acts on the orbit via correction steps or driving bumps
        pv_dict.update({'stepcounter': server.createPV(server_prefix + 'stepcounter', ca.Type.SHORT, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})  # number of successful steps since startup
        pv_dict.update({'set:Ibeam:min': server.createPV(server_prefix + 'set:Ibeam:min', ca.Type.FLOAT, attributes={'value': 1.0, 'precision': 1, 'unit': '', 'display_limits': (0.1, 9.9), 'control_limits': (0.1, 9.9), 'warning_limits': (1.0, 5.0), 'unit': 'mA'})})  # below this current correction steps are not allowed
        pv_dict.update({'set:factor': server.createPV(server_prefix + 'set:factor', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 1, 'unit': '', 'display_limits': (0.0, 1.0), 'control_limits': (0.0, 1.0)})})  # step factor
        pv_dict.update({'set:limiterrkap': server.createPV(server_prefix + 'set:limiterrkap', ca.Type.FLOAT, attributes={'value': 0.01, 'precision': 3, 'unit': 'mm', 'display_limits': (0.0, 0.05), 'control_limits': (0.0, 0.05)})})  # std deviation at which BPMs are excluded from correction
        pv_dict.update({'subcmd': server.createPV(server_prefix + 'subcmd', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'display_limits': (0, 6), 'control_limits': (0, 6), 'enum_strings': SubCMDs._member_names_})})  # sub command
        pv_dict.update({'refcmd': server.createPV(server_prefix + 'refcmd', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'display_limits': (0, 6), 'control_limits': (0, 6), 'enum_strings': ReferenceCMDs._member_names_})})  # reference command
        pv_dict.update({'set:tsleep': server.createPV(server_prefix + 'set:tsleep', ca.Type.FLOAT, attributes={'value': 5.0, 'precision': 1, 'unit': 's', 'display_limits': (0.5, 9.0), 'control_limits': (0.5, 9.0)})})  # sleep time
        pv_dict.update({'set:coupled': server.createPV(server_prefix + 'set:coupled', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'unit': '', 'enmum_strings': ['True', 'False'], 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flag to use off-diagonal elements of orbit response
        pv_dict.update({'set:prem': server.createPV(server_prefix + 'set:prem', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'unit': '', 'enmum_strings': ['True', 'False'], 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flag to remove pincushion distortion
        pv_dict.update({'set:remlindep': server.createPV(server_prefix + 'set:remlindep', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'unit': '', 'enmum_strings': ['True', 'False'], 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flag to minimize currents in each correction step. Finds uniques solution for linear dependent steerers.
        pv_dict.update({'set:sigrem:x': server.createPV(server_prefix + 'set:sigrem:x', ca.Type.SHORT, attributes={'value': 0, 'presision':0, 'unit': '', 'display_limits': (0, Kh), 'control_limits': (0, Kh)})})  # singular values to remove from R in x plane
        pv_dict.update({'set:sigrem:y': server.createPV(server_prefix + 'set:sigrem:y', ca.Type.SHORT, attributes={'value': 0, 'presision':0, 'unit': '', 'display_limits': (0, Kv), 'control_limits': (0, Kv)})})  # singular values to remove from R in z plane
        pv_dict.update({'set:settings:fname:load': server.createPV(server_prefix + 'set:settings:fname:load', ca.Type.STRING, attributes={'value': sofb_model.settings.settings_fname.strip()})})  # name of settings file to load
        pv_dict.update({'set:reference:fname:load': server.createPV(server_prefix + 'set:reference:fname:load', ca.Type.STRING, attributes={'value': sofb_model.settings.reference_fname.strip()})})  # name of reference file to load
        pv_dict.update({'set:response:fname:load': server.createPV(server_prefix + 'set:response:fname:load', ca.Type.STRING, attributes={'value': sofb_model.settings.response_fname.strip()})})  # name of response file to laod
        pv_dict.update({'set:settings:fname:save': server.createPV(server_prefix + 'set:settings:fname:save', ca.Type.STRING, attributes={'value': sofb_model.settings.settings_fname.strip()})})  # name of settings file to save
        pv_dict.update({'set:reference:fname:save': server.createPV(server_prefix + 'set:reference:fname:save', ca.Type.STRING, attributes={'value': sofb_model.settings.reference_fname.strip()})})  # name of reference file to save
        pv_dict.update({'set:response:fname:save': server.createPV(server_prefix + 'set:response:fname:save', ca.Type.STRING, attributes={'value': sofb_model.settings.response_fname.strip()})})  # name of response file to save
        pv_dict.update({'settings:fname': server.createPV(server_prefix + 'settings:fname', ca.Type.STRING, attributes={'value': sofb_model.settings.settings_fname.strip()})})  # name of loaded settings file
        pv_dict.update({'reference:fname': server.createPV(server_prefix + 'reference:fname', ca.Type.STRING, attributes={'value': sofb_model.settings.reference_fname.strip()})})  # name of loaded reference file
        pv_dict.update({'response:fname': server.createPV(server_prefix + 'response:fname', ca.Type.STRING, attributes={'value': sofb_model.settings.response_fname.strip()})})  # name of loaded response file
        pv_dict.update({'set:buflen': server.createPV(server_prefix + 'set:buflen', ca.Type.SHORT, attributes={'value': 10, 'precision': 0, 'unit': '', 'display_limits': (1, 100), 'control_limits': (1, 100)})})  # buffer length for kap_F
        pv_dict.update({'set:disp:Deltaf': server.createPV(server_prefix + 'set:disp:Deltaf', ca.Type.FLOAT, attributes={'value': 0.001, 'precision': 3, 'unit': 'MHz', 'display_limits': (0.001, 0.003), 'control_limits': (0.001, 0.003)})})  # frequency diff for dispersion measurement
        pv_dict.update({'set:disp:correction': server.createPV(server_prefix + 'set:disp:correction', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'display_limits': (0, 2), 'control_limits': (0, 2), 'enum_strings': orbcor.chi.PathLength._member_names_})})  # correct dispersion
        pv_dict.update({'set:wrms:threshold:nostep:x': server.createPV(server_prefix + 'set:wrms:threshold:nostep:x', ca.Type.FLOAT, attributes={'value': 1.4, 'precision': 3, 'unit': 'mm', 'display_limits': (0.1, 20), 'control_limits': (0.1, 20)})})  # 10 / sqrt(54), thresold of weighted_rms in x plane
        pv_dict.update({'set:wrms:threshold:nostep:y': server.createPV(server_prefix + 'set:wrms:threshold:nostep:y', ca.Type.FLOAT, attributes={'value': 1.4, 'precision': 3, 'unit': 'mm', 'display_limits': (0.1, 20), 'control_limits': (0.1, 20)})})  # 10 / sqrt(54), thresold of weighted_rms in z plane
        pv_dict.update({'set:wrms:threshold:micado:x': server.createPV(server_prefix + 'set:wrms:threshold:micado:x', ca.Type.FLOAT, attributes={'value': 6.8, 'precision': 3, 'unit': 'mm', 'display_limits': (0.1, 20), 'control_limits': (0.1, 20)})})  # 50 / sqrt(54), thresold of weighted_rms in x plane
        pv_dict.update({'set:wrms:threshold:micado:y': server.createPV(server_prefix + 'set:wrms:threshold:micado:y', ca.Type.FLOAT, attributes={'value': 6.8, 'precision': 3, 'unit': 'mm', 'display_limits': (0.1, 20), 'control_limits': (0.1, 20)})})  # 50 / sqrt(54), thresold of weighted_rms in z plane
        pv_dict.update({'set:autocorrect': server.createPV(server_prefix + 'set:autocorrect', ca.Type.ENUM, attributes={'value': 0, 'enmum_strings': ['True', 'False'], 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flag to turn on autocorrect
        pv_dict.update({'set:bpms:use:x': server.createPV(server_prefix + 'set:bpms:use:x', ca.Type.SHORT, count=Jx, attributes={'value': np.zeros(Jx, int), 'precision': 0, 'unit': '', 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flag to use or not use a bpm
        pv_dict.update({'set:bpms:use:y': server.createPV(server_prefix + 'set:bpms:use:y', ca.Type.SHORT, count=Jy, attributes={'value': np.zeros(Jy, int), 'precision': 0, 'unit': '', 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flag to use or not use a bpm
        pv_dict.update({'set:bpms:weights:x': server.createPV(server_prefix + 'set:bpms:weights:x', ca.Type.FLOAT, count=Jx, attributes={'value': np.zeros(Jx, int), 'precision': 0, 'unit': '', 'display_limits': (0, 1e4), 'control_limits': (0, 1e4)})})  # weights
        pv_dict.update({'set:bpms:weights:y': server.createPV(server_prefix + 'set:bpms:weights:y', ca.Type.FLOAT, count=Jy, attributes={'value': np.zeros(Jy, int), 'precision': 0, 'unit': '', 'display_limits': (0, 1e4), 'control_limits': (0, 1e4)})})  # weights
        pv_dict.update({'set:bpms:max:x': server.createPV(server_prefix + 'set:bpms:max:x', ca.Type.FLOAT, count=Jx, attributes={'value':  11*np.ones((Jx,), int), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # aperture
        pv_dict.update({'set:bpms:min:x': server.createPV(server_prefix + 'set:bpms:min:x', ca.Type.FLOAT, count=Jx, attributes={'value': -11*np.ones((Jx,), int), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # aperture
        pv_dict.update({'set:bpms:max:y': server.createPV(server_prefix + 'set:bpms:max:y', ca.Type.FLOAT, count=Jy, attributes={'value':  11*np.ones((Jy,), int), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # aperture
        pv_dict.update({'set:bpms:min:y': server.createPV(server_prefix + 'set:bpms:min:y', ca.Type.FLOAT, count=Jy, attributes={'value': -11*np.ones((Jy,), int), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # aperture
        pv_dict.update({'set:bpms:ref:x': server.createPV(server_prefix + 'set:bpms:ref:x', ca.Type.FLOAT, count=Jx, attributes={'value': np.zeros(Jx, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # reference orbit
        pv_dict.update({'set:bpms:ref:y': server.createPV(server_prefix + 'set:bpms:ref:y', ca.Type.FLOAT, count=Jy, attributes={'value': np.zeros(Jy, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # reference orbit
        pv_dict.update({'set:corrs:use:x': server.createPV(server_prefix + 'set:corrs:use:x', ca.Type.SHORT, count=Kh, attributes={'value': np.zeros(Kh, int), 'precision': 0, 'unit': '', 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flag to use or not use a corrector
        pv_dict.update({'set:corrs:use:y': server.createPV(server_prefix + 'set:corrs:use:y', ca.Type.SHORT, count=Kv, attributes={'value': np.zeros(Kv, int), 'precision': 0, 'unit': '', 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flag to use or not use a corrector
        pv_dict.update({'set:corrs:DeltaI:cut': server.createPV(server_prefix + 'set:corrs:DeltaI:cut', ca.Type.FLOAT, attributes={'value': 0.002, 'precision': 3, 'unit': 'A', 'display_limits': (0, 0.01), 'control_limits': (0, 0.01)})})  # current threshold where Delta_I_est is for a single steerer is cut
        pv_dict.update({'set:corrs:DeltaI:max': server.createPV(server_prefix + 'set:corrs:DeltaI:max', ca.Type.FLOAT, attributes={'value': 0.7, 'precision': 1, 'unit': 'A', 'display_limits': (0, 10.0), 'control_limits': (0, 10.0)})})  # current threshold where Delta_I_est is recuced for all steerers
        pv_dict.update({'set:corrs:Deltaphi:cut': server.createPV(server_prefix + 'set:corrs:Deltaphi:cut', ca.Type.FLOAT, attributes={'value': 0.001, 'precision': 4, 'unit': 'mrad', 'display_limits': (0, 0.005), 'control_limits': (0, 0.005)})})  # steerer strength threshold where Delta_phi_est is for a single steerer is cut
        pv_dict.update({'kap:x': server.createPV(server_prefix + 'kap:x', ca.Type.FLOAT, count=Jx, attributes={'value': np.zeros(Jx, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # orbit
        pv_dict.update({'kap:y': server.createPV(server_prefix + 'kap:y', ca.Type.FLOAT, count=Jy, attributes={'value': np.zeros(Jy, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # orbit
        pv_dict.update({'kap:err:x': server.createPV(server_prefix + 'kap:err:x', ca.Type.FLOAT, count=Jx, attributes={'value': np.zeros(Jx, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # error of orbit
        pv_dict.update({'kap:err:y': server.createPV(server_prefix + 'kap:err:y', ca.Type.FLOAT, count=Jy, attributes={'value': np.zeros(Jy, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # error of orbit
        pv_dict.update({'ui:kap:x': server.createPV(server_prefix + 'ui:kap:x', ca.Type.FLOAT, count=Jx, attributes={'value': np.zeros(Jx, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # orbit for user interface
        pv_dict.update({'ui:kap:y': server.createPV(server_prefix + 'ui:kap:y', ca.Type.FLOAT, count=Jy, attributes={'value': np.zeros(Jy, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # orbit for user interface
        pv_dict.update({'ui:kap:ref:x': server.createPV(server_prefix + 'ui:kap:ref:x', ca.Type.FLOAT, count=Jx, attributes={'value': np.zeros(Jx, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # reference orbit for user interface
        pv_dict.update({'ui:kap:ref:y': server.createPV(server_prefix + 'ui:kap:ref:y', ca.Type.FLOAT, count=Jy, attributes={'value': np.zeros(Jy, float), 'precision': 3, 'unit': 'mm', 'display_limits': (-11., 11.), 'control_limits': (-11., 11.)})})  # reference orbit for user interface
        pv_dict.update({'ui:xaxis': server.createPV(server_prefix + 'ui:xaxis', ca.Type.FLOAT, count=Jx, attributes={'value': np.asarray(np.arange(Jx)+1, float), 'precision': 2, 'unit': ''})})  # xaxis for plot in user interface
        pv_dict.update({'ui:disp:x': server.createPV(server_prefix + 'ui:disp:x', ca.Type.FLOAT, count=Jx, attributes={'value': np.zeros(Jx, float), 'precision': 3, 'unit': 'mm/MHz', 'display_limits': (-11000., 11000.), 'control_limits': (-11000., 11000.)})})  # dispersion for user interface
        pv_dict.update({'ui:disp:y': server.createPV(server_prefix + 'ui:disp:y', ca.Type.FLOAT, count=Jy, attributes={'value': np.zeros(Jy, float), 'precision': 3, 'unit': 'mm/MHz', 'display_limits': (-11000., 11000.), 'control_limits': (-11000., 11000.)})})  # dispersion for user interface
        pv_dict.update({'set:correctdisp': server.createPV(server_prefix + 'set:correctdisp', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'unit': '', 'enmum_strings': ['True', 'False'], 'display_limits': (0, 1), 'control_limits': (0, 1)})})  # flag to correct dispersion
        pv_dict.update({'set:ui:plot:ref': server.createPV(server_prefix + 'set:ui:plot:ref', ca.Type.ENUM, attributes={'value': PlotReference.NONE.value, 'enum_strings': PlotReference._member_names_})})
        pv_dict.update({'set:ui:plot:xaxis': server.createPV(server_prefix + 'set:ui:plot:xaxis', ca.Type.ENUM, attributes={'value': PlotXAxis.BPM_NUMBER.value, 'enum_strings': PlotXAxis._member_names_})})
        pv_dict.update({'wrms:x': server.createPV(server_prefix + 'wrms:x', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 1, 'unit': 'mm', 'display_limits': (0, 1e4), 'control_limits': (0, 1e4)})})  # weighted_rms in x plane
        pv_dict.update({'wrms:y': server.createPV(server_prefix + 'wrms:y', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 1, 'unit': 'mm', 'display_limits': (0, 1e4), 'control_limits': (0, 1e4)})})  # weighted_rms in z plane
        pv_dict.update({'wrms:err:x': server.createPV(server_prefix + 'wrms:err:x', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 1, 'unit': 'mm', 'display_limits': (0, 1e4), 'control_limits': (0, 1e4)})})  # error of weighted_rms in x plane
        pv_dict.update({'wrms:err:y': server.createPV(server_prefix + 'wrms:err:y', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 1, 'unit': 'mm', 'display_limits': (0, 1e4), 'control_limits': (0, 1e4)})})  # error of weighted_rms in z plane
        pv_dict.update({'rms:x': server.createPV(server_prefix + 'rms:x', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 3, 'unit': 'mm', 'display_limits': (0, 10.), 'control_limits': (0, 10.)})})  # rms in x plane
        pv_dict.update({'rms:y': server.createPV(server_prefix + 'rms:y', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 3, 'unit': 'mm', 'display_limits': (0, 10.), 'control_limits': (0, 10.)})})  # rms in z plane
        pv_dict.update({'rms:err:x': server.createPV(server_prefix + 'rms:err:x', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 1, 'unit': 'mm', 'display_limits': (0, 10.), 'control_limits': (0, 10.)})})  # error of rms in x plane
        pv_dict.update({'rms:err:y': server.createPV(server_prefix + 'rms:err:y', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 1, 'unit': 'mm', 'display_limits': (0, 10.), 'control_limits': (0, 10.)})})  # error of rms in z plane
        pv_dict.update({'I:rms:x': server.createPV(server_prefix + 'I:rms:x', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 3, 'unit': 'A', 'display_limits': (-10., 10.), 'control_limits': (-10., 10.)})})  # rms of horizontal corrector currents
        pv_dict.update({'I:rms:y': server.createPV(server_prefix + 'I:rms:y', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 3, 'unit': 'A', 'display_limits': (-10., 10.), 'control_limits': (-10., 10.)})})  # rms of vertical corrector currents
        pv_dict.update({'I:sum:abs:x': server.createPV(server_prefix + 'I:sum:abs:x', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 3, 'unit': 'A', 'display_limits': (Kh*-10., Kh*10.), 'control_limits': (Kh*-10., Kh*10.)})})  # sum of abs(horizontal corrector currents)
        pv_dict.update({'I:sum:abs:y': server.createPV(server_prefix + 'I:sum:abs:y', ca.Type.FLOAT, attributes={'value': 0.0, 'precision': 3, 'unit': 'A', 'display_limits': (Kv*-10., Kv*10.), 'control_limits': (Kv*-10., Kv*10.)})})  # sum of abs(vertical corrector currents)
        pv_dict.update({'I:x': server.createPV(server_prefix + 'I:x', ca.Type.FLOAT, count=Kh, attributes={'value': np.zeros(Kh, float), 'precision': 3, 'unit': 'A', 'display_limits': (-10., 10.), 'control_limits': (-10., 10.)})})  # horizontal corrector currents
        pv_dict.update({'I:y': server.createPV(server_prefix + 'I:y', ca.Type.FLOAT, count=Kv, attributes={'value': np.zeros(Kv, float), 'precision': 3, 'unit': 'A', 'display_limits': (-10., 10.), 'control_limits': (-10., 10.)})})  # vertical corrector currents
        pv_dict.update({'I:min:x': server.createPV(server_prefix + 'I:min:x', ca.Type.FLOAT, count=Kh, attributes={'value': -10*np.ones((Kh,), float), 'precision': 3, 'unit': 'A', 'display_limits': (-10., 10.), 'control_limits': (-10., 10.)})})  # horizontal corrector currents bop constraints
        pv_dict.update({'I:max:x': server.createPV(server_prefix + 'I:max:x', ca.Type.FLOAT, count=Kh, attributes={'value':  10*np.ones((Kh,), float), 'precision': 3, 'unit': 'A', 'display_limits': (-10., 10.), 'control_limits': (-10., 10.)})})  # horizontal corrector currents top constraints
        pv_dict.update({'I:min:y': server.createPV(server_prefix + 'I:min:y', ca.Type.FLOAT, count=Kv, attributes={'value': -10*np.ones((Kv,), float), 'precision': 3, 'unit': 'A', 'display_limits': (-10., 10.), 'control_limits': (-10., 10.)})})  # vertical corrector currents bop constraints
        pv_dict.update({'I:max:y': server.createPV(server_prefix + 'I:max:y', ca.Type.FLOAT, count=Kv, attributes={'value':  10*np.ones((Kv,), float), 'precision': 3, 'unit': 'A', 'display_limits': (-10., 10.), 'control_limits': (-10., 10.)})})  # vertical corrector currents top constraints
        pv_dict.update({'phi:x': server.createPV(server_prefix + 'phi:x', ca.Type.FLOAT, count=Kh, attributes={'value': np.zeros(Kh, float), 'precision': 5, 'unit': 'rad', 'display_limits': (-5e-3, 5e-3), 'control_limits': (-5e-3, 5e-3)})})  # horizontal corrector angles
        pv_dict.update({'phi:y': server.createPV(server_prefix + 'phi:y', ca.Type.FLOAT, count=Kv, attributes={'value': np.zeros(Kv, float), 'precision': 5, 'unit': 'rad', 'display_limits': (-2e-3, 2e-3), 'control_limits': (-2e-3, 2e-3)})})  # vertical corrector angles
        pv_dict.update({'step:Deltakap:x': server.createPV(server_prefix + 'step:Deltakap:x', ca.Type.FLOAT, count=Jx, attributes={'value': np.zeros(Jx, float), 'precision': 4, 'unit': 'mm'})})  # orbit(after correction) - orbit(before correction)
        pv_dict.update({'step:Deltakap:y': server.createPV(server_prefix + 'step:Deltakap:y', ca.Type.FLOAT, count=Jy, attributes={'value': np.zeros(Jy, float), 'precision': 4, 'unit': 'mm'})})  # orbit(after correction) - orbit(before correction)
        pv_dict.update({'step:Deltaphi:x': server.createPV(server_prefix + 'step:Deltaphi:x', ca.Type.FLOAT, count=Kh, attributes={'value': np.zeros(Kh, float), 'unit': 'rad'})})  # angles(after correction) - angles(before correction)
        pv_dict.update({'step:Deltaphi:y': server.createPV(server_prefix + 'step:Deltaphi:y', ca.Type.FLOAT, count=Kv, attributes={'value': np.zeros(Kv, float), 'unit': 'rad'})})  # angles(after correction) - angles(before correction)
        pv_dict.update({'config:range:Jx': server.createPV(server_prefix + 'config:range:Jx', ca.Type.SHORT, count=Jx, attributes={'value': np.arange(Jx) + 1, 'precision': 0, 'unit': '', 'display_limits': (0., Jx), 'control_limits': (0., Jx)})})
        pv_dict.update({'config:range:Jy': server.createPV(server_prefix + 'config:range:Jy', ca.Type.SHORT, count=Jy, attributes={'value': np.arange(Jy) + 1, 'precision': 0, 'unit': '', 'display_limits': (0., Jy), 'control_limits': (0., Jy)})})
        pv_dict.update({'config:range:Kh': server.createPV(server_prefix + 'config:range:Kh', ca.Type.SHORT, count=Kh, attributes={'value': np.arange(Kh) + 1, 'precision': 0, 'unit': '', 'display_limits': (0., Kh), 'control_limits': (0., Kh)})})
        pv_dict.update({'config:range:Kv': server.createPV(server_prefix + 'config:range:Kv', ca.Type.SHORT, count=Kv, attributes={'value': np.arange(Kv) + 1, 'precision': 0, 'unit': '', 'display_limits': (0., Kv), 'control_limits': (0., Kv)})})
        pv_dict.update({'status:response': server.createPV(server_prefix + 'status:response', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:response': server.createPV(server_prefix + 'statusstr:response', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'errcount:response': server.createPV(server_prefix + 'errcount:response', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:reference': server.createPV(server_prefix + 'status:reference', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:reference': server.createPV(server_prefix + 'statusstr:reference', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'errcount:reference': server.createPV(server_prefix + 'errcount:reference', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:laststep': server.createPV(server_prefix + 'status:laststep', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:laststep': server.createPV(server_prefix + 'statusstr:laststep', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'errcount:laststep': server.createPV(server_prefix + 'errcount:laststep', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:cmd': server.createPV(server_prefix + 'status:cmd', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:cmd': server.createPV(server_prefix + 'statusstr:cmd', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'errcount:cmd': server.createPV(server_prefix + 'errcount:cmd', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:correction': server.createPV(server_prefix + 'status:correction', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:correction': server.createPV(server_prefix + 'statusstr:correction', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'errcount:correction': server.createPV(server_prefix + 'errcount:correction', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:bpms:x': server.createPV(server_prefix + 'status:bpms:x', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:bpms:x': server.createPV(server_prefix + 'statusstr:bpms:x', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'stati:bpms:x': server.createPV(server_prefix + 'stati:bpms:x', ca.Type.SHORT, count=Jx, attributes={'value': [ErrorCodes.INVALID.value for k in range(Jx)], 'precision': 0})})
        pv_dict.update({'errcount:bpms:x': server.createPV(server_prefix + 'errcount:bpms:x', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:bpms:y': server.createPV(server_prefix + 'status:bpms:y', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:bpms:y': server.createPV(server_prefix + 'statusstr:bpms:y', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'stati:bpms:y': server.createPV(server_prefix + 'stati:bpms:y', ca.Type.SHORT, count=Jy, attributes={'value': [ErrorCodes.INVALID.value for k in range(Jy)], 'precision': 0})})
        pv_dict.update({'errcount:bpms:y': server.createPV(server_prefix + 'errcount:bpms:y', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:corrs:x': server.createPV(server_prefix + 'status:corrs:x', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:corrs:x': server.createPV(server_prefix + 'statusstr:corrs:x', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'stati:corrs:x': server.createPV(server_prefix + 'stati:corrs:x', ca.Type.SHORT, count=Kh, attributes={'value': [ErrorCodes.INVALID.value for k in range(Kh)], 'precision': 0})})
        pv_dict.update({'errcount:corrs:x': server.createPV(server_prefix + 'errcount:corrs:x', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:corrs:y': server.createPV(server_prefix + 'status:corrs:y', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:corrs:y': server.createPV(server_prefix + 'statusstr:corrs:y', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'stati:corrs:y': server.createPV(server_prefix + 'stati:corrs:y', ca.Type.SHORT, count=Kv, attributes={'value': [ErrorCodes.INVALID.value for k in range(Kv)]})})
        pv_dict.update({'errcount:corrs:y': server.createPV(server_prefix + 'errcount:corrs:y', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:beam_energy': server.createPV(server_prefix + 'status:beam_energy', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:beam_energy': server.createPV(server_prefix + 'statusstr:beam_energy', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'errcount:beam_energy': server.createPV(server_prefix + 'errcount:beam_energy', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'unit': '', 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:beam_current': server.createPV(server_prefix + 'status:beam_current', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:beam_current': server.createPV(server_prefix + 'statusstr:beam_current', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'errcount:beam_current': server.createPV(server_prefix + 'errcount:beam_current', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'unit': '', 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:f': server.createPV(server_prefix + 'status:f', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'statusstr:f': server.createPV(server_prefix + 'statusstr:f', ca.Type.STRING, attributes={'value': 'INVALID'})})
        pv_dict.update({'errcount:f': server.createPV(server_prefix + 'errcount:f', ca.Type.LONG, attributes={'value': 0, 'precision': 0, 'unit': '', 'display_limits': (0, 1e5), 'control_limits': (0, 1e5)})})
        pv_dict.update({'status:sum': server.createPV(server_prefix + 'status:sum', ca.Type.ENUM, attributes={'value': ErrorCodes.INVALID.value, 'precision': 0, 'display_limits': (0, 3), 'control_limits': (0, 3), 'enum_strings': ErrorCodes._member_names_})})
        pv_dict.update({'status:msg': server.createPV(server_prefix + 'status:msg', ca.Type.STRING, attributes={'value': 'sofb started'})})
        logging.debug('Server started.')

        # write list of server pvs
        if list_pvs_flag:
            pvs_list = '\n'.join(server_prefix + suffix for suffix in pv_dict.keys())
            print(pvs_list)
            with open(path.join(out_path, 'pv_list.txt'), 'w', encoding='utf-8') as f:
                f.write(pvs_list + '\n')

        # initialize pvs
        update_settings_pvs(pv_dict=pv_dict, parameters=sofb_model.parameters, settings=sofb_model.settings, configs=sofb_model.configs)
        pv_dict['mode'].value = 0

        # main loop
        exit_event = threading.Event()
        exit_event.clear()
        def exit_main_loop(*args):
            exit_event.set()
        #signal.signal(signal.SIGINT, exit_main_loop)
        #signal.signal(signal.SIGTERM, exit_main_loop)
        while not exit_event.is_set():
            sofb_driver.process(pv_dict=pv_dict, sofb_stati=sofb_stati, sofb_model=sofb_model, sofb_history=sofb_history, sofb_controller=sofb_controller)

        # clean up
        sofb_driver.pipe_parent.close()
        sofb_controller.hw.close()
        sofb_model.save_persistence()
        sofb_driver.opt_prog.join()

    logging.info('Done.\n')

