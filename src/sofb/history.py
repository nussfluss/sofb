from datetime import datetime
import logging
import numpy as np
from . import OptResults, BPMStatus, OrbitStatus, CorrStatus
from .hw.base import Severity


class SOFBHistory(object):

    def __init__(self):
        self._status_history = list()
        self._correction_step_history = list()
        self._steerer_current_history = list()

    def append_status(self, status, settings):
        """
        Add status to history.

        Parameters
        ----------
        status: Status instance
        settings: Settings instance
        """

        # append
        if len(self._status_history) == 0:
            self._status_history.append(status)
        else:
            time_passed = status.timestamp - self._status_history[-1].timestamp  # seconds
            if time_passed > settings.status_history_time_interval or len(self._status_history) == 0:
                self._status_history.append(status)
                logging.debug('status_history: {}/{}'.format(len(self._status_history), settings.status_history_length))
            else:
                logging.debug('Too early to append to status_history.')

        # check whether status is a correction result
        # -> the first status after orbit correction becomes available again is the result for the preceeding orbit correction step
        if status.correction_available:
            if not self._status_history[-1].correction_available:
                if len(self._correction_step_history) > 0:
                    if self._correction_step_history[-1]['correction_result'] is None:
                        self._correction_step_history[-1]['correction_result'] = status

        # pop first entry if neccessary
        if len(self._status_history) > settings.status_history_length:
            self._status_history.pop(0)

    def append_correction_status(self, correction_status, settings):
        """
        Add correction status to history.

        Parameters
        ----------
        status: Status instance
        settings: Settings instance
        """

        # append
        self._correction_step_history.append({'correction_status': correction_status, 'correction_result': None})
        self._steerer_current_history.append(correction_status.corr_status.I_K)

        # pop first entry if neccessary
        if len(self._correction_step_history) > settings.correction_step_history_length:
            self._correction_step_history.pop(0)
        if len(self._steerer_current_history) > settings.correction_step_history_length:
            self._steerer_current_history.pop(0)

        logging.debug('correction_step_history: {}/{}'.format(len(self._correction_step_history), settings.correction_step_history_length))
        logging.debug('steerer_current_history: {}/{}'.format(len(self._steerer_current_history), settings.correction_step_history_length))

    def get_previous_steerer_currents(self):
        if len(self._steerer_current_history) > 0:
            last_steerer_currents = self._steerer_current_history[-1]
            self._steerer_current_history.pop(-1)
            return last_steerer_currents
        else:
            logging.warning('List is of previous steerer currents is empty.')
            return None

    def get_data_of_last_correction_step(self, settings):

        if len(self._correction_step_history) >= 1:

            # retrieve status
            # - status before orbit correction (initial)
            # - status after orbit correction (final)
            # - if the last correction is still in progress, try to retrieve the one before the last one
            initial_correction_status = self._correction_step_history[-1]['correction_status']
            final_status = self._correction_step_history[-1]['correction_result']
            if final_status is None and len(self._correction_step_history) >= 2:
                initial_correction_status = self._correction_step_history[-2]['correction_status']
                final_status = self._correction_step_history[-2]['correction_result']

            # check whether data is okay
            # - all BPMs in use are working properly
            # - all steerers in use are working properly
            #   -> using map_Keff is okay, because it is created considering the severities of the both the set and the readback records
            if initial_correction_status is not None and final_status is not None:
                map_Finuse = []
                for f in range(len(settings.use_F)):
                    if settings.use_F[f]:
                        map_Finuse.append(f)
                if len(np.setdiff1d(map_Finuse, initial_correction_status.bpm_status.map_Feff, assume_unique=True)) > 0 \
                   or len(np.setdiff1d(map_Finuse, final_status.bpm_status.map_Feff, assume_unique=True)) > 0:
                    bpms_are_fine_flag = False
                else:
                    bpms_are_fine_flag = True
                map_Kinuse = []
                for k in range(len(settings.use_K)):
                    if settings.use_K[k]:
                        map_Kinuse.append(k)
                if len(np.setdiff1d(map_Kinuse, initial_correction_status.corr_status.map_Keff, assume_unique=True)) > 0 \
                   or len(np.setdiff1d(map_Kinuse, final_status.corr_status.map_Keff, assume_unique=True)) > 0:
                    corrs_are_fine_flag = False
                else:
                    corrs_are_fine_flag = True

                # collect data
                if bpms_are_fine_flag and corrs_are_fine_flag:
                    Delta_kap_F = final_status.bpm_status.kap_F - initial_correction_status.bpm_status.kap_F
                    Delta_phi_K = final_status.corr_status.phi_K - initial_correction_status.corr_status.phi_K
                    return initial_correction_status.timestamp, Delta_kap_F, Delta_phi_K

        return None
