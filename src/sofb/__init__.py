from timeit import default_timer
import logging
from os import path
import numpy as np
from datetime import datetime
import collections
from channel_access.client import Client
import time

from . import rw
from . import plot, i2phi, orbcor
from .orbcor import chi, R as resp
from . import hw as hw_package
from .status import ErrorCodes


OptResults = collections.namedtuple('OptResults', ('timestamp',
                                                   'runtime',
                                                   'kap_opt_eff_F',
                                                   'kap_est_eff_F',
                                                   'I_opt_eff_K',
                                                   'I_est_eff_K',
                                                   'Delta_I_opt_eff_K',
                                                   'Delta_I_est_eff_K',
                                                   'phi_opt_eff_K',
                                                   'phi_est_eff_K',
                                                   'Delta_phi_opt_eff_K',
                                                   'Delta_phi_est_eff_K',
                                                   'f_opt',
                                                   'f_est',
                                                   'Delta_f_opt',
                                                   'Delta_f_est',
                                                   'R_clean_FK',
                                                   'R_residuals',
                                                   'weighted_rms_est_eff',
                                                   'weighted_rms_est_eff_x',
                                                   'weighted_rms_est_eff_y',
                                                   'rms_est_eff',
                                                   'rms_est_eff_x',
                                                   'rms_est_eff_y'))
BPMStatus = collections.namedtuple('BPMStatus', ('timestamp',
                                                 'kap_F',
                                                 'kap_eff_F',
                                                 'kap_prem_F',
                                                 'kap_prem_eff_F',
                                                 'kap_ref_eff_F',
                                                 'kap_ref_prem_F',
                                                 'kap_ref_prem_eff_F',
                                                 'err_kap_F',
                                                 'map_Feff',
                                                 'map_Jxeff',
                                                 'map_Jyeff'))
OrbitStatus = collections.namedtuple('OrbitStatus', ('timestamp',
                                                     'rms_eff',
                                                     'rms_eff_x',
                                                     'rms_eff_y',
                                                     'err_rms_eff',
                                                     'err_rms_eff_x',
                                                     'err_rms_eff_y',
                                                     'weighted_rms_eff',
                                                     'weighted_rms_eff_x',
                                                     'weighted_rms_eff_y',
                                                     'err_weighted_rms_eff',
                                                     'err_weighted_rms_eff_x',
                                                     'err_weighted_rms_eff_y'))
CorrStatus = collections.namedtuple('CorrStatus', ('timestamp',
                                                   'I_K',
                                                   'I_eff_K',
                                                   'phi_K',
                                                   'phi_eff_K',
                                                   'I_quad_K',
                                                   'I_bounds_K2',
                                                   'map_Keff',
                                                   'map_Kheff',
                                                   'map_Kveff'))
MachineStatus = collections.namedtuple('MachineStatus', ('timestamp',
                                                         'beam_energy',
                                                         'beam_current', 
                                                         'beam_current_below_threshold_flag',
                                                         'f',
                                                         'severity_f'))
CorrectionStatus = collections.namedtuple('CorrectionStatus', ('timestamp',
                                                               'steerer_currents_set_flag',
                                                               'frequency_set_flag',
                                                               'opt_results',
                                                               'corr_status',
                                                               'bpm_status',
                                                               'orbit_status'))
Status = collections.namedtuple('Status', ('timestamp',
                                           'machine_status',
                                           'corr_status',
                                           'bpm_status',
                                           'orbit_status',
                                           'correction_available'))  # bool


class SOFBController(object):

    def __init__(self, configs, sofb_stati):
        """
        Controller Backend.

        This class supplies complete orbit-correction capabilities. It is supposed to be instanciated with with statement either from a script or a service.

        There are different types of quantities

            eff := effective quantities without deactivated bpms and/or deactivated corrs
            est := estimated quantities which include factor
            opt := optimized quantities which are solver output
        """
        self.configs = configs
        self.hw = hw_package.HW(configs)

        logging.debug('SOFBController initialized.')

        # stati
        sofb_stati.register('bpms:x', number=configs.Jx)
        sofb_stati.register('bpms:y', number=configs.Jy)
        sofb_stati.register('corrs:x', number=configs.Kh)
        sofb_stati.register('corrs:y', number=configs.Kv)
        sofb_stati.register('laststep')
        sofb_stati.set('laststep', ErrorCodes.OK, 'No step made, yet.')
        sofb_stati.register('correction')
        sofb_stati.register('beam_current')
        sofb_stati.register('beam_energy')
        sofb_stati.register('f')


    def meas_disp(self, Delta_f=0.001):
        """
        measure dispersion
        """
        logging.debug('measuring dispersion')
        kap_init_F, err_kap_init_F, severity_kap_init_F = self.hw.bpms.get_synchronized_kap_F()
        f_init, severity_f = self.hw.get_f()
        logging.debug('f = {:.03f}'.format(f_init))

        frequency_set_flag = self.hw.put_f(f_init + Delta_f, timeout=2.0)
        logging.debug('setting f = {:.03f} ... '.format(f_init + Delta_f))
        time.sleep(3.0)
        kap_plus_F, err_kap_plus_F, severity_kap_plus_F = self.hw.bpms.get_synchronized_kap_F()
        f_plus, severity_f = self.hw.get_f()
        logging.debug('measured orbit and frequency')

        frequency_set_flag = self.hw.put_f(f_init - Delta_f, timeout=2.0)
        logging.debug('setting f = {:.03f} ... '.format(f_init - Delta_f))
        time.sleep(3.0)
        kap_minus_F, err_kap_minus_F, severity_kap_minus_F = self.hw.bpms.get_synchronized_kap_F()
        f_minus, severity_f = self.hw.get_f()
        logging.debug('measured orbit and frequency')

        frequency_set_flag = self.hw.put_f(f_init, timeout=2.0)
        logging.debug('restoring f = {:.03f} ... '.format(f_init))

        Rd_F = (kap_plus_F - kap_minus_F) / (f_plus - f_minus)

        logging.info('finished dispersion measurment')

        return Rd_F


    def check_if_dead_time_passed(self, settings):
        """
        pv_dict.update({'set:correctdisp': server.createPV(server_prefix + 'set:correctdisp', ca.Type.ENUM, attributes={'value': 0, 'precision': 0, 'unit': '', 'enmum_strings': ['True', 'False'], 'control_limits': (0, 1)})})  # flag to correct dispersion
        determine if sofb is ready for a correction step
        """
        Delta_t =  default_timer() - self.hw.corrs.T_ready
        if Delta_t < settings.t_sleep:
            logging.debug('Orbit correction unavailable. {:2.02} s / {:2.02} s have passed since the last step.'.format(Delta_t, settings.t_sleep))
            return False
        return True

    def correct_orbit(self, opt_results, status, settings, configs, sofb_stati, unblock_after_ramp):
        if self.check_if_correction_step_is_available(orbit_status=status.orbit_status, settings=settings, configs=configs, sofb_stati=sofb_stati):
            logging.debug('Writing frequency and currents to records and starting ramp.')
            steerer_currents_set_flag = self.hw.corrs.put_I_K(opt_results.I_est_eff_K, unblock_after_ramp)
            frequency_set_flag = False
            if not steerer_currents_set_flag:
                logging.warning('Failed to set steerer currents!')
            if steerer_currents_set_flag:
                if abs(opt_results.f_est - status.machine_status.f) > 1e-9:
                    frequency_set_flag = self.hw.put_f(opt_results.f_est, timeout=2.0)
                else:
                    frequency_set_flag = None
                logging.debug('Started ramp.')
            return CorrectionStatus(timestamp = datetime.utcnow().timestamp(),
                                    steerer_currents_set_flag = steerer_currents_set_flag,
                                    frequency_set_flag = frequency_set_flag,
                                    opt_results = opt_results,
                                    corr_status = status.corr_status,
                                    bpm_status = status.bpm_status,
                                    orbit_status = status.orbit_status)
        else:
            return None

    def check(self, parameters, settings, configs, sofb_stati):

        # collect data
        beam_energy, severity_beam_energy = self.hw.get_beam_energy()
        beam_current, severity_beam_current = self.hw.get_beam_current()
        f, severity_f = self.hw.get_f()

        # evaluate beam_energy and beam_current
        beam_current_below_threshold_flag = False
        if severity_beam_current >= hw_package.base.Severity.INVALID:
            logging.warning('de-beam-i is in an error state: Assuming beam current is above threshold.')
            sofb_stati.set('beam_current', ErrorCodes.OK, 'epics error')
        else:
            if beam_current < settings.I_beam_min:
                beam_current_below_threshold_flag = True
                sofb_stati.set('beam_current', ErrorCodes.ERROR, '< threshold')
            else:
                sofb_stati.set('beam_current', ErrorCodes.OK, 'OK')
        if severity_beam_energy >= hw_package.base.Severity.INVALID:
            beam_energy = 1.492  # GeV
            logging.warning('de-beam-energy is in an error state: Using hard-coded energy of 1.492 GeV for calculation of steerer strengths.')
            sofb_stati.set('beam_energy', ErrorCodes.ERROR, '')
        else:
            sofb_stati.set('beam_energy', ErrorCodes.OK, 'OK')
        if severity_f >= hw_package.base.Severity.INVALID:
            logging.warning('bo-rfmaster-f is in an error state.')
            sofb_stati.set('f', ErrorCodes.ERROR, 'epics error')
        else:
            sofb_stati.set('f', ErrorCodes.OK, 'OK')

        # package data
        machine_status = MachineStatus(timestamp = datetime.utcnow().timestamp(),
                                       beam_energy = beam_energy,
                                       beam_current = beam_current,
                                       beam_current_below_threshold_flag = beam_current_below_threshold_flag,
                                       f = f,
                                       severity_f = severity_f)

        # collect other stati
        corr_status = self._check_corrs(machine_status, parameters, settings, configs, sofb_stati)
        bpm_status = self._check_bpms(machine_status, parameters, settings, configs, sofb_stati)
        orbit_status = check_orbit(bpm_status, parameters, settings, configs)
        correction_available = self.check_if_correction_step_is_available(orbit_status, settings, configs, sofb_stati)
        timestamp = datetime.utcnow().timestamp()

        return Status(timestamp = timestamp,
                      machine_status = machine_status,
                      corr_status = corr_status,
                      bpm_status = bpm_status,
                      orbit_status = orbit_status,
                      correction_available = correction_available)

    def check_if_correction_step_is_available(self, orbit_status, settings, configs, sofb_stati):
        I_beam, severity_I_beam = self.hw.get_beam_current()
        if severity_I_beam >= hw_package.base.Severity.INVALID:
            logging.debug('Beam current record is in an error state. Cannot guarantee current is above threshold!')
            sofb_stati.set('correction', ErrorCodes.OK, 'AVAILABLE (beam-current error)')
        else:
            if I_beam < settings.I_beam_min:
                logging.debug('Orbit correction unavailable. Beam current {} is below beam current threshold {}.'.format(I_beam, settings.I_beam_min))
                sofb_stati.set('correction', ErrorCodes.OK, 'UNAVAILABLE (beam current < threshold)')
                return False
        if not self.hw.bpms.synchronous_orbit_initialized and self.hw.bpms.injection_running:
            logging.debug('Orbit correction unvailable. Waiting for injector.')
            sofb_stati.set('correction', ErrorCodes.OK, 'UNAVAILABLE (waiting for injector)')
            return False
        if not self.hw.corrs.ready and not configs.unarm_flag:
            logging.debug('Orbit correction unavailable. Magnets are not ready.')
            sofb_stati.set('correction', ErrorCodes.OK, 'UNAVAILABLE (magnets-not-ready)')
            return False
        if not self.check_if_dead_time_passed(settings):
            sofb_stati.set('correction', ErrorCodes.OK, 'UNAVAILABLE (dead time)')
            return False
        if settings.decoupled_flag:
            if orbit_status.weighted_rms_eff_x <= settings.weighted_rms_threshold_nostep_x and orbit_status.weighted_rms_eff_y <= settings.weighted_rms_threshold_nostep_y:
                logging.debug('Orbit correction unvailable. weighted_rms is below threshold.')
                sofb_stati.set('correction', ErrorCodes.OK, 'UNAVAILABLE (wrms < threshold)')
                return False
        else:
            if orbit_status.weighted_rms_eff <= np.sqrt(settings.weighted_rms_threshold_nostep_x**2 + settings.weighted_rms_threshold_nostep_y**2):
                logging.debug('Orbit correction unvailable. weighted_rms is below threshold.')
                sofb_stati.set('correction', ErrorCodes.OK, 'UNAVAILABLE (wrms < threshold)')
                return False
        sofb_stati.set('correction', ErrorCodes.OK, 'AVAILABLE')
        return True

    def _check_corrs(self, machine_status, parameters, settings, configs, sofb_stati):
        """
        Check status of steerer magnets.

        1. Collect information from all steerer-related records. These are:
            * steerer currents, steerer current severities and steerer current ctrl limits
            * quadrupole currents and quadrupole current severities
            * beam energy and beam-energy severity
        2. Create map of effectively usable steerer magnets.

        Parameters
        ----------
        parameters: Parameters
        settings: Settings
        configs: Configs

        Return
        ------
        corr_status: CorrStatus
        """

        # update data
        I_K, _ = self.hw.corrs.get_I_K()
        I_bounds_K2 = self.hw.corrs.I_bounds_K2
        I_quad_K, _ = self.hw.corrs.get_I_quad_K()
        phi_K = i2phi.I_K_2_strength_K(configs.ar_cor['mount_type'], configs.ar_cor['sextupole_type'], configs.ar_cor['plane'],
                                       I_K, I_quad_K, machine_status.beam_energy, integrated_flag=True)
        severities_K = [self.hw.corrs.corrs[k].severity for k in range(configs.K)]

        # create maps and status list
        map_Keff = []
        statusstrs = []
        stati = []
        for k in range(configs.K):
            if not settings.use_K[k]:
                logging.debug('Excluding {}. Disabled by user.'.format(configs.ar_cor['idstr'][k]))
                statusstrs.append('disabled')
                stati.append(ErrorCodes.OK)
            elif not self.hw.corrs.corrs[k].turned_on:
                logging.debug('Excluding {}. Current source offline.'.format(configs.ar_cor['idstr'][k]))
                statusstrs.append('turned off')
                stati.append(ErrorCodes.ERROR)
            elif severities_K[k] >= hw_package.base.Severity.INVALID:
                logging.debug('Excluding {}. One of the associated records is in an error state.'.format(configs.ar_cor['idstr'][k]))
                statusstrs.append('epics error')
                stati.append(ErrorCodes.ERROR)
            elif not self.hw.corrs.corrs[k].at_set_value:
                logging.debug('Excluding {}. Not at set value'.format(configs.ar_cor['idstr'][k]))
                statusstrs.append('not at set value')
                stati.append(ErrorCodes.ERROR)
            elif I_bounds_K2[k, 0] is None or I_bounds_K2[k, 1] is None:
                logging.debug('Excluding {}. Current limits of steerer unavailable.'.format(configs.ar_cor['idstr'][k]))
                statusstrs.append('limits not available')
                stati.append(ErrorCodes.ERROR)
            else:
                map_Keff.append(k)
                statusstrs.append('AVAILABLE')
                stati.append(ErrorCodes.OK)
        if len(map_Keff) > 0:
            map_Keff = np.asarray(map_Keff)
            results = np.argwhere(map_Keff>=configs.Kh)
            if len(results) == 0:
                map_Kheff = map_Keff
                map_Kveff = []
            elif map_Keff[0] >= configs.Kh:
                map_Kheff = []
                map_Kveff = map_Keff
            else:
                Kheff = results[0][0]
                map_Kheff = map_Keff[:Kheff]
                map_Kveff = map_Keff[Kheff:]
                map_Keff = map_Keff
        else:
            map_Kheff = []
            map_Kveff = []
        logging.debug('Excluded {} horizontal and {} vertical steerering magnets.'.format(configs.Kh-len(map_Kheff), configs.Kv-len(map_Kveff)))
        if machine_status.beam_current is not None:
            if machine_status.beam_current >= settings.I_beam_min:
                sofb_stati.set('corrs:x', stati[:configs.Kh], statusstrs[:configs.Kh])
                sofb_stati.set('corrs:y', stati[configs.Kh:], statusstrs[configs.Kh:])
            else:
                sofb_stati.set('corrs:x', [ErrorCodes.OK for kh in range(configs.Kh)], ['no beam' for kh in range(configs.Kh)])
                sofb_stati.set('corrs:y', [ErrorCodes.OK for kv in range(configs.Kv)], ['no beam' for kv in range(configs.Kv)])

        # orbit correction is disabled completly and not plane secific because of coupled orbit correction
        if sofb_stati.get('corrs:x').status == ErrorCodes.ERROR:
            map_Kheff = []
            map_Kveff = []
            map_Keff = []
            logging.debug('less than 60 % of horizontal steerers are available! Orbit correction disabled.')
        if sofb_stati.get('corrs:y').status == ErrorCodes.ERROR:
            map_Kheff = []
            map_Kveff = []
            map_Keff = []
            logging.debug('less than 60 % of vertical steerers are available! Orbit correction disabled.')

        # initialize effective quantitites
        I_eff_K = np.zeros(configs.K, np.float64)
        phi_eff_K = np.zeros(configs.K, np.float64)
        I_eff_K[map_Keff] = I_K[map_Keff]
        phi_eff_K[map_Keff] = phi_K[map_Keff]

        return CorrStatus(timestamp = datetime.utcnow().timestamp(),
                          I_K = np.copy(I_K),
                          I_eff_K = np.copy(I_eff_K),
                          phi_K = np.copy(phi_K),
                          phi_eff_K = np.copy(phi_eff_K),
                          I_quad_K = np.copy(I_quad_K),
                          I_bounds_K2 = np.copy(np.asarray(I_bounds_K2, np.float64)),
                          map_Keff = np.asarray(map_Keff, int),
                          map_Kheff = np.asarray(map_Kheff, int),
                          map_Kveff = np.asarray(map_Kveff, int))


    def _check_bpms(self, machine_status, parameters, settings, configs, sofb_stati):
        """
        Check status of BPMs.

        1. Collect information from all BPM-related records. These are:
            * orbit deviations, orbit deviation severities and std deviation of orbit deviations
            * beam energy and beam-energy severity
        2. Create map of effectively usable BPMs.
        3. Remove pincushion distortion.

        Parameters
        ----------
        parameters: Parameters
        settings: Settings
        configs: Configs

        Return
        ------
        bpm_status: BPMStatus
        """

        # update data
        kap_F, err_kap_F, severity_kap_F = self.hw.bpms.get_synchronized_kap_F()

        # create maps and status list
        map_Feff = []
        statusstrs = []
        stati = []
        for f in range(configs.F):
            if not settings.use_F[f]:
                logging.debug('Excluding {}. Disabled by user.'.format(configs.ar_bpm['idstr'][f]))
                statusstrs.append('DISABLED')
                stati.append(ErrorCodes.OK)
            elif err_kap_F[f] >= settings.limit_err_kap:
                logging.debug('Excluding {}. Standard deviation of sample >= {:.03} mm.'.format(configs.ar_bpm['idstr'][f], settings.limit_err_kap))
                statusstrs.append('UNAVAILABLE (wrms > threshold)')
                stati.append(ErrorCodes.ERROR)
            elif severity_kap_F[f] >= hw_package.base.Severity.INVALID:
                logging.debug('Excluding {}. Associated record is in an error state.'.format(configs.ar_bpm['idstr'][f]))
                statusstrs.append('UNAVAILABLE (epics error)')
                stati.append(ErrorCodes.ERROR)
            else:
                map_Feff.append(f)
                statusstrs.append('AVAILABLE')
                stati.append(ErrorCodes.OK)
        if len(map_Feff) > 0:
            map_Feff = np.asarray(map_Feff)
            if map_Feff[-1] >= configs.Jx:
                Jxeff = np.argwhere(map_Feff >= configs.Jx)[0][0]
            else:
                Jxeff = len(map_Feff)
            map_Jxeff = map_Feff[:Jxeff]
            map_Jyeff = map_Feff[Jxeff:]
            map_Feff = map_Feff
        else:
            map_Jxeff = []
            map_Jyeff = []
        logging.debug('Excluded {} horizontal and {} vertical BPMs.'.format(configs.Jx-len(map_Jxeff), configs.Jy-len(map_Jyeff)))
        if machine_status.beam_current is not None:
            if machine_status.beam_current >= settings.I_beam_min:
                sofb_stati.set('bpms:x', stati[:configs.Jx], statusstrs[:configs.Jx], parameters.W_FF.diagonal()[:configs.Jx])
                sofb_stati.set('bpms:y', stati[configs.Jx:], statusstrs[configs.Jx:], parameters.W_FF.diagonal()[configs.Jx:])
            else:
                sofb_stati.set('bpms:x', [ErrorCodes.OK for j in range(configs.Jx)], ['no beam' for j in range(configs.Jx)], parameters.W_FF.diagonal()[:configs.Jx])
                sofb_stati.set('bpms:y', [ErrorCodes.OK for j in range(configs.Jy)], ['no beam' for j in range(configs.Jy)], parameters.W_FF.diagonal()[configs.Jx:])

        # orbit correction is disabled completly and not plane secific because of coupled orbit correction
        if sofb_stati.get('bpms:x').status == ErrorCodes.ERROR:
            map_Jxeff = []
            map_Jyeff = []
            map_Feff = []
            logging.debug('less than 70 % of horizontal BPM readings are available! Orbit correction disabled.')
        if sofb_stati.get('bpms:y').status == ErrorCodes.ERROR:
            map_Jxeff = []
            map_Jyeff = []
            map_Feff = []
            logging.debug('less than 70 % of vertical BPM readings are available! Orbit correction disabled.')

        # initialize prem quantities and effective quantities
        kap_prem_F = np.zeros(configs.F, np.float64)
        kap_ref_prem_F = np.zeros(configs.F, np.float64)
        kap_prem_eff_F = np.zeros(configs.F, np.float64)
        kap_ref_prem_eff_F = np.zeros(configs.F, np.float64)
        kap_eff_F = np.zeros(configs.F, np.float64)
        kap_ref_eff_F = np.zeros(configs.F, np.float64)

        # map to effective quantities
        kap_eff_F[map_Feff] = kap_F[map_Feff]
        kap_ref_eff_F[map_Feff] = parameters.kap_ref_F[map_Feff]
        kap_prem_eff_F[map_Feff] = kap_prem_eff_F[map_Feff]
        kap_ref_prem_eff_F[map_Feff] = kap_ref_prem_F[map_Feff]

        return BPMStatus(timestamp = datetime.utcnow().timestamp(),
                         kap_F = np.copy(kap_F),
                         kap_eff_F = np.copy(kap_eff_F),
                         kap_prem_F = np.copy(kap_prem_F),
                         kap_prem_eff_F = np.copy(kap_prem_F),
                         kap_ref_eff_F = np.copy(kap_ref_eff_F),
                         kap_ref_prem_F = np.copy(kap_ref_prem_F),
                         kap_ref_prem_eff_F = np.copy(kap_ref_prem_eff_F),
                         err_kap_F = np.copy(err_kap_F),
                         map_Feff = np.asarray(map_Feff, int),
                         map_Jxeff = np.asarray(map_Jxeff, int),
                         map_Jyeff = np.asarray(map_Jyeff, int))


def check_orbit(bpm_status, parameters, settings, configs):

    # determine parameters
    if settings.remove_pincushion_dist:
        kap_F = bpm_status.kap_prem_F
        kap_ref_F = bpm_status.kap_ref_prem_F
    else:
        kap_F = bpm_status.kap_F
        kap_ref_F = parameters.kap_ref_F
    W_FeffFeff = resp.reduce_W_FF_to_W_FeffFeff(W_FF=parameters.W_FF, map_Feff=bpm_status.map_Feff)

    # determine orbit-correction-quality metrics
    Jxeff = len(bpm_status.map_Jxeff)
    W_FeffFeff = resp.reduce_W_FF_to_W_FeffFeff(W_FF=parameters.W_FF, map_Feff=bpm_status.map_Feff)
    rms_eff = chi.cal_rms(kap_F[bpm_status.map_Feff], kap_ref_F[bpm_status.map_Feff])
    rms_eff_x = chi.cal_rms(kap_F[bpm_status.map_Jxeff], kap_ref_F[bpm_status.map_Jxeff])
    rms_eff_y = chi.cal_rms(kap_F[bpm_status.map_Jyeff], kap_ref_F[bpm_status.map_Jyeff])
    err_rms_eff = chi.cal_rms_err(kap_F[bpm_status.map_Feff], kap_ref_F[bpm_status.map_Feff], bpm_status.err_kap_F[bpm_status.map_Feff])
    err_rms_eff_x = chi.cal_rms_err(kap_F[bpm_status.map_Jxeff], kap_ref_F[bpm_status.map_Jxeff], bpm_status.err_kap_F[bpm_status.map_Jxeff])
    err_rms_eff_y = chi.cal_rms_err(kap_F[bpm_status.map_Jyeff], kap_ref_F[bpm_status.map_Jyeff], bpm_status.err_kap_F[bpm_status.map_Jyeff])
    weighted_rms_eff = chi.cal_weighted_rms(kap_F[bpm_status.map_Feff], kap_ref_F[bpm_status.map_Feff], W_FeffFeff)
    weighted_rms_eff_x = chi.cal_weighted_rms(kap_F[bpm_status.map_Jxeff], kap_ref_F[bpm_status.map_Jxeff], W_FeffFeff[:Jxeff, :Jxeff])
    weighted_rms_eff_y = chi.cal_weighted_rms(kap_F[bpm_status.map_Jyeff], kap_ref_F[bpm_status.map_Jyeff], W_FeffFeff[Jxeff:, Jxeff:])
    err_weighted_rms_eff = chi.cal_weighted_rms_err(kap_F[bpm_status.map_Feff], kap_ref_F[bpm_status.map_Feff], W_FeffFeff, bpm_status.err_kap_F[bpm_status.map_Feff])
    err_weighted_rms_eff_x = chi.cal_weighted_rms_err(kap_F[bpm_status.map_Jxeff], kap_ref_F[bpm_status.map_Jxeff], W_FeffFeff[:Jxeff, :Jxeff], bpm_status.err_kap_F[bpm_status.map_Jxeff])
    err_weighted_rms_eff_y = chi.cal_weighted_rms_err(kap_F[bpm_status.map_Jyeff], kap_ref_F[bpm_status.map_Jyeff], W_FeffFeff[Jxeff:, Jxeff:], bpm_status.err_kap_F[bpm_status.map_Jyeff])
    logging.debug('weighted_rms   x: {:.01f}+-{:.01f}, y: {:.01f}+-{:.01f}.'.format(weighted_rms_eff_x, err_weighted_rms_eff_x, weighted_rms_eff_y, err_weighted_rms_eff_y))

    return OrbitStatus(timestamp = datetime.utcnow().timestamp(),
                       rms_eff = rms_eff,
                       rms_eff_x = rms_eff_x,
                       rms_eff_y = rms_eff_y,
                       err_rms_eff = err_rms_eff,
                       err_rms_eff_x = err_rms_eff_x,
                       err_rms_eff_y = err_rms_eff_y,
                       weighted_rms_eff = weighted_rms_eff,
                       weighted_rms_eff_x = weighted_rms_eff_x,
                       weighted_rms_eff_y = weighted_rms_eff_y,
                       err_weighted_rms_eff = err_weighted_rms_eff,
                       err_weighted_rms_eff_x = err_weighted_rms_eff_x,
                       err_weighted_rms_eff_y = err_weighted_rms_eff_y)

def correction_guess(machine_status, corr_status, bpm_status, orbit_status, parameters, settings, configs, WR_reg=True):
    """
    Parameters
    ----------
    corr_status: CorrStatus
    bpm_status: BPMStatus
    orbit_status: OrbitStatus
    parameters: Parameters
    settings: Settings
    configs: Configs

    Return
    ------
    opt_results: OptResults or None
        Either a named tuple containining guesses for steerer currents and the ooptimized orbit among aother things or None if determining a best set of steerer currents failed.
    """

    # timer determining runtime
    t_begin = default_timer()

    F = configs.F
    K = configs.K
    Jxeff = len(bpm_status.map_Jxeff)
    Kheff = len(corr_status.map_Kheff)

    # determine parameters
    if settings.remove_pincushion_dist:
        kap_F = bpm_status.kap_prem_F
        kap_ref_F = bpm_status.kap_ref_prem_F
    else:
        kap_F = bpm_status.kap_F
        kap_ref_F = parameters.kap_ref_F
    # decoupled_flag has to be always True. Even if settings.decoupled_flag == True!
    if WR_reg:
        WR_FK = np.einsum('if,fk->ik', parameters.W_FF, parameters.R_FK)
        WR_reg_FK, _, _ = orbcor.R.remove_singular_values_from_R_FK(WR_FK, settings.sigmas_to_remove_h, settings.sigmas_to_remove_v, configs.Kh, configs.Jx, decoupled_flag=True)
        R_clean_FK = np.linalg.solve(parameters.W_FF, WR_reg_FK)
    else:
        R_clean_FK, _, _ = resp.remove_singular_values_from_R_FK(parameters.R_FK, sigmas_to_remove_h=settings.sigmas_to_remove_h,
                                                                 sigmas_to_remove_v=settings.sigmas_to_remove_v, Kh=configs.Kh, J=configs.Jx, decoupled_flag=True)
    R_clean_FeffKeff = resp.reduce_R_FK_to_R_FeffKeff(R_clean_FK, map_Feff=bpm_status.map_Feff, map_Keff=corr_status.map_Keff)
    W_FeffFeff = resp.reduce_W_FF_to_W_FeffFeff(W_FF=parameters.W_FF, map_Feff=bpm_status.map_Feff)
    if machine_status.f is not None:
        f_bounds_2 = np.asarray([machine_status.f-0.001, machine_status.f+0.001], np.float64)
    else:
        f_bounds_2 = None
    if chi.PathLength(settings.dispersion_correction) is not chi.PathLength.IGNORE and machine_status.severity_f < hw_package.base.Severity.INVALID:
        Rd_Jxeff = parameters.Rd_F[bpm_status.map_Jxeff]
        Rd_Feff = parameters.Rd_F[bpm_status.map_Feff]
    else:
        Rd_Feff = None
        Rd_Jxeff = None

    # check conditions and run optimizer
    if settings.decoupled_flag:
        if orbit_status.weighted_rms_eff_x > settings.weighted_rms_threshold_nostep_x:
            if orbit_status.weighted_rms_eff_x < settings.weighted_rms_threshold_micado_x:
                factor_x = 0.85
                micado_x = True
            else:
                factor_x = settings.factor
                micado_x = False
        else:
            factor_x = 0.0
            micado_x = True
            logging.debug('Aborting in x plane because weighted_rms_x is below threshold.')

        if orbit_status.weighted_rms_eff_y > settings.weighted_rms_threshold_nostep_y:
            if orbit_status.weighted_rms_eff_y < settings.weighted_rms_threshold_micado_y:
                factor_y = 0.85
                micado_y = True
            else:
                factor_y = settings.factor
                micado_y = False
        else:
            factor_y = 0.0
            micado_y = True
            logging.debug('Aborting in y plane because weighted_rms_y is below threshold.')

        if factor_x == 0.0 and factor_y == 0.0:
            return False

        I_Kheff_2_phi_Kheff = i2phi.decorate_I_K_2_strength_K(mount_type_K=configs.ar_cor['mount_type'][:Kheff], sextupole_type_K=configs.ar_cor['sextupole_type'][:Kheff],
                                                              plane_K=configs.ar_cor['plane'][:Kheff], I_quad_K=corr_status.I_quad_K[:Kheff], beam_energy=machine_status.beam_energy, integrated_flag=True)
        I_Kveff_2_phi_Kveff = i2phi.decorate_I_K_2_strength_K(mount_type_K=configs.ar_cor['mount_type'][Kheff:], sextupole_type_K=configs.ar_cor['sextupole_type'][Kheff:],
                                                              plane_K=configs.ar_cor['plane'][Kheff:], I_quad_K=corr_status.I_quad_K[Kheff:], beam_energy=machine_status.beam_energy, integrated_flag=True)
        phi_Kheff_2_I_Kheff = i2phi.decorate_strength_K_2_I_K(mount_type_K=configs.ar_cor['mount_type'][:Kheff], sextupole_type_K=configs.ar_cor['sextupole_type'][:Kheff],
                                                              plane_K=configs.ar_cor['plane'][:Kheff], I_quad_K=corr_status.I_quad_K[:Kheff], beam_energy=machine_status.beam_energy, integrated_flag=True)
        phi_Kveff_2_I_Kveff = i2phi.decorate_strength_K_2_I_K(mount_type_K=configs.ar_cor['mount_type'][Kheff:], sextupole_type_K=configs.ar_cor['sextupole_type'][Kheff:],
                                                              plane_K=configs.ar_cor['plane'][Kheff:], I_quad_K=corr_status.I_quad_K[Kheff:], beam_energy=machine_status.beam_energy, integrated_flag=True)

        try:
            orbcor_results = orbcor.determine_correction_step_uncoupled(kap_Jx=kap_F[bpm_status.map_Jxeff], kap_Jy=kap_F[bpm_status.map_Jyeff],
                                                                        kap_ref_Jx=kap_ref_F[bpm_status.map_Jxeff], kap_ref_Jy=kap_ref_F[bpm_status.map_Jyeff],
                                                                        kap_bounds_Jx2=parameters.constraint_F2[bpm_status.map_Jxeff],
                                                                        kap_bounds_Jy2=parameters.constraint_F2[bpm_status.map_Jyeff],
                                                                        I_Kh=corr_status.I_K[corr_status.map_Kheff],
                                                                        I_Kv=corr_status.I_K[corr_status.map_Kveff],
                                                                        I_bounds_Kh2=settings.I_bounds_factor*corr_status.I_bounds_K2[corr_status.map_Kheff],
                                                                        I_bounds_Kv2=settings.I_bounds_factor*corr_status.I_bounds_K2[corr_status.map_Kveff],
                                                                        R_JxKh=R_clean_FeffKeff[:Jxeff, :Kheff], R_JyKv=R_clean_FeffKeff[Jxeff:, Kheff:],
                                                                        f=machine_status.f, Rd_Jx=Rd_Jxeff, f_bounds_2=f_bounds_2,
                                                                        W_JxJx=W_FeffFeff[:Jxeff, :Jxeff], W_JyJy=W_FeffFeff[Jxeff:, Jxeff:],
                                                                        Delta_I_cut=settings.Delta_I_cut,
                                                                        Delta_I_max=settings.Delta_I_max, 
                                                                        Delta_phi_cut=settings.Delta_phi_cut, 
                                                                        factor_x=factor_x, factor_y=factor_y,
                                                                        I_Kh_2_phi_Kh=I_Kheff_2_phi_Kheff, I_Kv_2_phi_Kv=I_Kveff_2_phi_Kveff,
                                                                        phi_Kh_2_I_Kh=phi_Kheff_2_I_Kheff, phi_Kv_2_I_Kv=phi_Kveff_2_I_Kveff,
                                                                        orbit_slack=settings.orbit_slack, orbit_slack_factor=settings.orbit_slack_factor, 
                                                                        optimization_method=settings.method,
                                                                        path_length_mod=chi.PathLength(settings.dispersion_correction),
                                                                        minimize_steerer_strength_flag=settings.minimize_currents,
                                                                        micado_x_flag=micado_x, micado_y_flag=micado_y)
        except orbcor.CorrectionFailedError as error:
            return error
        except:
            return None
    else:
        if orbit_status.weighted_rms_eff <= np.sqrt(settings.weighted_rms_threshold_nostep_x**2 + settings.weighted_rms_threshold_nostep_y**2):
            logging.debug('Aborting because weighted_rms is below threshold.')
            return False
        else:
            if orbit_status.weighted_rms_eff <= np.sqrt(settings.weighted_rms_threshold_micado_x**2 + settings.weighted_rms_threshold_micado_y**2):
                factor = 0.85
                micado = True
            else:
                factor = settings.factor
                micado = False

        I_Keff_2_phi_Keff = i2phi.decorate_I_K_2_strength_K(mount_type_K=configs.ar_cor['mount_type'][corr_status.map_Keff], sextupole_type_K=configs.ar_cor['sextupole_type'][corr_status.map_Keff],
                                                            plane_K=configs.ar_cor['plane'][corr_status.map_Keff], I_quad_K=corr_status.I_quad_K[corr_status.map_Keff], beam_energy=machine_status.beam_energy, integrated_flag=True)
        phi_Keff_2_I_Keff = i2phi.decorate_strength_K_2_I_K(mount_type_K=configs.ar_cor['mount_type'][corr_status.map_Keff], sextupole_type_K=configs.ar_cor['sextupole_type'][corr_status.map_Keff],
                                                            plane_K=configs.ar_cor['plane'][corr_status.map_Keff], I_quad_K=corr_status.I_quad_K[corr_status.map_Keff], beam_energy=machine_status.beam_energy, integrated_flag=True)
        orbit_slack_F = np.full((W_FeffFeff.shape[0],), 0.10, np.float64)/np.diag(W_FeffFeff)
        try:
            orbcor_results = orbcor.determine_correction_step(kap_F=kap_F[bpm_status.map_Feff], kap_ref_F=kap_ref_F[bpm_status.map_Feff],
                                                              kap_bounds_F2=parameters.constraint_F2[bpm_status.map_Feff],
                                                              I_K=corr_status.I_K[corr_status.map_Keff],
                                                              I_bounds_K2=settings.I_bounds_factor*corr_status.I_bounds_K2[corr_status.map_Keff],
                                                              R_FK=R_clean_FeffKeff, Delta_I_cut=settings.Delta_I_cut,
                                                              Delta_I_max=settings.Delta_I_max, 
                                                              Delta_phi_cut=settings.Delta_phi_cut, 
                                                              f=machine_status.f, Rd_F=Rd_Feff, f_bounds_2=f_bounds_2,
                                                              W_FF=W_FeffFeff, factor=settings.factor,
                                                              I_K_2_phi_K=I_Keff_2_phi_Keff, phi_K_2_I_K=phi_Keff_2_I_Keff,
                                                              orbit_slack_F=orbit_slack_F, Jx=Jxeff, optimization_method=settings.method,
                                                              path_length_mod=chi.PathLength(settings.dispersion_correction),
                                                              minimize_steerer_strength_flag=settings.minimize_currents,
                                                              micado_flag=micado)
        except orbcor.CorrectionFailedError as error:
            return error
        except:
            return None

    status_str = ''
    for idstr in configs.ar_cor['idstr'][corr_status.map_Keff[orbcor_results['map_Kreduced']]]:
        status_str += idstr + ' '
    logging.debug('Final set of steerer magnets: {}.'.format(status_str))

    # unmap results
    kap_est_eff_F = np.zeros(F, np.float64)
    kap_opt_eff_F = np.zeros(F, np.float64)
    I_est_eff_K = np.copy(corr_status.I_K)
    I_opt_eff_K = np.copy(corr_status.I_K)
    Delta_I_opt_eff_K = np.zeros(K, np.float64)
    Delta_I_est_eff_K = np.zeros(K, np.float64)
    phi_est_eff_K = np.copy(corr_status.phi_K)
    phi_opt_eff_K = np.copy(corr_status.phi_K)
    Delta_phi_est_eff_K = np.zeros(K, np.float64)
    Delta_phi_opt_eff_K = np.zeros(K, np.float64)
    kap_est_eff_F[bpm_status.map_Feff] = orbcor_results['kap_est_F']
    kap_opt_eff_F[bpm_status.map_Feff] = orbcor_results['kap_opt_F']
    I_est_eff_K[corr_status.map_Keff] = orbcor_results['I_est_K']
    I_opt_eff_K[corr_status.map_Keff] = orbcor_results['I_opt_K']
    Delta_I_opt_eff_K[corr_status.map_Keff] = orbcor_results['Delta_I_opt_K']
    Delta_I_est_eff_K[corr_status.map_Keff] = orbcor_results['Delta_I_est_K']
    phi_est_eff_K[corr_status.map_Keff] = orbcor_results['Delta_phi_est_K']
    phi_opt_eff_K[corr_status.map_Keff] = orbcor_results['Delta_phi_opt_K']
    Delta_phi_est_eff_K[corr_status.map_Keff] = orbcor_results['Delta_phi_est_K']
    Delta_phi_opt_eff_K[corr_status.map_Keff] = orbcor_results['Delta_phi_opt_K']

    if 'f_opt' in orbcor_results.keys():
        f_opt = orbcor_results['f_opt']
        f_est = orbcor_results['f_est']
        Delta_f_opt = orbcor_results['Delta_f_opt']
        Delta_f_est = orbcor_results['Delta_f_est']
    else:
        f_opt = machine_status.f
        f_est = machine_status.f
        Delta_f_opt = 0.0
        Delta_f_est = 0.0

    runtime = default_timer() - t_begin
    logging.debug('Runtime was {:.04} s.'.format(runtime))

    return OptResults(timestamp = datetime.utcnow().timestamp(),
                      runtime = runtime,
                      kap_opt_eff_F = np.copy(kap_opt_eff_F),
                      kap_est_eff_F = np.copy(kap_est_eff_F),
                      I_opt_eff_K = np.copy(I_opt_eff_K),
                      I_est_eff_K = np.copy(I_est_eff_K),
                      Delta_I_opt_eff_K = np.copy(Delta_I_opt_eff_K),
                      Delta_I_est_eff_K = np.copy(Delta_I_est_eff_K),
                      phi_opt_eff_K = np.copy(phi_opt_eff_K),
                      phi_est_eff_K = np.copy(phi_est_eff_K),
                      Delta_phi_opt_eff_K = np.copy(Delta_phi_opt_eff_K),
                      Delta_phi_est_eff_K = np.copy(Delta_phi_est_eff_K),
                      f_opt = f_opt,
                      f_est = f_est,
                      Delta_f_opt = Delta_f_opt,
                      Delta_f_est = Delta_f_est,
                      R_clean_FK = np.copy(R_clean_FK),
                      R_residuals = np.linalg.matrix_rank(R_clean_FK, tol=0.03),
                      weighted_rms_est_eff = orbcor_results['weighted_rms_est'],
                      weighted_rms_est_eff_x = orbcor_results['weighted_rms_est_x'],
                      weighted_rms_est_eff_y = orbcor_results['weighted_rms_est_y'],
                      rms_est_eff = orbcor_results['rms_est'],
                      rms_est_eff_x = orbcor_results['rms_est_x'],
                      rms_est_eff_y = orbcor_results['rms_est_y'])


def plot_key_quantities(corr_status, bpm_status, parameters, settings, configs, **kwargs):
    """
    Parameters
    ----------
    corr_status: CorrStatus
    bpm_status: BPMStatus
    parameters: Parameters
    settings: Settings
    configs: Configs
    opt_results: OptResults, optional
    """
    # decoupled_flag has to be always True. Even if settings.decoupled_flag == True!
    R_clean_FK, sigma_Kh, sigma_Kv = resp.remove_singular_values_from_R_FK(parameters.R_FK, sigmas_to_remove_h=settings.sigmas_to_remove_h,
                                                       sigmas_to_remove_v=settings.sigmas_to_remove_v, Kh=configs.Kh, J=configs.Jx, decoupled_flag=True)
    R_clean_FeffKeff = resp.reduce_R_FK_to_R_FeffKeff(R_clean_FK, map_Feff=bpm_status.map_Feff, map_Keff=corr_status.map_Keff)
    R_clean_eff_FKeff = np.zeros((configs.F, len(corr_status.map_Keff)), np.float64)
    R_clean_eff_FKeff[bpm_status.map_Feff] = R_clean_FeffKeff[:]
    R_clean_eff_FK = np.zeros((configs.F, configs.K), np.float64)
    R_clean_eff_FK[:, corr_status.map_Keff] = R_clean_eff_FKeff[:, :]
    if 'opt_results' in kwargs.keys():
        plot.plot_I(corr_status.I_eff_K, kwargs['opt_results'].I_est_eff_K, corr_status.I_bounds_K2, corr_status.map_Keff, configs.ar_cor['idstr'], savefpath=path.join(configs.out_path, 'I.pdf'))
    else:
        plot.plot_I(corr_status.I_eff_K, np.zeros(configs.K), corr_status.I_bounds_K2, corr_status.map_Keff, configs.ar_cor['idstr'], savefpath=path.join(configs.out_path, 'I.pdf'))
    if 'opt_results' in kwargs.keys():
        plot.plot_kap(bpm_status.kap_eff_F, parameters.kap_ref_F, kwargs['opt_results'].kap_est_eff_F, bpm_status.err_kap_F, bpm_status.map_Feff, configs.ar_bpm['idstr'], savefpath=path.join(configs.out_path, 'kaps.pdf'))
    else:
        plot.plot_kap(bpm_status.kap_eff_F, parameters.kap_ref_F, np.zeros(configs.F), bpm_status.err_kap_F, bpm_status.map_Feff, configs.ar_bpm['idstr'], savefpath=path.join(configs.out_path, 'kaps.pdf'))

    plot.plot_R_FK(parameters.R_FK, norm=parameters.norm, bpm_idstr_F=configs.ar_bpm['idstr'], corr_idstr_K=configs.ar_cor['idstr'], title='R_FK', savefpath=path.join(configs.out_path, 'R_FK.pdf'))
    plot.plot_R_FK(R_clean_eff_FK, norm=parameters.norm, bpm_idstr_F=configs.ar_bpm['idstr'], corr_idstr_K=configs.ar_cor['idstr'], title='R_clean_eff_FK', savefpath=path.join(configs.out_path, 'R_clean_eff_FK.pdf'))
    plot.plot_R_FK(parameters.sig_R_FK, norm=parameters.norm, bpm_idstr_F=configs.ar_bpm['idstr'], corr_idstr_K=configs.ar_cor['idstr'], title='sig_R_FK', savefpath=path.join(configs.out_path, 'sig_R_FK.pdf'))
    plot.plot_sigmas(sigma_Kh, settings.sigmas_to_remove_h, plane='h', corr_idstr_K=configs.ar_cor['idstr'][:configs.Kh], savefpath=path.join(configs.out_path, 'sigma_Kh.pdf'))
    plot.plot_sigmas(sigma_Kv, settings.sigmas_to_remove_v, plane='v', corr_idstr_K=configs.ar_cor['idstr'][configs.Kh:], savefpath=path.join(configs.out_path, 'sigma_Kv.pdf'))
    plot.plot_Ws(parameters.W_FF, bpm_idstr_F=configs.ar_bpm['idstr'], savefpath=path.join(configs.out_path, 'W_F.pdf'))
    logging.info('Saved figures to {}.'.format(configs.out_path))
