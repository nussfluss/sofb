import os
import collections
import logging
from datetime import datetime
import pickle
import json
import numpy as np


def load(path, mode='json'):
    """
    Loads file from path.

    Parameters
    ----------
    path: str
    mode: str
        Either 'pk' or 'json'.

    Return
    ------
    obj: object
        Loaded object.
    """
    if '.pk' in path:
        mode = 'pk'
    elif '.json' in path:
        mode = 'json'
    if mode == 'pk':
        with open(path, 'rb') as f:
            obj = pickle.load(f)
    elif mode == 'json':
        with open(path, 'r') as f:
            obj = numpify(json.load(f))
    logging.debug('loaded file from {} (mode = {})'.format(path, mode))
    return obj


def save(obj, path, mode='json', prepend_date=False):
    """
    Saves object to path.

    Parameters
    ----------
    path: str
    mode: str
        Either 'pk' or 'json'.
    prepend_date: bool, default=False
        Insert 'YYYYMMDD-HHmmss_' before fname.
    """
    if prepend_date:
        head, tail =  os.path.split(path)
        path = head + '/' + datetime.utcnow().strftime('%Y%m%d_%H-%M-%S_') + tail
    if '.pk' in path or mode == 'pickle':
        with open(path, 'wb') as f:
            pickle.dump(obj, f)
    else:
        with open(path, 'w') as f:
            json.dump(jsonify(obj), f, indent=4)
    logging.debug('saved file to {}'.format(path))


def all_fpaths_in_path(path):
    """
    Lists all files in path.

    Return
    ------
    fpaths: list
        File paths as string.
    """
    fpaths = []
    for p in os.listdir(path):
        p = os.path.join(path, p)
        if os.path.isdir(p):
           fpaths += all_fpaths_in_path(p)
        else:
           fpaths.append(p)
    return fpaths

def jsonify(obj):
    """
    Converts nested lists and tuples containing json-UNserializable, nested numpy arrays into json-serializable nested iterables:
      1. Converts any numpy data types into base data types
      2. Converts np.nan, np.inf, np.neginf into "nan", "posinf" and "neginf"
      3. Converts complex numbers into tuples: ("C", number.real, number.imag)

    Parameters
    ----------
    obj: iterable

    Return
    ------
    obj: list
    """

    def _jsonify(obj):
        if isinstance(obj, collections.abc.Iterable) and not isinstance(obj, str):
            if isinstance(obj, dict):
                for key, value in obj.items():
                    obj[key] = _jsonify(value)
                return obj
            elif isinstance(obj, (list, tuple, np.ndarray)):
                return tuple(_jsonify(o) for o in obj)

        else:
            # convert nan and inf
            try:
                if np.isnan(obj):
                    return 'nan'
            except TypeError:
                pass
            try:
                if np.isposinf(obj):
                    return 'posinf'
            except TypeError:
                pass
            try:
                if np.isneginf(obj):
                    return 'neginf'
            except TypeError:
                pass

            # convert data types
            if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                                np.int16, np.int32, np.int64, np.uint8,
                                np.uint16, np.uint32, np.uint64)):
                return int(obj)
            elif isinstance(obj, (np.float_, np.float16, np.float32, np.float64)):
                return float(obj)
            elif isinstance(obj, (np.complex64, np.complex128)):
                return ('C', obj.real, obj.imag)
        return obj

    if isinstance(obj, collections.abc.Iterable) and not isinstance(obj, str):
        return _jsonify(obj)
    else:
        raise TypeError('jsonify was presented with an object of type {} while it can convert iterables only'.format(type(obj)))


def numpify(obj, toarrays=False):
    """
    Undoes some changes applied by jsonify:
      1. converts strings as "nan", "posinf" and "neginf" back to np.nan, np.inf and np.neginf
      2. Converts tuples ("C", number1, number2) to complex numbers number1 + 1.j*number2

    Parameters
    ----------
    obj: iterable
    toarrays: bool
        Try to convert obj and all sub-iterables to ndarrays.

    Return
    ------
    obj: iterable
    """

    def _numpify(obj):
        if isinstance(obj, collections.abc.Iterable):
            if len(obj) == 3 and (isinstance(obj, list) or isinstance(obj, tuple)):
                if isinstance(obj[0], str) and isinstance(obj[1], float) and isinstance(obj[2], float):
                    return obj[1] + 1.j*obj[2]
            if isinstance(obj, dict):
                for key, value in obj.items():
                    obj[key] = _numpify(value)
                return obj
            elif isinstance(obj, str):
                if obj == 'nan': return np.nan
                elif obj == 'posinf': return np.inf
                elif obj == 'neginf': return -1 * np.inf
                else: return obj
            if isinstance(obj, (tuple, list)):
                if toarrays:
                    try:
                        return np.asarray([_numpify(o) for o in obj])
                    except ValueError:
                        return [_numpify(o) for o in obj]
                else:
                    return [_numpify(o) for o in obj]
        else:
            return obj

    if isinstance(obj, collections.abc.Iterable) and not isinstance(obj, str):
        return _numpify(obj)
    else:
        raise TypeError('numpify was presented with an object of type {} while it can convert iterables only'.format(type(obj)))



def load_reference_dat(path='korrektur.test'):
    """
    Loads orbit reference and weights. Handles old format (for example: 'reference.190501-1') when there is a 'reference' present in the file path. All deferring file paths are handled as new format.

    Parameters
    ----------
    path: str
        file path

    Return
    ------
    dat: dict
        Loaded orbit reference, weights and BPM IDs. Default BPM IDs are returned for all old-formatted input files.
    """

    load_old_format = False
    try:
        dat = load(path)
    except json.decoder.JSONDecodeError:
        load_old_format = True

    if load_old_format:
        kap_ref_x = list()
        kap_ref_z = list()
        w_x = list()
        w_z = list()
        status_x = list()
        status_z = list()

        # copy data from ascii-file to dict
        with open(path, 'r') as f:
            for line in f.readlines()[9:9+54]:
                line = line.split()

                kap_ref_x.append(line[1])
                kap_ref_z.append(line[2])
                w_x.append(line[5])
                w_z.append(line[6])
                status_x.append(line[-2])
                status_z.append(line[-1])

        # convert to ndarray
        kap_ref = np.asarray([kap_ref_x, kap_ref_z], float)
        w = np.asarray([w_x, w_z], float)
        status_F = np.asarray([status_x, status_z], float)

        # convert weights vector to diag matrix
        W = np.zeros((2, len(w_x), len(w_x)), float)
        W[0, np.diag_indices_from(W[0])] = w[0]
        W[1, np.diag_indices_from(W[1])] = w[1]

        kap_ref_F = kap_ref.flatten()
        W_FF = np.zeros((kap_ref_F.shape[0], kap_ref_F.shape[0]), dtype=np.float64)
        W_FF[np.diag_indices_from(W_FF)] = w.flatten()
        status_F = status_F.flatten()

        # correct entries
        for n in range(len(status_F)):
            if status_F[n] == 0:
                status_F[n] = 1
            else:
                status_F[n] = 0

        dat = {'dat': {'kap_ref_F': kap_ref_F,
                        'W_FF': W_FF,
                        'use_F': status_F,
                        'bpm_idstr_F': ['xbpm{:02}'.format(jx+1) for jx in range(54)] + ['ybpm{:02}'.format(jz+1) for jz in range(54)]},
                'debug': {'comment': None},
                'machine': {'saw': None,
                            'fuertsch': None}}

    logging.debug('loaded kap_ref_F and W_FF from {}'.format(path))
    return dat

def load_reference(path, bpm_idstr_F):
    """
    Wraps load_reference_dat.

    Loads orbit reference, weights and possibly BPM usage from path and maps them to supplied BPM IDs. Usage is only loaded if an old reference file is supplied.

    Parameters
    ----------
    path: str
        File path.
    bpm_idstr_F: list
        BPM IDs.

    Returns
    -------
    kap_ref_F: float (F,)-ndarray
        Orbit reference in x plane and y plane.
    W_FF: float (F, F)-ndarray
        Weights in x plane and y plane.
    use_F: int (F,)-ndarray
        Usage in x plane and y plane.
    """
    F = len(bpm_idstr_F)

    dat = load_reference_dat(path)
    bpm_idstr_Fl = dat['dat']['bpm_idstr_F']
    kap_ref_Fl = dat['dat']['kap_ref_F']
    W_FlFl = np.asarray(dat['dat']['W_FF'], np.float64)
    Fl = len(bpm_idstr_Fl)

    # map kap_ref_F and W_FF
    kap_ref_F = np.zeros(F, np.float64)
    W_FF = np.zeros((F, F), np.float64)
    for f in range(F):
        for fl in range(Fl):
            if bpm_idstr_F[f] == bpm_idstr_Fl[fl]:
                kap_ref_F[f] = kap_ref_Fl[fl]
                W_FF[f, f] = W_FlFl[fl, fl]
                break
            elif fl == Fl-1:
                logging.debug('load_reference: reference file does not contain.debugrmation for {} kap_ref_F and W_FF'.format(bpm_idstr_F[f]))

    # map use_F
    if 'use_F' in dat['dat'].keys():
        use_Fl = dat['dat']['use_F']
        use_F = np.zeros((F,), np.int)
        for f in range(F):
            for fl in range(Fl):
                if bpm_idstr_F[f] == bpm_idstr_Fl[fl]:
                    use_F[f] = use_Fl[fl]
                    break
                elif fl == Fl-1:
                    logging.debug('load_reference: reference file does not contain.debugrmation for {} to fill use_F'.format(bpm_idstr_F[f]))
    else:
        logging.warning('reference file {} does not contain use_F'.format(path))
        use_F = None

    return kap_ref_F, W_FF, use_F


def save_reference(kap_ref_F, W_FF, bpm_idstr_F, path, use_F=None, saw_flag=None, fuertsch_flag=None, comment=None):
    """
    Saves orbit reference and weights to path.

    Parameters
    ----------
    kap_ref_F: float (F)-ndarray
        Orbit reference in x plane and y plane.
    W_FF: float (F, F)-ndarray
        Weights in x plane and y plane.
    bpm_idstr_F: list
        BPM IDs.
    path: str
        File path.
    use_F: float (F,)-ndarray, optional
        BPM usage.
    saw_flag: bool, optional
    fuertsch_flag: bool, optional
    comment: str, optional
    """
    dat = {'dat': {'kap_ref_F': kap_ref_F,
                   'W_FF': W_FF,
                   'use_F': use_F,
                   'bpm_idstr_F': bpm_idstr_F},
           'debug': {'comment': comment},
           'machine': {'saw': saw_flag,
                       'fuertsch': fuertsch_flag}}
    save(dat, path)


def save_response(R_FK, Rd_F, sig_R_FK, bpm_idstr_F, corr_idstr_K, norm, path, saw_flag=None, fuertsch_flag=None, q_M=None, comment=None):
    """
    Saves orbit-response to path.

    Parameters
    ----------
    R_FK: float (F, K)-ndarray
        Orbit response matrix.
    Rd_F: float (F,)-ndarray
        Revolution-frequency orbit response.
    sig_R_FK: float (F, K)-ndarray
        Residuals of orbit response.
    bpm_idstr_F: list
        BPM IDs.
    corr_idstr_K: list
        Steerer IDs.
    norm: str
        Units of R_FK.
    path: str
        File path.
    saw_flag: bool, optional
    fuertsch_flag: bool, optional
    q_M: array-like, optional
    comment: str, optional
    """
    dat = {'dat': {'R_FK': R_FK,
                   'Rd_F': np.zeros((R_FK.shape[0],), np.float64),
                   'sig_R_FK': sig_R_FK,
                   'q_M': q_M,
                   'corr_idstr_K': corr_idstr_K,
                   'bpm_idstr_F': bpm_idstr_F},
           'debug': {'norm': norm,
                    'comment': comment},
           'machine': {'saw': saw_flag,
                       'fuertsch': fuertsch_flag}}
    save(dat, path)

def load_response(path, bpm_idstr_F, corr_idstr_K):
    """
    Wraps load_response_dat.

    Loads orbit-response.debugrmation from path and map loaded orbit-response matrix to BPM IDs and Steeer IDs.

    Parameters
    ----------
    path: str
        File path to input file.
    bpm_idstr_F: list
        BPM IDs.
    corr_idstr_K: list
        Steerer IDs.

    Returns
    -------
    R_FK: float (F, K)-ndarray
        Mapped orbit-response matrix.
    Rd_F: float (F,)-ndarray
        Revolution-frequency orbit response.
    sig_R_FK: float (F, K)-ndarray
        Residuals of orbit-response matrix.
    norm: str
        Units of R_FK.
    """
    F = len(bpm_idstr_F)
    K = len(corr_idstr_K)
    R_FK = np.zeros((F, K), np.float64)
    Rd_F = np.zeros((F,), np.float64)
    sig_R_FK = np.zeros((F, K), np.float64)

    dat = load_response_dat(path)
    norm = dat['debug']['norm']
    bpm_idstr_Fl = dat['dat']['bpm_idstr_F']
    corr_idstr_Kl = dat['dat']['corr_idstr_K']
    R_FlKl = np.asarray(dat['dat']['R_FK'], np.float64)
    Rd_Fl = np.asarray(dat['dat']['Rd_F'], np.float64)
    sig_R_FlKl = np.asarray(dat['dat']['sig_R_FK'], np.float64)
    Fl = len(bpm_idstr_Fl)
    Kl = len(corr_idstr_Kl)

    for k in range(K):
        for kl in range(Kl):
            if corr_idstr_K[k] == corr_idstr_Kl[kl]:
                for f in range(F):
                    for fl in range(Fl):
                        if bpm_idstr_F[f] == bpm_idstr_Fl[fl]:
                            R_FK[f, k] = R_FlKl[fl, kl]
                            Rd_F[f] = Rd_Fl[fl]
                            sig_R_FK[f, k] = sig_R_FlKl[fl, kl]
                            break
                break
            elif kl == Kl-1:
                logging.debug('load_R_FK: response file does not contain.debugrmation for {}'.format(corr_idstr_K[k]))
    return R_FK, Rd_F, sig_R_FK, norm


def load_response_dat(path):
    """
    Loads orbit-response.debugrmation from path. Path strings containing "response" will be read as old format. All other paths will be read as new format.

    Parameters
    ----------
    path: str
        File path to input file.

    Return
    ------
    dat: dict
    """

    load_old_format = False
    try:
        ext = 'json'
        with open(path, 'r') as f:
            dat = json.load(f)
    except json.decoder.JSONDecodeError:
        load_old_format = True

    if load_old_format:

        # ---------------------------
        # load a legacy response file
        # ---------------------------

        ext = 'response'
        R_FK = np.zeros((108, 56), np.float64)
        sig_R_FK = np.zeros((108, 56), np.float64)

        with open(path, 'r') as f:
            lines = f.readlines()

        for n in range(len(lines)):
            line = lines[n]

            def read_corr_data(n):
                """
                Reads orbit response data
                    'vk01 {
                           ...
                           }'
                for a single corrector. The final orbit response is generated by averaging over the four available orbit
                reponses for +0.050 mrad, +0.025 mrad, -0.025 mrad and -0.050 mrad.

                Parameters:
                -----------
                n : int
                    line number
                """

                def convert_line_duo(n, plane):
                    """
                    Converts data from 'line duo' to a row of a orbit response matrix R_F.

                    The 'line duo' refers to two consecutive lines in the response file corresponding to the horizontal
                    orbit response in the first line and the vertical orbit response in the second line for a horizontal
                    corrector. For a vertical corrector, the vertical orbit response corresponds to the first line and
                    the horizontal orbit response corresponds to the second line.

                    Example:
                    --------
                    ...
                       {{+0.025 mrad} 40.0} {{   2.197e+00   9.009e+00   1.057e+01   1.042e+01   9.570e+00   7.764e+00   6.152e+00   2.686e+00   5.615e-01  -2.393e+00  -7.813e+00  -1.414e+01  -8.545e+00  -8.985e+00  -8.808e+00  -1.015e+01  -1.575e+01  -9.595e+00  -3.467e+00  -1.172e+00   1.221e+00   4.688e+00   6.836e+00   8.154e+00   9.937e+00   1.023e+01   9.375e+00  -4.883e-02  -6.616e+00  -8.887e+00  -1.028e+01  -1.038e+01  -8.911e+00  -8.350e+00  -6.323e+00  -4.590e+00  -3.589e+00   1.465e-01   1.758e+00   2.979e+00   5.371e+00   1.067e+01   7.235e+00   2.598e-01  -8.996e-01  -5.371e+00  -5.225e+00  -6.470e+00  -7.886e+00  -8.179e+00  -8.521e+00  -8.252e+00  -5.933e+00  -4.053e+00} {   2.197   8.960}}
                                            {{  -7.324e-01  -7.324e-02   0.000e+00  -1.489e+00   4.150e-01   1.709e-01  -2.441e-02  -2.441e-01   1.953e-01   7.324e-02   0.000e+00  -1.953e-01  -2.190e-01   0.000e+00  -5.302e-01  -1.260e-02  -1.440e+00   1.709e-01  -2.441e-02   0.000e+00  -1.709e-01   2.441e-02   7.324e-02   1.709e-01  -4.150e-01  -1.953e-01  -3.662e-01   1.221e-01   1.953e-01  -1.709e-01  -4.883e-02   1.465e-01  -4.883e-02  -2.930e-01  -1.221e-01   0.000e+00  -2.441e-02   4.883e-02   7.324e-02   9.766e-02  -1.465e-01   0.000e+00  -3.772e-02  -6.280e-02   2.161e-01   0.000e+00   2.442e-02  -1.953e-01  -2.441e-02  -3.906e-01  -3.906e-01   5.615e-01   3.662e-01   0.000e+00} {  -0.781   0.049}}
                    ...

                     Notes:
                     - The '40.0' hails from '0.025 rad * 40.0 = 1 rad'. Only god knows why its written there.
                     - The last two embraced values correspond to two test bpms.
                     - Regardless of the preceeding deflection angle +0.050 mrad, +0.025 mrad, -0.025 mrad and -0.050 mrad,
                       the orbit reponse seems to have the units mm/mrad. I suppose each line contains

                                <kap_F>(0 rad) - <kap_F>(<deflection_angle>)
                                -----------------------------------
                                              <deflection_angle>

                        with <defection_angle> in {0.025rad|-0.025 rad ... }. The orbit response is in consequence the
                        average of all entries.

                    Parameters:
                    -----------
                    n : int
                        line number
                    plane : str
                        'v' or 'h'
                    """
                    line1 = lines[n]
                    line2 = lines[n + 1]

                    # remove braces
                    for brace in ['{', '}']:
                        while line1.find(brace) > -1:
                            line1 = line1.replace(brace, '')
                        while line2.find(brace) > -1:
                            line2 = line2.replace(brace, '')

                    # convert to array
                    items1 = line1.split()
                    items2 = line2.split()
                    phi = float(items1[0])
                    R1_F = list()  # horizontal or vertical orbbit response
                    R2_F = list()  # horizontal or vertical orbbit response
                    for item in items1[3:54 + 3]:
                        R1_F.append(float(item))
                    for item in items2[0:54]:
                        R2_F.append(float(item))
                    R1_F = np.asarray(R1_F, np.float64)
                    R2_F = np.asarray(R2_F, np.float64)
                    if plane == 'h':
                        R_F = np.concatenate((R1_F, R2_F,), axis=0)
                    else:
                        R_F = np.concatenate((R2_F, R1_F,), axis=0)
                    return R_F, phi

                idstr = lines[n][:4]
                plane = idstr[0]
                if plane == 'h':
                    k = int(idstr[2:]) - 1
                else:
                    k = int(idstr[2:]) + 30 - 1
                # logging.debug('\n\nreading response for corrector {} into column {} of R_FK:'.format(idstr, k))
                n += 1
                R_Fs = list()
                phis = list()
                while not lines[n][0] == '}':
                    R_F, phi = convert_line_duo(n=n, plane=plane)
                    R_Fs.append(R_F)
                    phis.append(phi)
                    n += 2

                R_Fs = np.asarray(R_Fs)
                phis = np.asarray(phis)
                R_FK[:, k] = np.average(R_Fs, axis=0)
                sig_R_FK[:, k] = np.std(R_Fs, axis=0)

            if line.find('saw-state') > -1:
                if line.find('AN') > -1:
                    saw_flag = True
                else:
                    saw_flag = False
            elif line.find('tune') > -1:
                line = line.split()
                mu_M = np.asarray((float(line[1].replace('{', '')), float(line[2].replace('}', ''))),
                                  np.float64) * 2 * np.pi
            elif line.find('hk') > -1 or line.find('vk') > -1:
                read_corr_data(n)

        dat = {'dat': {'R_FK': R_FK,
                       'Rd_F': np.zeros((R_FK.shape[0]), np.float64),
                       'sig_R_FK': sig_R_FK,
                       'mu_M': mu_M,
                       'corr_idstr_K': ['hk{:02}'.format(kh+1) for kh in range(30)] + ['vk{:02}'.format(kv+1) for kv in range(26)],
                       'bpm_idstr_F': ['xbpm{:02}'.format(jx+1) for jx in range(54)] + ['ybpm{:02}'.format(jz+1) for jz in range(54)]},
               'debug': {'cleaned_flag': False,
                        'norm': 'mm/mrad'},
               'machine': {'saw': saw_flag,
                           'fuertsch': None}}

    if np.max(dat['dat']['R_FK']) < 200.00:
        logging.debug('load_R_FK: renormalizing obit response from mm/mrad to mm/rad')
        dat['dat']['R_FK'] *=  1e3
        dat['dat']['sig_R_FK'] *=  1e3
        dat['debug']['norm'] = 'mm/rad'

    logging.debug('loaded R_FK and norm from {}'.format(path))
    logging.debug('details:  <ext> = {}, norm = {}, SAW = {}'.format(path, ext, dat['debug']['norm'], dat['machine']['saw']))

    return dat


def read_csv(fpath, spacer=',', tup=False):
    """
    Reads in a csv-file which fulfills:
        1. comment lines start with #
        2. comma (or spacer) is used as separator

    Parameters:
    -----------
    fpath : str
        filepath to csv
    spacer : str
        separator of csv file. Default: spacer = ','.

    Returns:
    --------
    dat : list
        csv file as nested list
    """

    # fpath = abspath(path=fpath)

    dat = list()
    with open(fpath, 'r') as f:
        for line in f.readlines():
            if not line[0] == '#':
                line = line.split(spacer)
                for i in range(len(line)):
                    line[i] = line[i].strip()

                    # converts value to float if possible
                    # => ensures that asarray() doesnt produce casting errors
                    try:
                        line[i] = float(line[i])
                    except:
                        pass

                if tup:
                    dat.append(tuple(line))
                else:
                    dat.append(line)
    return dat
