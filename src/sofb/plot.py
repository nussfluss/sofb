import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
from os import path
import logging


def plot_kap(kap_F, kap_ref_F, kap_est_F, sigma_kap_F, map_Feff, bpm_idstr_F, savefpath=None):

    F = kap_F.shape[0]

    fig, ax = plt.subplots()
    ax.set_title('compare orbit, reference and std. dev. vs. BPMs')
    ax.plot(kap_ref_F, '--', linewidth=1.0, color='g', label='should')
    ax.plot(kap_F, linewidth=1.0, color='r', label='is')
    ax.plot(kap_est_F, linewidth=1.3, color='g', label='est')
    ax.set_xlim((0, F))
    ax.set_ylabel('orbit deviation / mm')
    # ax.tick_params('y', colors='r')
    ax.set_xticks(np.arange(F))
    ax.set_xticklabels(bpm_idstr_F, rotation=90, fontsize=5)
    ax.legend()
    ticklabels = ax.xaxis.get_ticklabels()
    for f in range(F):
        if f in map_Feff:
            ticklabels[f].set_color('green')
        else:
            ticklabels[f].set_color('red')
    ax_2 = ax.twinx()
    ax_2.bar(np.arange(F), sigma_kap_F, label='std deviation', color='b')
    ax_2.axhline(0.01, linestyle='--', color='b')
    ax_2.set_xlim((0, F))
    ax_2.set_ylim((0, 0.1))
    ax_2.set_ylabel(r'std. dev. / mm', color='b')
    ax_2.tick_params('y', colors='b')
    plt.grid()
    plt.tight_layout()

    if savefpath is not None:
        plt.savefig(savefpath, mode='pdf')
        logging.info('saved figure to ' + savefpath)


def plot_kap_prem(kap_F, kap_prem_F, map_Feff, bpm_idstr_F, title='', savefpath=None):
    F = kap_F.shape[0]

    fig, ax = plt.subplots()
    ax.set_title(title)
    ax.set_ylabel('deviation / mm')
    ax.plot(kap_F, '--', linewidth=1.0, color='b', label='measurement')
    ax.plot(kap_prem_F, linewidth=1.0, color='r', label='undistorted')
    ax.set_xlim((0, F))
    ax.set_xticks(np.arange(F))
    ax.set_xticklabels(bpm_idstr_F, rotation=90, fontsize=5)
    ticklabels = ax.xaxis.get_ticklabels()
    for f in range(F):
        if f in map_Feff:
            ticklabels[f].set_color('green')
        else:
            ticklabels[f].set_color('red')
    plt.legend()
    plt.grid()
    plt.tight_layout()

    if savefpath is not None:
        plt.savefig(savefpath, mode='pdf')
        logging.info('saved figure to ' + savefpath)

def plot_I(I_K, I_est_K, constraint_K2, map_Keff, bpm_idstr_K, title='', savefpath=None):
    K = I_K.shape[0]

    fig, ax = plt.subplots()
    ax.set_title(title)
    ax.set_ylabel('I / A')
    ax.plot(I_K, '--', linewidth=1.0, color='b', label='measured')
    ax.plot(I_est_K, linewidth=1.0, color='r', label='estimated')
    ax.plot(constraint_K2[:, 0], '--', linewidth=1.0, color='0', label='limits')
    ax.plot(constraint_K2[:, 1], '--', linewidth=1.0, color='0', label='limits')
    ax.set_xlim((0, K))
    ax.set_xticks(np.arange(K))
    ax.set_xticklabels(bpm_idstr_K, rotation=90, fontsize=5)
    ticklabels = ax.xaxis.get_ticklabels()
    for k in range(K):
        if k in map_Keff:
            ticklabels[k].set_color('green')
        else:
            ticklabels[k].set_color('red')
    plt.legend()
    plt.grid()
    plt.tight_layout()

    if savefpath is not None:
        plt.savefig(savefpath, mode='pdf')
        logging.info('saved figure to ' + savefpath)

def plot_Delta_I(Delta_I_est_K, map_Keff, bpm_idstr_K, title='', savefpath=None):
    K = kap_K.shape[0]

    fig, ax = plt.subplots()
    ax.set_title(title)
    ax.set_ylabel('I / A')
    ax.bar(np.arange(K), Delta_I_K, color='b', label='measurement')
    ax.set_xlim((0, K))
    ax.set_xticks(np.arange(K))
    ax.set_xticklabels(bpm_idstr_K, rotation=90, fontsize=5)
    ticklabels = ax.xaxis.get_ticklabels()
    for k in range(K):
        if k in map_Keff:
            ticklabels[k].set_color('green')
        else:
            ticklabels[k].set_color('red')
    plt.legend()
    plt.grid()
    plt.tight_layout()

    if savefpath is not None:
        plt.savefig(savefpath, mode='pdf')
        logging.info('saved figure to ' + savefpath)

def plot_R_FK(R_FK, norm, bpm_idstr_F, corr_idstr_K, title='', savefpath=None):

    F = R_FK.shape[0]
    K = R_FK.shape[1]

    plt.figure()
    ax = plt.gca()
    im = ax.imshow(R_FK, interpolation='nearest')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(im, label=norm, cax=cax)
    ax.set_yticks(np.arange(F))
    ax.set_yticklabels(bpm_idstr_F, fontdict={'fontsize': 4})
    ax.set_xticks(np.arange(K))
    ax.set_xticklabels(corr_idstr_K, fontdict={'fontsize': 4}, rotation=90)
    plt.title(title)
    plt.tight_layout()

    if savefpath is not None:
        plt.savefig(savefpath, mode='pdf')
        logging.info('saved figure to ' + savefpath)


def plot_sigmas(sigma_K, sigmas_to_remove, plane, corr_idstr_K, savefpath=None):

    K = sigma_K.shape[0]

    # horizontal singular values
    plt.figure()
    K = sigma_K.shape[0]
    if sigmas_to_remove > 0:
        plt.bar(np.arange(K-sigmas_to_remove), sigma_K[:-sigmas_to_remove], color='g', label='used')
        plt.bar(np.arange(K-sigmas_to_remove, K), sigma_K[-sigmas_to_remove:], color='r', label='discarded')
    else:
        plt.bar(np.arange(K), sigma_K, color='g', label='used')
    plt.yscale('log')
    plt.ylabel('singular value')
    plt.xticks(np.arange(K), corr_idstr_K, rotation=90, fontsize=5)
    plt.title('singular values {}'.format(plane))
    plt.tight_layout()

    if savefpath is not None:
        plt.savefig(savefpath, mode='pdf')
        logging.info('saved figure to ' + savefpath)


def plot_Ws(W_FF, bpm_idstr_F, savefpath=None):
    F = W_FF.shape[0]

    plt.figure()
    W_F = np.diag(W_FF)
    plt.bar(np.arange(W_F.shape[0]), W_F)
    plt.ylabel('weight')
    plt.title('effective weights')
    plt.xticks(np.arange(F), bpm_idstr_F, rotation=90, fontsize=5)
    plt.grid()
    plt.tight_layout()

    if savefpath is not None:
        plt.savefig(savefpath, mode='pdf')
        logging.info('saved figure to ' + savefpath)

