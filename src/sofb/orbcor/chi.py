import enum
import logging
from timeit import default_timer
import pickle as pk
import numpy as np
from scipy.optimize import fmin_l_bfgs_b
import cvxopt


class Solvers(enum.Enum):
    SVD = 0
    L_BFGS_B = 1
    QPCONE = 2


class PathLength(enum.Enum):
    IGNORE = 0
    LOCK = 1
    MINIMIZE = 2


def cal_norm(kap, kap_ref):
    """root of sum of squares"""
    return np.linalg.norm(kap - kap_ref)


def cal_weighted_norm(kap, kap_ref, W):
    return np.linalg.norm(np.dot(W, (kap - kap_ref)))


def cal_rms(kap, kap_ref):
    return np.sqrt(np.mean(np.square(kap - kap_ref)))


def cal_weighted_rms(kap, kap_ref, W):
    return np.sqrt(np.mean(np.square(np.dot(W, kap - kap_ref))))


def cal_weighted_norm_err(kap_F, kap_ref_F, W_FF, err_kap_F):
    """implements derivation 'Determine Error of Chi' found in log file"""
    weighted_norm = cal_weighted_norm(kap_F, kap_ref_F, W_FF)
    return np.linalg.norm(np.dot(np.dot(np.transpose(err_kap_F), W_FF), np.dot(W_FF, kap_F - kap_ref_F))) / weighted_norm


def cal_norm_err(kap_F, kap_ref_F, err_kap_F):
    norm = cal_norm(kap_F, kap_ref_F)
    return np.linalg.norm(np.dot(np.transpose(err_kap_F), kap_F - kap_ref_F)) / norm


def cal_weighted_rms_err(*args, **kwargs):
    return 0.0


def cal_rms_err(*args, **kwargs):
    return 0.0


def minimize_chi_sq(kap_F,
                    kap_ref_F,
                    phi_K,
                    phi_bounds_K2,
                    R_FK,
                    W_FF,
                    f=None,
                    Rd_F=None,
                    f_bounds_2=None,
                    weight_f=1.0,
                    orbit_slack_F=None,
                    Jx=None,
                    method=Solvers.QPCONE,
                    path_length_mod=PathLength.IGNORE,
                    minimize_steerer_strength_flag=True,
                    factor=0.7,
                    *args,
                    **kwargs):
    """
    This function determines an optimal vector of angles

        Delta_phi_opt_K

    which minimizes the objective functional

        ||W_FF @ (Delta_kap_F + R_FK @ Delta_phi_opt_K) + Rd_F * Delta_f_opt ||_2

    via one of three methods: Either SVD from scipy, the L-BFGS-B optimization algorithm from scipy or
    the qpcone solver from cvxopt. The last two solvers support box constraints for the angles and the last
    solver additionally supports box constraints for the orbit.

    Correction angles will be equally distributed among linear dependent steerer magnets by default. If

        orbit_slack_F

    is specified, the vector of optimal angles will be reduced at the cost of moving the optimized orbit within

        kap_opt_F +- orbit_slack_F.


    Parameters
    ----------
    kap_F :  float (J)-ndarray
        orbit deviations
    kap_ref_F : float (J)-ndarray
        orbit reference
    phi_K : float(K)-ndarray
        vector of corrector currents
    phi_bounds_K2 : float (K, 2)-tuples, ignored by method = 'svd'
        a tuple of K (phi_min, phi_max)-tuples
    R_FK : float (F, K)-ndarray
        orbit response matrix
    W_FF : float (F, F)-ndarray
        diagonal matrix of weights
    f: float
        rf frequency
    Rd_F: float (F,)-np.ndarray, optional
        frequency orbit response
    f_bounds_2: float (2,)-np.ndarray, optional
        frequency constraints. If Rd_F is supplied, these constraints have to be applied, as well.
    weight_f: float
        frequency weight in modified orbit-correction problem
    orbit_slack_F : None or float (F,)-ndarray
        If None, current minimization will be omitted.
    Jx : int, optional
        nu        f = 0.0
        Rd_F = np.zeros((F,), np.float64)
mber of horizontal BPMs. defaults to int(F/2).
    constraint_F2 : faboultering the rf frequency :math:`f` to loat (F, 2)-ndarray
        optional (see minimize_chi_sq_within_bounds_qpcone() for details)
    maxiter : int
        maximum number of iterations of solver. default : maxiter = 10e6
    method: Enum
        either SVD, L-BFGS-B or QPCone
    path_length_mod: Enum
        either IGNORE, LOCK or MINIMIZE path length
    minimize_steerer_strength_flag: bool
        flag to minimize steerer strengths after minimization of orbit errors. Finds unique solution for
        linear dependent steerer magents. Note, unflagging is ignored if "path_length_mod == PathLength.Minimize".


    Return
    ------
    results : dict
        See below for details!
    """

    F = R_FK.shape[0]
    if Jx is None:
        Jx = int(F/2.0)
        logging.debug('Jx was not supplied! Using default value.')
    K = R_FK.shape[1]
    Kd = K + 1

    if Rd_F is None or f_bounds_2 is None or f is None:
        path_length_mod = PathLength.IGNORE
        logging.debug('Parameters missign: Ignoring path length!')

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                           solve orbit-correction problem and minimize steerer strengths
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    if path_length_mod is PathLength.IGNORE:
        logging.debug('Solving orbit-correction IGNORING dispersion.')
        if method is Solvers.QPCONE:
            results = minimize_chi_sq_within_bounds_qpcone(kap_F=kap_F, kap_ref_F=kap_ref_F, phi_K=phi_K, phi_bounds_K2=phi_bounds_K2, R_FK=R_FK, W_FF=W_FF, **kwargs)
        elif method is Solvers.L_BFGS_B:
            results = minimize_chi_sq_within_bounds_lbfgsb(kap_F=kap_F, kap_ref_F=kap_ref_F, phi_K=phi_K, phi_bounds_K2=phi_bounds_K2, R_FK=R_FK, W_FF=W_FF, **kwargs)
            results.update({'constraint_F2': [[None] for f in range(F)]})
        elif method is Solvers.SVD:
            results = minimize_chi_sq_globally_svd(kap_F=kap_F, kap_ref_F=kap_ref_F, phi_K=phi_K, phi_bounds_K2=phi_bounds_K2, R_FK=R_FK, W_FF=W_FF, **kwargs)
            results.update({'constraint_F2': [[None] for f in range(F)]})
            results.update({'func_val_0': np.nan})
        else:
            raise Exception('Solver id string not recognized.')

        if minimize_steerer_strength_flag:
            logging.debug('Minimizing currents.')
            corrected_results = minimize_steerer_strength(phi_K=phi_K+results['Delta_phi_opt_K'], phi_bounds_K2=phi_bounds_K2, R_FK=R_FK, 
                                                        orbit_slack_F=orbit_slack_F, **kwargs)

            Delta_phi_opt_K = results['Delta_phi_opt_K'][:K] + corrected_results['Delta_phi_opt_K'][:K]
        else:
            Delta_phi_opt_K = results['Delta_phi_opt_K'][:K]
        Delta_phi_est_K = factor * Delta_phi_opt_K
        kap_opt_F = kap_F + np.dot(R_FK, Delta_phi_opt_K)
        kap_est_F = kap_F + np.dot(R_FK, Delta_phi_est_K)

    elif path_length_mod is PathLength.LOCK:
        logging.debug('Solving orbit-correction problem under locked path length.')

        Fd = F + 1
        Kd = K + 1
        R_FdKd = np.zeros((Fd, Kd), np.float64)
        R_FdKd[:F, :K] = R_FK
        R_FdKd[:F, -1] = Rd_F
        R_FdKd[-1, :K] = np.ones((K,), np.float64)
        bounds_Kd2 = np.concatenate((phi_bounds_K2, f_bounds_2.reshape(1, 2)), axis=0)
        value_Kd = np.concatenate((phi_K, [f]), axis=0)
        kap_Fd = np.concatenate((kap_F, [0]), axis=0)
        kap_ref_Fd = np.concatenate((kap_ref_F, [0]), axis=0)
        W_FdFd = np.zeros((Fd, Fd), dtype=np.float64)
        W_FdFd[:F, :F] = W_FF
        W_FdFd[-1, -1] = weight_f



        if 'constraint_F2' in kwargs.keys():
            constraint_Fd2 = np.concatenate((kwargs['constraint_F2'], np.asarray([[-np.inf, np.inf]])), axis=0)
            kwargs['constraint_F2'] = constraint_Fd2

        if method is Solvers.QPCONE:
            results = minimize_chi_sq_within_bounds_qpcone(kap_F=kap_Fd, kap_ref_F=kap_ref_Fd, phi_K=value_Kd, phi_bounds_K2=bounds_Kd2, R_FK=R_FdKd, W_FF=W_FdFd, **kwargs)
            results['Delta_phi_0_K'] = None
            results['phi_bounds_eff_K2'] = results['phi_bounds_eff_K2'][:K]
            results['constraint_F2'] = results['constraint_F2'][:F]
        elif method is Solvers.L_BFGS_B:
            results = minimize_chi_sq_within_bounds_lbfgsb(kap_F=kap_Fd, kap_ref_F=kap_ref_Fd, phi_K=value_Kd, phi_bounds_K2=bounds_Kd2, R_FK=R_FdKd, W_FF=W_FdFd, **kwargs)
            results['Delta_phi_0_K'] = results['Delta_phi_0_K'][:K]
            results['phi_bounds_eff_K2'] = results['phi_bounds_eff_K2'][:K]
            results.update({'constraint_F2': [[None] for f in range(F)]})
        elif method is Solvers.SVD:
            results = minimize_chi_sq_globally_svd(kap_F=kap_Fd, kap_ref_F=kap_ref_Fd, phi_K=value_Kd, phi_bounds_K2=bounds_Kd2, R_FK=R_FdKd, W_FF=W_FdFd, **kwargs)
            results['Delta_phi_0_K'] = None
            results['phi_bounds_eff_K2'] = None
            results.update({'constraint_F2': [[None] for f in range(F)]})
            results.update({'func_val_0': np.nan})
        else:
            raise Exception('Solver id string not recognized.')

        Delta_f_opt = results['Delta_phi_opt_K'][-1]
        Delta_f_est = factor * Delta_f_opt
        results['Delta_phi_opt_K'] = results['Delta_phi_opt_K'][:K]
        if minimize_steerer_strength_flag:
            logging.debug('Minimizing currents.')
            corrected_results = minimize_steerer_strength(phi_K=phi_K+results['Delta_phi_opt_K'], phi_bounds_K2=phi_bounds_K2, R_FK=R_FK,
                                                        orbit_slack_F=orbit_slack_F, **kwargs)

            Delta_phi_opt_K = results['Delta_phi_opt_K'][:K] + corrected_results['Delta_phi_opt_K'][:K]
        else:
            Delta_phi_opt_K = results['Delta_phi_opt_K'][:K]
        Delta_phi_est_K = factor * Delta_phi_opt_K
        kap_opt_F = kap_F + np.dot(R_FK, Delta_phi_opt_K) + Delta_f_opt * Rd_F
        kap_est_F = kap_F + np.dot(R_FK, Delta_phi_est_K) + Delta_f_est * Rd_F

    elif path_length_mod is PathLength.MINIMIZE:
        logging.debug('Solving orbit-correction problem without dispersion.')

        if method is Solvers.QPCONE:
            results = minimize_chi_sq_within_bounds_qpcone(kap_F=kap_F, kap_ref_F=kap_ref_F, phi_K=phi_K, phi_bounds_K2=phi_bounds_K2, R_FK=R_FK, W_FF=W_FF, **kwargs)
        elif method is Solvers.L_BFGS_B:
            results = minimize_chi_sq_within_bounds_lbfgsb(kap_F=kap_F, kap_ref_F=kap_ref_F, phi_K=phi_K, phi_bounds_K2=phi_bounds_K2, R_FK=R_FK, W_FF=W_FF, **kwargs)
            results.update({'constraint_F2': [[None] for f in range(F)]})
        elif method is Solvers.SVD:
            results = minimize_chi_sq_globally_svd(kap_F=kap_F, kap_ref_F=kap_ref_F, phi_K=phi_K, phi_bounds_K2=phi_bounds_K2, R_FK=R_FK, W_FF=W_FF, **kwargs)
            results.update({'constraint_F2': [[None] for f in range(F)]})
            results.update({'func_val_0': np.nan})
        else:
            raise Exception('Solver id string not recognized.')

        if minimize_steerer_strength_flag:
            logging.debug('Minimizing steerer strength and path length.')
        else:
            logging.warning('Overriding "minimize_steerer_strength_flag=False": Path length minimization does not work without calling minimize_steerer_strength()!')
        corrected_results = minimize_steerer_strength(phi_K=phi_K+results['Delta_phi_opt_K'][:K], phi_bounds_K2=phi_bounds_K2, R_FK=R_FK,
                                                      f=f, f_bounds_2=f_bounds_2, Rd_F=Rd_F, orbit_slack_F=orbit_slack_F, **kwargs)

        Delta_f_opt = corrected_results['Delta_f_opt']
        Delta_f_est = factor * Delta_f_opt
        Delta_phi_opt_K = results['Delta_phi_opt_K'][:K] + corrected_results['Delta_phi_opt_K'][:K]
        Delta_phi_est_K = factor * Delta_phi_opt_K
        kap_opt_F = kap_F + np.dot(R_FK, Delta_phi_opt_K) + Delta_f_opt * Rd_F
        kap_est_F = kap_F + np.dot(R_FK, Delta_phi_est_K) + Delta_f_est * Rd_F

    else:
        raise Exception('Path length modifier not recognized.')

    logging.debug('    Results:')
    logging.debug('    --------')
    logging.debug('    before optimization:    chi = {}'.format(np.sqrt(results['func_val_0'])))
    logging.debug('    after optimization:     chi = {}'.format(np.sqrt(results['func_val'])))
    logging.debug('runtime  = {:.03}'.format(results['runtime']))
    logging.debug('\n   {:*^5} {} {:*^5}\n'.format('', 'OPTIMIZATION FINISHED', ''))

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                             collect results
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    results.update({'factor': factor})
    results.update({'kap_F': kap_F})
    results.update({'kap_ref_F': kap_ref_F})
    results.update({'kap_opt_F': kap_opt_F})
    results.update({'kap_est_F': kap_est_F})
    results.update({'phi_K': phi_K})
    results.update({'phi_opt_K': phi_K + Delta_phi_opt_K})
    results.update({'phi_est_K': phi_K + Delta_phi_est_K})
    results.update({'Delta_phi_opt_K': Delta_phi_opt_K})
    results.update({'Delta_phi_est_K': Delta_phi_est_K})
    results.update({'phi_bounds_K2': phi_bounds_K2})
    results.update({'R_FK': R_FK})
    results.update({'R_residuals': np.linalg.matrix_rank(R_FK, tol=0.03)})
    results.update({'W_FF': W_FF})
    results.update({'weighted_rms': cal_weighted_rms(kap_F, kap_ref_F, W_FF)})
    results.update({'weighted_rms_x': cal_weighted_rms(kap_F[:Jx], kap_ref_F[:Jx], W_FF[:Jx, :Jx])})
    results.update({'weighted_rms_y': cal_weighted_rms(kap_F[Jx:], kap_ref_F[Jx:], W_FF[Jx:, Jx:])})
    results.update({'weighted_rms_opt': cal_weighted_rms(kap_opt_F, kap_ref_F, W_FF)})
    results.update({'weighted_rms_opt_x': cal_weighted_rms(kap_opt_F[:Jx], kap_ref_F[:Jx], W_FF[:Jx, :Jx])})
    results.update({'weighted_rms_opt_y': cal_weighted_rms(kap_opt_F[Jx:], kap_ref_F[Jx:], W_FF[Jx:, Jx:])})
    results.update({'weighted_rms_est': cal_weighted_rms(kap_est_F, kap_ref_F, W_FF)})
    results.update({'weighted_rms_est_x': cal_weighted_rms(kap_est_F[:Jx], kap_ref_F[:Jx], W_FF[:Jx, :Jx])})
    results.update({'weighted_rms_est_y': cal_weighted_rms(kap_est_F[Jx:], kap_ref_F[Jx:], W_FF[Jx:, Jx:])})
    results.update({'rms': cal_rms(kap_F, kap_ref_F)})
    results.update({'rms_x': cal_rms(kap_F[:Jx], kap_ref_F[:Jx])})
    results.update({'rms_y': cal_rms(kap_F[Jx:], kap_ref_F[Jx:])})
    results.update({'rms_opt': cal_rms(kap_opt_F, kap_ref_F)})
    results.update({'rms_opt_x': cal_rms(kap_opt_F[:Jx], kap_ref_F[:Jx])})
    results.update({'rms_opt_y': cal_rms(kap_opt_F[Jx:], kap_ref_F[Jx:])})
    results.update({'rms_est': cal_rms(kap_est_F, kap_ref_F)})
    results.update({'rms_est_x': cal_rms(kap_est_F[:Jx], kap_ref_F[:Jx])})
    results.update({'rms_est_y': cal_rms(kap_est_F[Jx:], kap_ref_F[Jx:])})

    if path_length_mod is not PathLength.IGNORE:
        results.update({'f_opt': f + Delta_f_opt})
        results.update({'f_est': f + Delta_f_est})
        results.update({'Delta_f_opt': Delta_f_opt})
        results.update({'Delta_f_est': Delta_f_est})
        results.update({'Rd_F': Rd_F})
        results.update({'f_bounds_2': f_bounds_2})

    return results


def minimize_chi_sq_globally_svd(kap_F, kap_ref_F, R_FK, W_FF, rcond=0.0, *args, **kwargs):
    """
    Minimize orbit deviations.

    This function determines a vector of currents
    
        Delta_phi_opt_K
        
    which minimizes the objective functional
    
        ||W_FF @ (Delta_kap_F + R_FK @ Delta_phi_opt_K)||_2.
        
    The || ... ||_2 refers to the 'Euclidean' norm.
    

    Parameters
    ----------
    kap_F :  float (F)-ndarray
        orbit deviations
    kap_ref_F : float (F)-ndarray
        orbit reference
    R_FK : float (F, K)-ndarray
        orbit response matrix
    W_FF : float (F, F)-ndarray
        a diagonal FxF array of weights
    rcond : float
        Determines which singular values are cut off by np.linalg.pinv() when determining A_inv. All singular values smaller than rcond * <biggest singular value> are removed!

    Returns
    -------
    results : dict
        See below for details!
    """

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                                                preparations
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *

    # remove references
    kap_F = np.copy(kap_F)
    kap_ref_F = np.copy(kap_ref_F)
    R_FK = np.copy(R_FK)
    W_FF = np.copy(W_FF)

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                                optimization
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    t_begin = default_timer()
    A = np.dot(W_FF, R_FK)
    A_inv = np.linalg.pinv(A, rcond=rcond)
    Delta_phi_opt_K = - np.dot(A_inv, np.dot(W_FF, (kap_F - kap_ref_F)))
    t_end = default_timer()

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                                                clean up
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *

    results = {'Delta_phi_opt_K': Delta_phi_opt_K,
               'Delta_phi_0_K': None,
               'phi_bounds_eff_K2': None,
               'func_val': cal_weighted_norm(kap_F, kap_ref_F, W_FF)**2,
               'runtime': t_end - t_begin}

    return results


def minimize_chi_sq_within_bounds_lbfgsb(kap_F,
                                         kap_ref_F,
                                         phi_K,
                                         Delta_phi_0_K,
                                         phi_bounds_K2,
                                         R_FK,
                                         W_FF,
                                         maxfun=10e6,
                                         maxiter=10e6,
                                         callback=None,
                                         verb=3,
                                         *args, **kwargs):
    """
    This function determines a vector of currents
    
        Delta_phi_opt_K
        
    which minimizes the objective functional
    
        ||W_FF @ (Delta_kap_F + R_FK @ Delta_phi_opt_K)||_2
        
    within bounds
    
        phi_bounds_K2.
        
    The || ... ||_2 refers to the 'Euclidean' norm.


    Parameters
    ----------
    kap_F :  float (J)-ndarray
        orbit deviations
    kap_ref_F : float (J)-ndarray
        orbit reference
    phi_K : float(K)-ndarray
        vector of corrector currents
    Delta_phi_0_K : float (K)-ndarray or None
        start values for solver. If None or wrong dim, start values will be generated via svd.
    phi_bounds_K2 : float (K, 2)-tuples
        a tuple of K (phi_min, phi_max)-tuples
    R_FK : float (F, K)-ndarray
        orbit response matrix
    W_FF : float (F, F)-ndarray
        diagonal matrix of weights
    rcond : float
        singular values < rcond * max singular value are removed. default : rcond = 0.0
    sigmas_to_remove : int
        remove <sigmas_to_remove> singular values from R_FK
    maxfun : int
        maximum number of function evaluations of solver. default : maxfun = 10e6
    maxiter : int
        maximum number of iterations of solver. default : maxiter = 10e6
    callback : function
        function(Delta_phi_K) is called after each iteration of solver. default : callback = None

    Attributes
    ----------
    phi_bounds_eff_K2 : float (K, 2)-tuples
        a tuple of K (phi_eff_min, phi_eff_max)-tuples. "eff" refers to effective meaning that phi_K alread has been
        subtracted from each value.

    Returns
    -------
    results : dict
        See below for details!
    """

    logger = logging.getLogger('__main__')

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                                           validate array shapes
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *

    K = R_FK.shape[1]
    F = R_FK.shape[0]

    if not W_FF.shape == (F, F):
        raise Exception('W_FF has wrong shape.')
    if not kap_F.shape == (F,):
        raise Exception('kap_F has wrong shape.')
    if not kap_ref_F.shape == (F,):
        raise Exception('kap_ref_F has wrong shape.')
    if not phi_K.shape == (K,):
        raise Exception('phi_K has wrong shape.')
    if not phi_bounds_K2.shape == (K, 2):
        raise Exception('phi_bounds_eff_K2 has wrong shape.')
    if Delta_phi_0_K is None:
        logging.debug('Start values NOT supplied.')
    else:
        if not Delta_phi_0_K.shape == (K,):
            Delta_phi_0_K = None
            logging.debug('Start values not supplied.')

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                                                preparations
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *

    # remove references
    kap_F = np.copy(kap_F)
    kap_ref_F = np.copy(kap_ref_F)
    phi_K = np.copy(phi_K)
    phi_bounds_K2 = np.copy(phi_bounds_K2)
    R_FK = np.copy(R_FK)
    W_FF = np.copy(W_FF)

    # map parameters to f_min_l_bfgs_b options
    if callback == None:
        def callback(x):
            pass
    iprint = -1
    if logger.getEffectiveLevel() == logging.DEBUG:
        iprint = 0
    elif logger.getEffectiveLevel() < logging.DEBUG:
        iprint = 1

    # generate start values
    if Delta_phi_0_K is None:
        logging.debug('Generating start values via SVD.')
        Delta_phi_0_K = minimize_chi_sq_globally_svd(kap_F=kap_F,
                                                     kap_ref_F=kap_ref_F,
                                                     R_FK=R_FK,
                                                     W_FF=W_FF)['Delta_phi_opt_K']

    # prepare bounds
    phi_bounds_eff_K2 = list()
    for k in range(K):
        phi_min_eff = phi_bounds_K2[k][0] - phi_K[k]
        phi_max_eff = phi_bounds_K2[k][1] - phi_K[k]
        phi_bounds_eff_K2.append((phi_min_eff, phi_max_eff))
    phi_bounds_eff_K2 = tuple(phi_bounds_eff_K2)

    # correct start values by bounds
    for k in range(K):
        if Delta_phi_0_K[k] < phi_bounds_eff_K2[k][0]:
            Delta_phi_0_K[k] = phi_bounds_eff_K2[k][0]
        elif Delta_phi_0_K[k] > phi_bounds_eff_K2[k][1]:
            Delta_phi_0_K[k] = phi_bounds_eff_K2[k][1]

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                                optimization
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    # allocate ram to store intermediate results required for execution of obj_func_grad
    chi_F = np.zeros(F, float)
    grad_K = np.zeros(K, float)
    RW_KF = np.dot(R_FK.transpose(), W_FF)
    Delta_kap_F = kap_F - kap_ref_F

    def obj_func_fgrad(Delta_phi_K):
        """
        This method combines the calculation of the target functional and its gradient.

        Parameters
        ----------
        Delta_phi_K : float (K)-ndarray
            Vector of corrector currents

        Attributes
        ----------
        chi : float (J)-ndarray
            variable to store intermediate results. See log for details.
        RW : float (J, J)-ndarray
            intermediate results. See log for details.

        Returns
        -------
        f : float
            functional value
        grad : float (K)-ndarray
            gradient of functional with respect to Delta_phi_K. See log for details.
        """
        chi_F[:] = np.dot(W_FF, (Delta_kap_F + np.dot(R_FK, Delta_phi_K)))
        f = np.dot(chi_F, chi_F)
        np.einsum('ij,j->i', RW_KF, chi_F, out=grad_K)
        return f, 2*grad_K

    logging.debug('EXCECUTING fmin_l_bfgs_b.')

    # invoke the optimization algorithm
    t_begin = default_timer()
    Delta_phi_0_K = np.zeros(K, np.float64)
    Delta_phi_opt_K, func_val, dict_exit_status = fmin_l_bfgs_b(obj_func_fgrad,
                                                              Delta_phi_0_K,
                                                              fprime=None,
                                                              approx_grad=False,
                                                              bounds=phi_bounds_eff_K2,
                                                              maxfun=int(maxfun),
                                                              maxiter=int(maxiter),
                                                              callback=callback,
                                                              iprint=iprint, factr=10e-12)
    t_end = default_timer()

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                                                clean up
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *

    results = {'Delta_phi_opt_K': Delta_phi_opt_K,
               'Delta_phi_0_K': Delta_phi_0_K,
               'phi_bounds_eff_K2': phi_bounds_eff_K2,
               'warnflag': dict_exit_status['warnflag'],
               'max_func_calls': maxfun,
               'func_calls': dict_exit_status['funcalls'],
               'max_iter': maxiter,
               'iter': dict_exit_status['nit'],
               'func_val': func_val,
               'func_val_0': cal_weighted_norm(kap_F, kap_ref_F, W_FF)**2,
               'grad_val': dict_exit_status['grad'],
               'runtime': t_end - t_begin}

    if dict_exit_status['warnflag'] == 2:
        results.update({'warning': dict_exit_status['task']})

    # print final status message
    if dict_exit_status['warnflag'] == 2:
        logging.warning('L-BFGS-B did not converge! reason: {}'.format(dict_exit_status['task']))

    return results


def minimize_chi_sq_within_bounds_qpcone(kap_F,
                                         kap_ref_F,
                                         phi_K,
                                         phi_bounds_K2,
                                         R_FK,
                                         W_FF,
                                         constraint_F2=None,
                                         use_thikonov=False,
                                         maxiter=100,
                                         *args,
                                         **kwargs):
    """
    This function determines a vector of currents
    
        Delta_phi_opt_K
    
    which minimizes the objective functional
    
        ||W_FF @ (Delta_kap_F + R_FK @ Delta_phi_opt_K)||_2
    
    within bounds
    
        phi_bounds_K2.
    
    The || ... ||_2 refers to the 'Euclidean' norm.


    Parameters
    ----------
    kap_F :  float (J)-ndarray
        orbit deviations
    kap_ref_F : float (J)-ndarray
        orbit reference
    phi_K : float(K) - ndarray
        vector of corrector currents
    phi_bounds_K2 : float (K, 2)-tuples
        a tuple of K (phi_min, phi_max)-tuples
    R_FK : float (F, K)-ndarray
        orbit response matrix
    W_FF : float (F, F)-ndarray
        diagonal matrix of weights
    constraint_F2 : float (F, 2)-ndarray
        list of constraints 
            [constraints on BPM1, constraints on BPM 2, ..., constraints on BPM F] 
        for each BPM f where a constraint may either be
            [-np.inf, np.inf] for an unbounded orbit, 
            [float (fix point), float (same fix point)] for a fixed orbit or 
            [float (lower bound), float (upper bound)] for bounded orbit.
    maxiter : int
        maximum number of iterations of solver. default : maxiter = 10e6

    Attributes
    ----------
    phi_bounds_eff_K2 : float (K, 2)-tuples
        a tuple of K (phi_eff_min, phi_eff_max)-tuples. "eff" refers to effective meaning that phi_K alread has been
        subtracted from each value.

    Returns
    -------
    results : dict
        See below for details!
    """

    logger = logging.getLogger('__main__')

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                                           validate array shapes
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *

    K = R_FK.shape[1]
    F = R_FK.shape[0]

    if not W_FF.shape == (F, F):
        raise Exception('W_FF has wrong shape.')
    if not kap_F.shape == (F,):
        raise Exception('kap_F has wrong shape.')
    if not kap_ref_F.shape == (F,):
        raise Exception('kap_ref_F has wrong shape.')
    if not phi_K.shape == (K,):
        raise Exception('phi_K has wrong shape.')
    if not phi_bounds_K2.shape == (K, 2):
        raise Exception('phi_bounds_eff_K2 has wrong shape.')

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                                                preparations
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *

    # remove references
    kap_F = np.copy(kap_F)
    kap_ref_F = np.copy(kap_ref_F)
    phi_K = np.copy(phi_K)
    phi_bounds_K2 = np.copy(phi_bounds_K2)
    R_FK = np.copy(R_FK)
    W_FF = np.copy(W_FF)

    # map parameters to cvxopt.qpcone options
    options = {'maxiters': maxiter,
               'show_progress': False}
    if logger.getEffectiveLevel() <= logging.DEBUG:
        options['show_progress'] = True

    # prepare bounds
    if constraint_F2 is None:
        constraint_F2 = np.asarray([[-np.inf, np.inf] for f in range(F)])
    phi_bounds_eff_K2 = list()
    for k in range(K):
        phi_min_eff = phi_bounds_K2[k][0] - phi_K[k]
        phi_max_eff = phi_bounds_K2[k][1] - phi_K[k]
        phi_bounds_eff_K2.append((phi_min_eff, phi_max_eff))
    phi_bounds_eff_K2 = np.asarray(phi_bounds_eff_K2)


    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                                optimization
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    # create modified quantities for solver
    if use_thikonov:
        Gamma = np.eye(K, dtype=np.float64)
        M = np.concatenate((np.dot(W_FF, R_FK), Gamma), axis=0)
        P = cvxopt.matrix(np.dot(M.transpose(), M))
        logging.debug('USING THIKONOV REGULARIZATION.')
    else:
        P = cvxopt.matrix(np.dot(R_FK.transpose(), np.dot(W_FF.transpose(), np.dot(W_FF, R_FK))))
    q = cvxopt.matrix(np.dot((kap_F - kap_ref_F).transpose(), np.dot(W_FF, np.dot(W_FF, R_FK))))

    # corrector current constraints
    G_phi_min_KK = -np.eye(K, dtype=np.float64)
    G_phi_max_KK =  np.eye(K, dtype=np.float64)
    G_phi_2xKK  = np.concatenate((G_phi_min_KK, G_phi_max_KK,), axis=0)
    h_phi_min_K = abs(phi_bounds_eff_K2[:, 0])
    h_phi_max_K = phi_bounds_eff_K2[:, 1]
    h_phi_2xK = np.concatenate((h_phi_min_K, h_phi_max_K,), axis=0)

    # orbit constraints
    G_kap_MK = list()
    h_kap_M = list()
    A_kap_NK = list()
    b_kap_N = list()
    for f in range(F):
        constraints = constraint_F2[f]
        if constraints[0] == constraints[1]:
            A_kap_NK.append(R_FK[f])
            b_kap_N.append(constraints[0] - kap_F[f])
        elif not constraints[0] == constraints[1] and not constraints[0] == -np.inf:
            G_kap_MK.append(-R_FK[f])
            h_kap_M.append(kap_F[f] - constraints[0])
            G_kap_MK.append(R_FK[f])
            h_kap_M.append(constraints[1] - kap_F[f])

    # convert lists into numpy arrays
    G_kap_MK = np.asarray(G_kap_MK)
    h_kap_M = np.asarray(h_kap_M)
    A_kap_NK = np.asarray(A_kap_NK)
    b_kap_N = np.asarray(b_kap_N)

    # convert all arrays into cvxopt format
    M = G_kap_MK.shape[0]  # number of inequality constraints
    N = A_kap_NK.shape[0]  # number of equality constraints
    if M > 0:
        G = cvxopt.matrix(np.concatenate((G_phi_2xKK, G_kap_MK,), axis=0))
        h = cvxopt.matrix(np.concatenate((h_phi_2xK, h_kap_M,), axis=0))
    else:
        G = cvxopt.matrix(G_phi_2xKK)
        h = cvxopt.matrix(h_phi_2xK)
    if N > 0:
        A = cvxopt.matrix(A_kap_NK)
        b = cvxopt.matrix(b_kap_N)

    # dump input parameters
    d = {'kap_F': kap_F,
         'kap_ref_F': kap_ref_F,
         'phi_K': phi_K,
         'phi_bounds_K2': phi_bounds_K2,
         'R_FK': R_FK,
         'W_FF': W_FF,
         'constraint_F2': constraint_F2}
    #with open('step_param.pk', 'wb') as f:
    #    pk.dump(d, f)

    # invoke the optimization algorithm
    logging.debug('EXCECUTING cvxopt.qpcone().')
    t_begin = default_timer()
    if N > 0:
        # with equality and inequality constraints
        solver_results = cvxopt.solvers.coneqp(P=P, q=q, G=G, h=h, A=A, b=b, options=options)
    else:
        # with inequality constraints
        solver_results = cvxopt.solvers.coneqp(P=P, q=q, G=G, h=h, options=options)
    t_end = default_timer()

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                                                clean up
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *

    Delta_phi_opt_K = np.asarray(solver_results['x']).flatten()
    results = {'Delta_phi_opt_K': Delta_phi_opt_K,
               'Delta_phi_0_K': None,
               'phi_bounds_eff_K2': phi_bounds_eff_K2,
               'constraint_F2': constraint_F2,
               'warnflag': solver_results['status'],
               'max_iter': maxiter,
               'iter': solver_results['iterations'],
               'func_val': cal_weighted_norm(kap_F + np.dot(R_FK, Delta_phi_opt_K), kap_ref_F, W_FF)**2,
               'func_val_0': cal_weighted_norm(kap_F, kap_ref_F, W_FF)**2,
               'runtime': t_end - t_begin}

    # print final status message
    if solver_results['status'] == 'unknown':
        logging.debug('cvxopt.qpcone did not converge! reason: unknown')

    return results


def minimize_steerer_strength(phi_K, phi_bounds_K2, R_FK, f=None, f_bounds_2=None, Rd_F=None, orbit_slack_F=None, maxiter=100, *args, **kwargs):
    r"""
    Minimize steerer strength :math:`\vec\varphi`.

    Decrease absolute value of steerer strengths :math:`|\vec\varphi|` while maintaining the current orbit by ...

    1. distributing steerer strength among all linear dependent steerers equally

    and optionally by ...

    2. switching to a dispersive orbit if f and Rd_F are specified and
    3. shifting the orbit in a symmetric margin :math:`\pm\Delta\vec\kappa` around the current orbit :math:`\vec\kappa` if orbit_slack_F is specified.

    The optimal vector of steerer strengths :math:`\vec\varphi` is obtained by solving

    .. math::

       \min\limits_{\vec\varphi, \Delta f}\left(\vec{\varphi}^\mathrm{T}\vec{\varphi} + \epsilon(\Delta f)^2\right)

    subject to

    .. math::

       &\vec\varphi \le \vec\varphi^\mathrm{max}\\
       &\vec\varphi \ge \vec\varphi^\mathrm{min}\\
       &\textbf{R}\vec\varphi + \Delta f\vec{d} \le \textbf{R}\vec\varphi_0 + \Delta\vec\kappa\\
       &\textbf{R}\vec\varphi + \Delta f\vec{d} \ge \textbf{R}\vec\varphi_0 - \Delta\vec\kappa

    where :math:`\epsilon` is zero. Mutiplying with :math:`\epsilon` removes the effect of frequency changes on the value of the objective functional while keeping :math:`\Delta f` as optimization variable.

    .. warning:
       If K < F, correcting dispersion usually requires :math:`\Delta\kappa\neq\vec{0}`. This condition does not hold if :math:`D(s)\propto\textbf{R}\vec{b}` where :math:`D(s)` is the dispersion function. In other words: The dispersion function would have to be a linear combination of response matrix rows.


    Parameters
    ----------
    phi_K: float(K) - ndarray
        symbol :math:`\vec\varphi`: vector of steerer strengths
    phi_bounds_K2: float (K, 2)-tuples
        a vector of :math:`K` :math:`(\varphi_k^\mathrm{min}, \varphi_k^\mathrm{max})`-tuples where :math:`\varphi_k^\mathrm{min}` and :math:`\varphi_k^\mathrm{max}` are absolute constraints for the strength of the :math:`k`-th steerer. If constraints should be omitted for any steerer, use (-np.inf, np.inf) for its constraints.
    R_FK: float (F, K)-ndarray
        symbol :math:`\textbf{R}`: orbit-response matrix
    f: float
        symbol :math:`f`: rf frequency. Optional. Will be ignored if f_bounds_2 and Rd_F are not specified as well.
    f_bounds_2
        absolute constraints of the rf frequency :math:`f`. Optional. Will be ignored if f and Rd_F are not supplied as well.
    Rd_F: float (F,)-ndarray
        symbol :math:`\vec d`: frequency orbit-response vector. Optional. Will be ignored if f and f_bounds_2 are not specified as well.
    orbit_slack_F : float (F,)-ndarray
        symbol :math:`\Delta\vec\kappa`: allow the solver to move the optimized orbit within a margin :math:`\pm\Delta\vec\kappa` around the current orbit. Optional.
    maxiter : int
        iteration limit for conic solver. Defaults to maxiter = 10e6.

    Returns
    -------
    results : dict
        See dictionary definition for details!
    """

    logger = logging.getLogger('__main__')

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                               validate parameters
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    K = R_FK.shape[1]
    F = R_FK.shape[0]
    Kd = K + 1        # K steerer magnets + 1 frequency

    if not phi_bounds_K2.shape == (K, 2):
        raise Exception('phi_bounds_K2 has not the right shape.')
    if not phi_K.shape == (K,):
        raise Exception('phi_K has not the right shape.')
    if Rd_F is not None and f is not None:
        if not Rd_F.shape == (F,):
            raise Exception('Rd_F has not the right shape.')
        if f_bounds_2 is None:
            raise Exception('No rf frequency constraints supplied.')
        if K < F and (orbit_slack_F is None or not np.any(orbit_slack_F)):
            logging.warning('Attempting rf-frequency correction with K < F and orbit_slack_F == np.zeros((F,)) will be in vain.')

    if orbit_slack_F is None:
        orbit_slack_F = 1e-6*np.ones((F,), np.float64)

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                                preparations
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    # remove references
    phi_K = np.copy(phi_K)
    R_FK = np.copy(R_FK)
    phi_bounds_K2 = np.copy(phi_bounds_K2)
    f_bounds_2 = np.copy(f_bounds_2)
    Rd_F = np.copy(Rd_F)

    # prepare custom arrays
    kap_hold_F = np.dot(R_FK, phi_K)  # orbit to hold
    if Rd_F is not None and f is not None and f_bounds_2 is not None:
        f_bounds_rel_2 = f_bounds_2 - f  # relative bounds
        bounds_Kd2 = np.concatenate((phi_bounds_K2, f_bounds_rel_2.reshape(1, 2)), axis=0)  # absolute bounds for steerers and relative bounds for frequency
        response_FKd = np.concatenate((R_FK, Rd_F.reshape((F, 1))), axis=1)

    # map parameters to cvxopt.qpcone options
    options = {'maxiters': maxiter,
               'show_progress': False}
    if logger.getEffectiveLevel() <= logging.DEBUG:
        options['show_progress'] = True

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                                optimization
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    if False:
        from .. import rw
        dat = {'F': F,
               'K': K,
               'phi_K': phi_K,
               'phi_bounds_K2': phi_bounds_K2,
               'R_FK': R_FK,
               'f': f,
               'f_bounds_2': f_bounds_2,
               'Rd_F': Rd_F,
               'orbit_slack_F': orbit_slack_F}
        rw.save(dat, '/home/operator/personal/koetter/debug_dat.pk', 'pk')

    # corrector current constraints
    if Rd_F is None or f is None or f_bounds_2 is None:
        logging.debug('minimize_steerer_strength: ignoring dispersion')

        G_phi_min_KK = -np.eye(K, dtype=np.float64)
        G_phi_max_KK =  np.eye(K, dtype=np.float64)
        G_kap_min_FK = -R_FK
        G_kap_max_FK =  R_FK
        G = np.concatenate((G_phi_min_KK, G_phi_max_KK, G_kap_min_FK, G_kap_max_FK), axis=0)
        h_phi_min_K = -phi_bounds_K2[:, 0]
        h_phi_max_K =  phi_bounds_K2[:, 1]
        h_kap_min_F = -(kap_hold_F - orbit_slack_F)
        h_kap_max_F =   kap_hold_F + orbit_slack_F
        h = np.concatenate((h_phi_min_K, h_phi_max_K, h_kap_min_F, h_kap_max_F), axis=0)

        # convert all arrays into cvxopt-matrix format
        P = cvxopt.matrix(np.eye(K, dtype=np.float64))
        q = cvxopt.matrix(np.zeros(K, dtype=np.float64))
        G = cvxopt.matrix(G)
        h = cvxopt.matrix(h)

    else:
        G_phi_and_f_min_KdKd = -np.eye(Kd, dtype=np.float64)
        G_phi_and_f_max_KdKd =  np.eye(Kd, dtype=np.float64)
        G_kap_min_FKd = -response_FKd
        G_kap_max_FKd =  response_FKd
        G = np.concatenate((G_phi_and_f_min_KdKd, G_phi_and_f_max_KdKd, G_kap_min_FKd, G_kap_max_FKd), axis=0)
        h_phi_and_f_min_Kd = -bounds_Kd2[:, 0]
        h_phi_and_f_max_Kd =  bounds_Kd2[:, 1]
        h_kap_min_F = -(kap_hold_F - orbit_slack_F)
        h_kap_max_F =   kap_hold_F + orbit_slack_F
        h = np.concatenate((h_phi_and_f_min_Kd, h_phi_and_f_max_Kd, h_kap_min_F, h_kap_max_F), axis=0)

        # convert all arrays into cvxopt-matrix format
        P = cvxopt.matrix(np.eye(Kd, dtype=np.float64))
        P[-1, -1] = 0.0  # remove frequency-effect on cost function
        q = cvxopt.matrix(np.zeros(Kd, dtype=np.float64))
        G = cvxopt.matrix(G)
        h = cvxopt.matrix(h)

    # invoke the optimization algorithm
    logging.debug('minimize_currents_qpcone: EXCECUTING cvxopt.qpcone().')
    t_begin = default_timer()
    solver_results = cvxopt.solvers.coneqp(P=P, q=q, G=G, h=h, options=options)
    t_end = default_timer()

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                                                clean up
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    x = np.asarray(solver_results['x']).flatten()
    phi_opt_K = x[:K]
    results = {'Delta_phi_opt_K': phi_opt_K - phi_K,             # optimized relative steerer strengths
               'phi_opt_K': phi_opt_K,                           # optimized absolute steerer strengths
               'warnflag': solver_results['status'],             # convergence flag
               'iter': solver_results['iterations'],             # number of iterations of conic solver
               'func_val': 0.5 * np.linalg.norm(phi_opt_K)**2,   # cost-function value at exit point
               'func_val_0': 0.5 * np.linalg.norm(phi_opt_K)**2, # cost-function value at entry point
               'runtime': t_end - t_begin}                       # runtime of conic solver
    if Rd_F is not None and f is not None and f_bounds_2 is not None:
        Delta_f_opt = x[-1]
        results.update({'Delta_f_opt': Delta_f_opt,              # optimized relative rf frequency
                        'f_opt': f + Delta_f_opt})               # optimized absolute rf frequency

    # print final status message
    if solver_results['status'] == 'unknown':
        logging.debug('cvxopt.qpcone did not converge! reason: unknown')
    else:
        logging.debug('    Results:')
        logging.debug('    --------')
        logging.debug('    Before optimization:    chi = {}'.format(np.sqrt(results['func_val_0'])))
        logging.debug('    After optimization:     chi = {}'.format(np.sqrt(results['func_val'])))
        logging.debug('    Runtime  = {:.03}'.format(results['runtime']))

    return results
