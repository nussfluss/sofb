import time
import numpy as np
import logging


def remove_singular_values_from_R_MN(R_MN, sigmas_to_remove):
    """
    Removes the smallest
        sigmas_to_remove
    singular values from the singular value spectrum of a matrix
        R_MN.

    Parameters:
    -----------
    R_MN: (M, N)-ndarray
        matrix
    sigmas_to_remove: int
        number of singular values to remove

    Returns:
    --------
    R_clean_MN: (M, N)-ndarray
        cleaned matrix
    sigma_MorN: float (M or N)-ndarray
        singular value spectrum of R_MN
    """
    U, sigma_MorN, V = np.linalg.svd(R_MN, full_matrices=False)
    for i in range(sigma_MorN.shape[0]):
        if i >= sigma_MorN.shape[0] - sigmas_to_remove:
            sigma_MorN[i] = 0
    R_clean_MN = np.dot(U, np.dot(np.diag(sigma_MorN), V))
    return R_clean_MN, sigma_MorN


def remove_singular_values_from_R_FK(R_FK, sigmas_to_remove_h, sigmas_to_remove_v, Kh, J, decoupled_flag=False):
    """
    Convenience function that applies
        remove_singular_values_from_R_MN()
    in both planes.

    Removes the smallest
        sigmas_to_remove_h
    horizontal singular values and the smallest
        sigmas_to_remove_v
    vertical singular values from the singular value spectrum of
        R_FK.

    Additionally removes inter-plane coupling if
        decoupled_flag
    is True.

    Parameters:
    -----------
    R_FK: (F, K)-ndarray
        matrix. Has to be block diagonal.
        R_FK = (R_xx  R_xz)
               (R_zx  R_ZZ)
    sigmas_to_remove_h: int
        number of horizontal singular values to remove
    sigmas_to_remove_v: int
        number of vertical singular values to remove
    Kh: int
        number of horizontal correctors
    J: int
        number of BPMs
    decoupled_flag: bool
        remove inter-plane coupling elements from R_FK

    Returns:
    --------
    R_clean_MN: (M, N)-ndarray
        cleaned matrix
    sigma_MorN: float (M or N)-ndarray
        singular value spectrum of R_MN
    """

    R_FK_clean = np.zeros(R_FK.shape, np.float64)
    if decoupled_flag:
        R_FK_clean[:J, :Kh], sigma_Kh = remove_singular_values_from_R_MN(R_FK[:J, :Kh], sigmas_to_remove_h)
        R_FK_clean[J:, Kh:], sigma_Kv = remove_singular_values_from_R_MN(R_FK[J:, Kh:], sigmas_to_remove_v)
        logging.debug('remove_singular_values_from_R_FK: removed inter-plane-coupling elements from R_FK')
    else:
        R_FK_clean[:J], sigma_Kh = remove_singular_values_from_R_MN(R_FK[:J], sigmas_to_remove_h)
        R_FK_clean[J:], sigma_Kv = remove_singular_values_from_R_MN(R_FK[J:], sigmas_to_remove_v)

    logging.debug('remove_singular_values_from_R_FK: removed {} horizontal and {} vertical singular values from R_FK'.format(sigmas_to_remove_h, sigmas_to_remove_v))

    return R_FK_clean, sigma_Kh, sigma_Kv


def remove_inter_plane_coupling_from_R_FK(R_FK, Kh=30, J=54, verb=3):
    """
    Removes inter-plane coupling from
        R_FK.

    Parameters:
    -----------
    R_FK: (F, K)-ndarray
        orbit-response matrix. Has to be block diagonal.
        R_FK = (R_xx  R_xz)
               (R_zx  R_ZZ)
    Kh: int
        number of horizontal correctors
    J: int
        number of BPMs
    decoupled_flag: bool
        remove inter-plane coupling elements from R_FK

    Returns:
    --------
    R_clean_MN: (M, N)-ndarray
        cleaned matrix
    """
    R_clean_FK = np.zeros(R_FK.shape, np.float64)
    R_clean_FK[:J, :Kh] = R_FK[:J, :Kh]
    R_clean_FK[J:, Kh:] = R_FK[J:, Kh:]
    logging.debug('remove_inter_plane_coupling_from_R_FK: removed inter-plane-coupling elements from R_FK')
    return R_clean_FK


def reduce_R_FK_to_R_FeffKeff(R_FK, map_Feff, map_Keff):
    Feff = len(map_Feff)
    Keff = len(map_Keff)
    R_FeffKeff = np.zeros((Feff, Keff), np.float64)
    for feff in range(Feff):
        for keff in range(Keff):
            R_FeffKeff[feff, keff] = R_FK[map_Feff[feff], map_Keff[keff]]
    return R_FeffKeff


def reduce_R_FK_to_R_eff_FK(R_FK, map_Feff, map_Keff):
    F = R_FK.shape[0]
    K = R_FK.shape[1]
    Feff = len(map_Feff)
    Keff = len(map_Keff)
    R_eff_FK = np.zeros((F, K), np.float64)
    for feff in range(Feff):
        for keff in range(Keff):
            R_eff_FK[map_Feff[feff], map_Keff[keff]] = R_FK[map_Feff[feff], map_Keff[keff]]
    return R_eff_FK


def reduce_W_FF_to_W_FeffFeff(W_FF, map_Feff):
    Feff = len(map_Feff)
    W_FeffFeff = np.zeros((Feff, Feff), np.float64)
    for feff in range(Feff):
        W_FeffFeff[feff, feff] = W_FF[map_Feff[feff], map_Feff[feff]]
    return W_FeffFeff


def make_empty_dict_R_raw_k(F, buf_len):

    """
    Creates a dictionary
        dict_R_raw_k
    to store orbit response raw data for a single corrector k for an arbitrary number of disturbances Delta_I.

    Parameters:
    -----------
    F : int
        combined number of BPMs of both planes
    Delta_Is : list
        correction summands for corrector currents used to excite closed orbit perturbations
    buf_len : int
        length of buffer

    Returns:
    --------
    dict_R_raw_k : dict
        Dictionary containing
         {
            'pos' : {'Delta_I': float,                            # excitation current
                     'I_quad': float,                             # quadrupole current
                     'I_should': float,                           # <set> corrector current
                     'I_is_K': float (buf_len)-ndarray            # <returned> corrector current
                     'beam': float (buf_len)-ndarray              # buffer of Delta beam currents
                     'kap_F':  float (buf_len, F)-ndarray         # buffer of orbit deviations
                     'q_M':    float (buf_len, 2)-ndarray},       # buffer of tunes
            'neg' : {'Delta_I': float,                            # excitation current
                     'I_quad': float,                             # quadrupole current
                     'I_should': float,                           # <set> corrector current
                     'I_is_K': float (buf_len)-ndarray            # <returned> corrector current
                     'beam': float (buf_len)-ndarray              # buffer of Delta beam currents
                     'kap_F':  float (buf_len, F)-ndarray         # buffer of orbit deviations
                     'q_M':    float (buf_len, 2)-ndarray},       # buffer of tunes
            'current_src': '',                                    # quadrupole current src
            't_intv': float,                                      # time interval in between consecutive measurements of buffer entries in ms
            't_end': float,                                       # time interval in between consecutive measurements of excitation states
            'idstr': str,                                         # idstr of corrector
            'use': int,                                           # status of corrector while measuring
            'ver': float                                          # version of this dictionary
        }
    """

    dict_R_raw_k = {'pos' : {'Delta_I': 0.0,                                      # excitation current
                             'I_quad': 0.0,                                       # quadrupole current
                             'I_should': 0.0,                                     # <set> corrector current
                             'I_is_K': np.zeros(buf_len, dtype=np.float64),       # <returned> corrector current
                             'beam': np.zeros(buf_len, dtype=np.float64),         # buffer of Delta beam currents
                             'kap_F': np.zeros((buf_len, F), dtype=np.float64),   # buffer of orbit deviations
                             'q_M': np.zeros((buf_len, 2), dtype=np.float64)},    # buffer of tunes
                    'neg' : {'Delta_I': 0.0,                                      # excitation current
                             'I_quad': 0.0,                                       # quadrupole current
                             'I_should': 0.0,                                     # <set> corrector current
                             'I_is_K': np.zeros(buf_len, dtype=np.float64),       # <returned> corrector current
                             'beam': np.zeros(buf_len, dtype=np.float64),         # buffer of Delta beam currents
                             'kap_F': np.zeros((buf_len, F), dtype=np.float64),   # buffer of orbit deviations
                             'q_M': np.zeros((buf_len, 2), dtype=np.float64)},    # buffer of tunes
                    'current_src': '',                                            # quadrupole current src
                    't_intv': 0.0,                                                # t_intv
                    't_end': 0.0,                                                 # t_end
                    'idstr': '',                                                  # idstr
                    'use': 0,                                                     # use
                    'ver': 3.0                                                    # version
                    }

    return dict_R_raw_k


def meas_dict_R_raw(hw, Delta_I, ks=None, t_intv=0.2, t_end=2, buf_len=60, dict_R_raw=None, verb=3):
    """
    Collects buffers for all correctors
        ks.

    Parameters:
    -----------
    hw : hw.HW instance
        hardware interface
    Delta_I : float
        orbit disturbance to apply in Ampere
    ks : list, default = None
        list of columns corresponding to correctors in R to collect buffers for. If None, buffer will be collected
        for all correctors listed in hw.corrs.
    t_intv : float, default = 0.2
        sleep time in seconds in between consecutive measurements during buffer collection
    t_end : float, default = 2
        sleep time in seconds after buffer collection for a single corrector k is finished
    buf_len : int, default = 60
        sets buffer length for each buffer in collection
    dict_R_raw :  dict, default = None
        If None, dict_R_raw will be created on the fly.
    verb : int, default = 3
        verbosity
            1 := important messages only
            3 := printout details

    Returns:
    --------
    dict_R_raw_k :  dict
        Dictionary containing collected buffers and hardware information for a single corrector k.
             {
                'pos' : {'Delta_I': float,                            # excitation current
                         'I_should': float,                           # <set> corrector current
                         'I_is_K': float (buf_len)-ndarray            # <returned> corrector current
                         'beam': float (buf_len)-ndarray              # buffer of Delta beam currents
                         'kap_F':  float (buf_len, F)-ndarray         # buffer of orbit deviations
                         'q_M':    float (buf_len, 2)-ndarray},       # buffer of tunes
                'neg' : {'Delta_I': float,                            # excitation current
                         'I_should': float,                           # <set> corrector current
                         'I_is_K': float (buf_len)-ndarray            # <returned> corrector current
                         'beam': float (buf_len)-ndarray              # buffer of Delta beam currents
                         'kap_F':  float (buf_len, F)-ndarray         # buffer of orbit deviations
                         'q_M':    float (buf_len, 2)-ndarray},       # buffer of tunes
                't_intv': float,                                      # time interval in between consecutive measurements of buffer entries in ms
                't_end': float,                                       # time interval in between consecutive measurements of excitation states
                'idstr': str,                                         # idstr of corrector
                'use': int,                                           # status of corrector while measuring
                'ver': float                                          # version of this dictionary
             }
    """
    if ks is None: ks = np.arange(hw.corrs.K)
    if verb >= 1:
        print('R.meas_dict_R_raw: Measuring buffers ... ')
        print('                       K       = {}'.format(len(ks)))
        print('                       Delta_I = {} A'.format(Delta_I))
        print('                       t_intv  = {} s'.format(t_intv))
        print('                       t_end   = {} s'.format(t_end))
        print('                       buf_len = {} A'.format(buf_len))
        print('                   The estimated runtime will be {} s'.format(len(ks) * (2 * buf_len * t_intv + t_end)))

    if dict_R_raw is None:
        dict_R_raw = dict()

    for k in ks:
        dict_R_raw_k = meas_dict_R_raw_k(hw=hw, k=k, Delta_I=Delta_I, t_intv=t_intv, t_end=t_end, buf_len=buf_len, verb=verb)
        if k in dict_R_raw:
            dict_R_raw[k] = dict_R_raw_k
        else:
            dict_R_raw.update({k: dict_R_raw_k})

    return dict_R_raw


def meas_dict_R_raw_k(hw, k, Delta_I, t_intv=0.2, t_end=2, buf_len=60, verb=3):

    """
    Starts buffer collections for a single corrector. All collected data is returned as
        dict_R_raw_k.
    See hw.dict_R_raw.py for Details of this data type.

    Parameters:
    -----------
    hw : hw.HW instance
        hardware interface
    k : int
        column corresponding to corrector in R
    Delta_I : float
        applied orbit disturbances in Ampere
    t_intv : float
        sleep time in seconds in between each consecutive measurement during buffer collection
    t_end : float
        sleep time in seconds after buffer collection for a single corrector k is finished
    buf_len : int
        buffer length
    verb : int
        verbosity
            1 := important messages only
            3 := printout details

    Returns:
    --------
    dict_R_raw_k :  dict
        Dictionary containing collected buffers and hardware information for a single corrector k.
    """

    dict_R_raw_k = dict_R_raw.make_empty_dict_R_raw_k(hw.bpms.F, buf_len)

    dict_R_raw_k['t_intv'] = t_intv
    dict_R_raw_k['idstr'] = hw.corrs[k].idstr
    dict_R_raw_k['use'] = hw.corrs[k].status
    dict_R_raw_k['current_src'] = hw.corrs[k].current_src

    if hw.corrs[k].status == 1:

        def collect_buffers(identifier, Delta_I, I_should):
            """
            Collects buffers for a single excitation current Delta_I.

            The sleep time in between consecutive measurements is adjusted for fast correctors because there is an
            additional sleep time when accessing the property CorFast.I which executes CorFast.dialog().

            Parameters:
            -----------
            Delta_I : float
                applied excitation current
            """
            dict_R_raw_k[identifier]['Delta_I_K']   = Delta_I
            dict_R_raw_k[identifier]['I_should']    = I_should
            dict_R_raw_k[identifier]['I_quad']      = hw.corrs[k].I_quad
            for n in range(buf_len):
                dict_R_raw_k[identifier]['beam'][n]     = hw.beam.get()
                dict_R_raw_k[identifier]['kap_F'][n, :] = hw.bpms.kap_F(t=0)
                dict_R_raw_k[identifier]['q_M'][n, 0]   = hw.qs[0].get()
                dict_R_raw_k[identifier]['q_M'][n, 1]   = hw.qs[1].get()
                dict_R_raw_k[identifier]['I_is_K'][n]   = hw.corrs[k].I

                time.sleep(t_intv)
                #if t_intv == 0.0:
                #    print('hw.bpm.BPM: Careful, Im not sleeping during buffer collection! t_intv < 0.1')

        if verb >= 1: print('\n{}:'.format(hw.corrs.ar_cor[k]['idstr']))
        if verb >= 3: print('{:-^25}'.format(''))

        #                                                                                            collect buffers
        # ----------------------------------------------------------------------------------------------------------

        #                                                                                                    query I
        I = hw.corrs[k].I
        if verb >= 3: print('    corrector current is I = {:.03} A'.format(0.0, I))

        #                                                                                            set I + Delta_I
        hw.corrs[k].I = I + Delta_I
        collect_buffers(identifier='pos', Delta_I=Delta_I,  I_should=I+Delta_I)
        if verb >= 3: print('    exciting beam for by Delta_I =  {:.03} A and collecting buffers'.format(Delta_I))

        #                                                                                            set I - Delta_I
        hw.corrs[k].I = I - Delta_I
        collect_buffers(identifier='neg', Delta_I=-Delta_I,  I_should=I-Delta_I)
        if verb >= 3: print('    exciting beam for by Delta_I = {:.03} A and collecting buffers'.format(-Delta_I))

        #                                                                                                  restore I
        hw.corrs[k].I = I
        if verb >= 3: print('    restoring original corrector current I = {:.03}'.format(I))
        if verb >= 3: print('    sleeping {} s'.format(t_end))
        time.sleep(t_end)

        # ----------------------------------------------------------------------------------------------------------
    else:
        if verb >= 1: print('ignoring {}   status = {}'.format(hw.corrs[k].idstr, hw.corrs[k].status))

    return dict_R_raw_k


def convert_dict_R_raw_to_R_FK(dict_R_raw, K=56, F=108, t_orb=1.5, norm='rad', verb=1):
    """
    Converts a R_raw dictionary into orbit a response matrix R_FK.

    Parameters:
    -----------
    dict_R_raw :  dict
    K : int, default = 56
    F : int, default = 108
    t_orb : float, default = 1.5
        time in seconds which is excluded from beginning of collected buffers for creation of R_FK.
    norm : str
        either 'rad' or 'A'
    verb : int
        verbosity

    Returns:
    --------
    R_FK : float (F, K)-ndarray
        Orbit response matrix calculated from buffers of disturbed orbits 'pos' and 'neg'
    sig_R_FK : float (F, K)-ndarray
    mu_M : float (M)-ndarray
        average tune in rad
    sig_mu_M : float (M)-ndarray
    """
    R_FK = np.zeros((F, K), np.float64)
    sig_R_FK = np.zeros((F, K), np.float64)
    q_NM = np.zeros((1, 2), np.float64)

    for k in range(K):
        use    = dict_R_raw[k]['use']
        t_intv = dict_R_raw[k]['t_intv']
        idstr  = dict_R_raw[k]['idstr']
        q_NM   = np.concatenate((q_NM, dict_R_raw[k]['pos']['q_M']), axis=0)
        q_NM   = np.concatenate((q_NM, dict_R_raw[k]['neg']['q_M']), axis=0)

        if use == 1:
            def calculate_disturbance_per_current(buf_kap_pos,
                                                  buf_kap_neg,
                                                  buf_I_pos,
                                                  buf_I_neg,
                                                  I_quad_pos,
                                                  I_quad_neg):
                """
                Determines the orbit response matrix R_Fk and its standard deviations sig_R_Fk for
                a single corrector k. If norm == 'rad', the matrix elements are given in rad or Ampere otherwise.

                See log for derivation of sig_R_Fk!

                Parameters:
                -----------
                buf_kap_pos : float (buf_len, F)-ndarray
                    buffer of orbit deviations for positive excitation current
                buf_kap_neg : float (buf_len, F)-ndarray
                    buffer of orbit deviations for negative excitation current
                buf_I_pos : float (buf_len)-ndarray
                    buffer of corrector currents I while measuring buf_kap_pos
                buf_I_neg : float (buf_len)-ndarray
                    buffer of corrector currents I while measuring buf_kap_neg

                Attributes:
                -----------
                n_min : int
                    number of buffer entry n_min where n_min*t_intv == t_orb. Buffer entries n<n_min are ignored.

                Returns:
                --------
                R_Fk : float (F)-ndarray
                    normalized response matrix elements for all F BPMs corresponding to input orbit disturbances at
                    corrector k
                sig_R_Fk : float (F)-ndarray
                    std deviations of r
                """

                if t_intv == 0.0:
                    n_min = 0  # division by t_intv = 0 always returns nan
                else:
                    n_min = int(t_orb/t_intv) + 1

                avg_kap_pos_Fk = np.average(buf_kap_pos[n_min:], axis=0)
                avg_kap_neg_Fk = np.average(buf_kap_neg[n_min:], axis=0)
                avg_I_pos_Fk = np.average(buf_I_pos[n_min:], axis=0)
                avg_I_neg_Fk = np.average(buf_I_neg[n_min:], axis=0)

                if norm == 'rad':
                    # transforms currents to deflection angles
                    avg_I_pos_Fk = i2k2py.I2phi(idstr, I_corr=avg_I_pos_Fk, I_quad=I_quad_pos, beam_energy=1.5, verb=verb)
                    avg_I_neg_Fk = i2k2py.I2phi(idstr, I_corr=avg_I_neg_Fk, I_quad=I_quad_neg, beam_energy=1.5, verb=verb)

                R_Fk = (avg_kap_pos_Fk - avg_kap_neg_Fk) / (avg_I_pos_Fk - avg_I_neg_Fk)

                # determine standard deviation of each variable
                sig_kap_pos_Fk = np.var(buf_kap_pos[n_min:], ddof=1, axis=0)
                sig_kap_neg_Fk = np.var(buf_kap_neg[n_min:], ddof=1, axis=0)
                sig_I_pos_Fk = np.var(buf_I_pos[n_min:], ddof=1, axis=0)
                sig_I_neg_Fk = np.var(buf_I_neg[n_min:], ddof=1, axis=0)

                # propagate error - see log for derivation!
                sig_R_Fk = np.sqrt((1 - avg_kap_neg_Fk)**2 / (avg_I_pos_Fk - avg_I_neg_Fk)**2 * sig_kap_pos_Fk**2                                  +
                                   (1 - avg_kap_pos_Fk)**2 / (avg_I_pos_Fk - avg_I_neg_Fk)**2 * sig_kap_neg_Fk**2                                  +
                                   (avg_kap_pos_Fk - avg_kap_neg_Fk)**2 / (avg_I_pos_Fk - avg_I_neg_Fk)**2 * (sig_I_pos_Fk + sig_I_neg_Fk)**2)

                return R_Fk, sig_R_Fk

            R_FK[:, k], sig_R_FK[:, k] = calculate_disturbance_per_current(buf_kap_pos=dict_R_raw[k]['pos']['kap_F'],
                                                                           buf_kap_neg=dict_R_raw[k]['neg']['kap_F'],
                                                                           buf_I_pos=dict_R_raw[k]['pos']['I_is_K'],
                                                                           buf_I_neg=dict_R_raw[k]['neg']['I_is_K'],
                                                                           I_quad_pos=dict_R_raw[k]['pos']['I_quad'],
                                                                           I_quad_neg=dict_R_raw[k]['neg']['I_quad'])

    # determine tune as average
    q_NM = np.delete(q_NM, 0, axis=0)
    mu_NM = 2 * np.pi * q_NM
    mu_M = np.average(mu_NM, axis=0)
    sig_mu_M = np.std(mu_NM, axis=0, ddof=1)

    if verb >= 1:
        print('\n')
        print('R.convert_dict_R_raw_to_R_FK: dict_R_raw was converted to R_FK.')
        print('                                  norm  = {}'.format(norm))
        print('                                  sig_R = {:.03}'.format(np.sum(sig_R_FK)))
        print('                                  mu_x  = {:.03} +- {:.03}'.format(mu_M[0], sig_mu_M[0]))
        print('                                  mu_y  = {:.03} +- {:.03}'.format(mu_M[1], sig_mu_M[1]))

    return R_FK, sig_R_FK, mu_M, sig_mu_M, norm


def determine_most_effective_corrector(kap_F, kap_ref_F, R_FK,  W_FF=None):
    """
    Finds the single most effective corrector k with column R_Fk in R_FK which is linear dependent as in
        W_FF @ (kap_F - kap_ref_F)  ||  W_FF @ R_Fk.
    If such a corrector does not exist the most linear dependet corrector will be returned.

    Parameters:
    -----------
    kap_F : float (F)-ndarray
        orbit
    kap_ref_F: float (F)-ndarray
        orbit reference
    R_FK: float (F, K)-ndarray
        orbit-response matrix
    W_FF: float (F, F)-ndarray
        weights

    Returns:
    --------
    k_micado : int
        the single most effective corrector
    """
    F = R_FK.shape[0]
    K = R_FK.shape[1]

    if W_FF is None:
        W_FF = np.eye(F, dtype=np.float64)
        logging.debug('determine_most_effective_corrector: omitting weights')
    correlation_K = np.zeros(K, np.float64)
    WDeltakap_F = np.dot(W_FF, kap_F - kap_ref_F)
    WR_FK = np.dot(W_FF, R_FK)
    for k in range(K):
        ip = np.abs(np.dot(WR_FK[:, k], WDeltakap_F))
        ni = np.linalg.norm(WR_FK[:, k])
        nj = np.linalg.norm(WDeltakap_F)
        """ Cauchy Schwarz inequality: |<u,v>| <= ||u|| * ||v||, Ansatz by Dennis Rhode"""
        correlation_K[k] = ni * nj / ip
    logging.debug('determine_most_effective_corrector: min = {} @ k = {}'.format(np.min(correlation_K), np.argmin(correlation_K)))
    return np.argmin(correlation_K)
