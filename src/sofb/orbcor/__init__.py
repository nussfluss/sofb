import time
import matplotlib.pyplot as plt
import numpy as np
import logging

from .chi import *
from . import R


class CorrectionFailedError(Exception):
    pass


class EmptySteererSetError(CorrectionFailedError):
    pass


def determine_correction_step_uncoupled(kap_Jx,
                                        kap_Jy,
                                        kap_ref_Jx,
                                        kap_ref_Jy,
                                        kap_bounds_Jx2,
                                        kap_bounds_Jy2,
                                        I_Kh,
                                        I_Kv,
                                        I_bounds_Kh2,
                                        I_bounds_Kv2,
                                        R_JxKh,
                                        R_JyKv,
                                        W_JxJx,
                                        W_JyJy,
                                        factor_x,
                                        factor_y,
                                        I_Kh_2_phi_Kh,
                                        I_Kv_2_phi_Kv,
                                        phi_Kh_2_I_Kh,
                                        phi_Kv_2_I_Kv,
                                        f,
                                        Rd_Jx=None,
                                        f_bounds_2=None,
                                        Delta_I_cut=0.0,
                                        Delta_I_max=10.0,
                                        Delta_phi_cut=1e-6,
                                        orbit_slack=0.0,
                                        orbit_slack_factor=0.0,
                                        optimization_method=Solvers.QPCONE,
                                        path_length_mod=PathLength.IGNORE,
                                        minimize_steerer_strength_flag=True,
                                        micado_x_flag=False,
                                        micado_y_flag=False,
                                        plot_flag=False,
                                        auto_set_flag=True):
    """
    Determine a set of optimal steerer currents. Each plane is treated separately.

    Parameters
    ----------
    kap_Jx: float np.ndarray
    kap_Jy: float np.ndarray
    kap_ref_Jx: float np.ndarray
    kap_ref_Jy: float np.ndarray
    kap_bounds_Jx2: float np.ndarray
    kap_bounds_Jy2: float np.ndarray
    I_Kh: float np.ndarray
    I_Kv: float np.ndarray
    I_bounds_Kh2: float np.ndarray
    I_bounds_Kv2: float np.ndarray
    R_JxKh: float np.ndarray
    R_JyKv: float np.ndarray
    W_JxJx: float np.ndarray
    W_JyJy: float np.ndarray
    factor_x: float
        Has to be in between zero and one.
    factor_y: float
        Has to be in between zero and one.
    I_Kh_2_phi_Kh: float np.ndarray
    I_Kv_2_phi_Kv: float np.ndarray
    phi_Kh_2_I_Kh: float np.ndarray
    phi_Kv_2_I_Kv: float np.ndarray
    f: float
    Rd_Jx: float np.ndarray, optional
        frequency orbit response. There is no Rd_Jy becuase it should be close to zero anyways.
    f_bounds_2: float (2,)-np.ndarray, optional
        frequency constraints. If Rd_F is supplied, these constraints have to be applied, as well.
    Delta_I_cut: float, optional
    Delta_I_max: float, optional
    Delta_phi_cut: float, optional
    orbit_slack: float, optional
    orbit_slack_factor: float, optional
    optimization_method: int, optional
        One of the methods in Solvers.
    path_length_mod: Enum
        either IGNORE, LOCK or MINIMIZE path length
    minimize_steerer_strength_flag: bool, optional
    micado_x_flag: bool, optional
    micado_y_flag: bool, optional
    plot_flag: bool, optional
    auto_set_flag: bool, optional

    Return
    ------
    results: dict
    """

    # logging.getLogger().setLevel(logging.DEBUG)

    Jx = R_JxKh.shape[0]
    Kh = R_JxKh.shape[1]
    Jy = R_JyKv.shape[0]
    Kv = R_JyKv.shape[1]

    horizontal_correction_failed = False
    orbit_slack_Jx = np.full((Jx,), 0.10, np.float64)/np.diag(W_JxJx)
    logging.debug('Determining horizontal correction step ...')
    try:
        results_x = determine_correction_step(kap_F=kap_Jx, kap_ref_F=kap_ref_Jx, kap_bounds_F2=kap_bounds_Jx2, I_K=I_Kh, I_bounds_K2=I_bounds_Kh2,
                                              I_K_2_phi_K=I_Kh_2_phi_Kh, phi_K_2_I_K=phi_Kh_2_I_Kh, R_FK=R_JxKh, W_FF=W_JxJx, f=f, Rd_F=Rd_Jx, f_bounds_2=f_bounds_2,
                                              Delta_I_cut=Delta_I_cut, Delta_I_max=Delta_I_max,  Delta_phi_cut=Delta_phi_cut,  orbit_slack_F=orbit_slack_Jx,
                                              factor=factor_x, Jx=Jx, optimization_method=optimization_method, path_length_mod=path_length_mod, micado_flag=micado_x_flag,
                                              minimize_steerer_strength_flag=minimize_steerer_strength_flag, plot_flag=plot_flag, auto_set_flag=auto_set_flag)
    except CorrectionFailedError:
        logging.debug('Correction in x plane aborted.')
        horizontal_correction_failed = True
        results_x = {'kap_F': kap_Jx,
                     'kap_ref_F': kap_ref_Jx,
                     'kap_opt_F': kap_Jx,
                     'kap_est_F': kap_Jx,
                     'I_K': I_Kh,
                     'I_opt_K': I_Kh,
                     'I_est_K': I_Kh,
                     'Delta_I_opt_K': np.zeros((Kh,), np.float64),
                     'Delta_I_est_K': np.zeros((Kh,), np.float64),
                     'phi_K': I_Kh_2_phi_Kh(I_Kh),
                     'phi_opt_K': I_Kh_2_phi_Kh(I_Kh),
                     'phi_est_K': I_Kh_2_phi_Kh(I_Kh),
                     'Delta_phi_opt_K': np.zeros((Kh,), np.float64),
                     'Delta_phi_est_K': np.zeros((Kh,), np.float64),
                     'phi_bounds_K2': np.swapaxes(np.asarray([I_Kh_2_phi_Kh(I_bounds_Kh2[:, 0]), I_Kh_2_phi_Kh(I_bounds_Kh2[:, 1])]), 0, 1),
                     'map_Kreduced': np.arange(0)}
    logging.debug('Determining vertical correction step ...')
    try:
        results_y = determine_correction_step(kap_F=kap_Jy, kap_ref_F=kap_ref_Jy, kap_bounds_F2=kap_bounds_Jy2, I_K=I_Kv, I_bounds_K2=I_bounds_Kv2,
                                              I_K_2_phi_K=I_Kv_2_phi_Kv, phi_K_2_I_K=phi_Kv_2_I_Kv, R_FK=R_JyKv, W_FF=W_JyJy, f=None, Rd_F=None, f_bounds_2=None,
                                              Delta_I_cut=Delta_I_cut, Delta_I_max=Delta_I_max,  Delta_phi_cut=Delta_phi_cut, orbit_slack_F=None,
                                              factor=factor_y, Jx=Jy, optimization_method=optimization_method, path_length_mod=path_length_mod, micado_flag=micado_y_flag,
                                              minimize_steerer_strength_flag=minimize_steerer_strength_flag, plot_flag=plot_flag, auto_set_flag=auto_set_flag)
    except CorrectionFailedError:
        logging.debug('Correction in y plane aborted.')
        if horizontal_correction_failed:
            raise CorrectionFailedError('Correction in both planes aborted.')
        results_y = {'kap_F': kap_Jy,
                     'kap_ref_F': kap_ref_Jy,
                     'kap_opt_F': kap_Jy,
                     'kap_est_F': kap_Jy,
                     'I_K': I_Kv,
                     'I_opt_K': I_Kv,
                     'I_est_K': I_Kv,
                     'Delta_I_opt_K': np.zeros((Kv,), np.float64),
                     'Delta_I_est_K': np.zeros((Kv,), np.float64),
                     'phi_K': I_Kv_2_phi_Kv(I_Kv),
                     'phi_opt_K': I_Kv_2_phi_Kv(I_Kv),
                     'phi_est_K': I_Kv_2_phi_Kv(I_Kv),
                     'Delta_phi_opt_K': np.zeros((Kv,), np.float64),
                     'Delta_phi_est_K': np.zeros((Kv,), np.float64),
                     'phi_bounds_K2': np.swapaxes(np.asarray([I_Kv_2_phi_Kv(I_bounds_Kv2[:, 0]), I_Kv_2_phi_Kv(I_bounds_Kv2[:, 1])]), 0, 1),
                     'map_Kreduced': np.arange(0)}

    kap_F = np.concatenate((kap_Jx, kap_Jy), axis=0)
    kap_ref_F = np.concatenate((kap_ref_Jx, kap_ref_Jy), axis=0)
    kap_opt_F = np.concatenate((results_x['kap_opt_F'], results_y['kap_opt_F']), axis=0)
    kap_est_F = np.concatenate((results_x['kap_est_F'], results_y['kap_est_F']), axis=0)
    Delta_phi_opt_K = np.concatenate((results_x['Delta_phi_opt_K'], results_y['Delta_phi_opt_K']), axis=0)
    Delta_phi_est_K = np.concatenate((results_x['Delta_phi_est_K'], results_y['Delta_phi_est_K']), axis=0)
    R_FK = np.zeros((Jx+Jy, Kh+Kv), np.float64)
    R_FK[:Jx, :Kh] = R_JxKh
    R_FK[Jx:, Kh:] = R_JyKv
    W_FF = np.zeros((Jx+Jy, Jx+Jy), np.float64)
    W_FF[:Jx, :Jx] = W_JxJx
    W_FF[Jx:, Jx:] = W_JyJy
    if Rd_Jx is None:
        Rd_Jx = np.zeros((Jx,), np.float64)
    Rd_F = np.concatenate((Rd_Jx, np.zeros((Jy,), np.float64)), axis=0)
    results = {'factor': (factor_x, factor_y),
               'kap_F': kap_F,
               'kap_ref_F': kap_ref_F,
               'kap_opt_F': kap_opt_F,
               'kap_est_F': kap_est_F,
               'phi_K': np.concatenate((results_x['phi_K'], results_y['phi_K']), axis=0),
               'phi_opt_K': np.concatenate((results_x['phi_opt_K'], results_y['phi_opt_K']), axis=0),
               'phi_est_K': np.concatenate((results_x['phi_est_K'], results_y['phi_est_K']), axis=0),
               'Delta_phi_opt_K': Delta_phi_opt_K,
               'Delta_phi_est_K': Delta_phi_est_K,
               'phi_bounds_K2': np.concatenate((results_x['phi_bounds_K2'], results_y['phi_bounds_K2']), axis=0),
               'R_FK': R_FK,
               'R_residuals': np.linalg.matrix_rank(R_FK, tol=0.03),
               'W_FF': W_FF,
               'weighted_rms': cal_weighted_rms(kap_F, kap_ref_F, W_FF),
               'weighted_rms_x': cal_weighted_rms(kap_F[:Jx], kap_ref_F[:Jx], W_FF[:Jx, :Jx]),
               'weighted_rms_y': cal_weighted_rms(kap_F[Jx:], kap_ref_F[Jx:], W_FF[Jx:, Jx:]),
               'weighted_rms_opt': cal_weighted_rms(kap_opt_F, kap_ref_F, W_FF),
               'weighted_rms_opt_x': cal_weighted_rms(kap_opt_F[:Jx], kap_ref_F[:Jx], W_FF[:Jx, :Jx]),
               'weighted_rms_opt_y': cal_weighted_rms(kap_opt_F[Jx:], kap_ref_F[Jx:], W_FF[Jx:, Jx:]),
               'weighted_rms_est': cal_weighted_rms(kap_est_F, kap_ref_F, W_FF),
               'weighted_rms_est_x': cal_weighted_rms(kap_est_F[:Jx], kap_ref_F[:Jx], W_FF[:Jx, :Jx]),
               'weighted_rms_est_y': cal_weighted_rms(kap_est_F[Jx:], kap_ref_F[Jx:], W_FF[Jx:, Jx:]),
               'rms': cal_rms(kap_F, kap_ref_F),
               'rms_x': cal_rms(kap_F[:Jx], kap_ref_F[:Jx]),
               'rms_y': cal_rms(kap_F[Jx:], kap_ref_F[Jx:]),
               'rms_opt': cal_rms(kap_opt_F, kap_ref_F),
               'rms_opt_x': cal_rms(kap_opt_F[:Jx], kap_ref_F[:Jx]),
               'rms_opt_y': cal_rms(kap_opt_F[Jx:], kap_ref_F[Jx:]),
               'rms_est': cal_rms(kap_est_F, kap_ref_F),
               'rms_est_x': cal_rms(kap_est_F[:Jx], kap_ref_F[:Jx]),
               'rms_est_y': cal_rms(kap_est_F[Jx:], kap_ref_F[Jx:]),
               'Delta_I_opt_K': np.concatenate((results_x['Delta_I_opt_K'], results_y['Delta_I_opt_K']), axis=0),
               'Delta_I_est_K': np.concatenate((results_x['Delta_I_est_K'], results_y['Delta_I_est_K']), axis=0),
               'I_K': np.concatenate((I_Kh, I_Kv), axis=0),
               'I_opt_K': np.concatenate((results_x['I_opt_K'], results_y['I_opt_K']), axis=0),
               'I_est_K': np.concatenate((results_x['I_est_K'], results_y['I_est_K']), axis=0),
               'map_Kreduced': np.concatenate((results_x['map_Kreduced'], results_y['map_Kreduced']+Kh), axis=0)}

    # add frequency-correction results
    if 'f_opt' in results_x.keys():
        results.update({'f': f,
                        'f_opt': results_x['f_opt'],
                        'f_est': results_x['f_est'],
                        'Delta_f_opt': results_x['Delta_f_opt'],
                        'Delta_f_est': results_x['Delta_f_est'],
                        'f_bounds_2': f_bounds_2,
                        'Rd_F': Rd_F})

    return results


def determine_correction_step(kap_F,
                              kap_ref_F,
                              kap_bounds_F2,
                              I_K,
                              I_bounds_K2,
                              R_FK,
                              W_FF,
                              factor,
                              I_K_2_phi_K,
                              phi_K_2_I_K,
                              f=None,
                              Rd_F=None,
                              f_bounds_2=None,
                              Delta_I_cut=0.0,
                              Delta_I_max=10.0,
                              Delta_phi_cut = 1e-6,
                              orbit_slack_F=None,
                              Jx=None,
                              optimization_method=Solvers.QPCONE,
                              path_length_mod=PathLength.IGNORE,
                              minimize_steerer_strength_flag=True,
                              micado_flag=False,
                              plot_flag=False,
                              auto_set_flag=False):
    """
    Determine a set of optimal steerer currents.

    Converts steerer currents to steerer strengths and initiates solving of the orbit-correction problem. The resulting steerer strengths are converted back to currents. If any change in steerer currents is smaller than the cut-off parameter Delta_I_cut, the corresponding steerer will be removed from the active steerer set and the orbit-correction problem will be resolved. This procedure is repeated iteratively. Raises EmptySteererSetError if the active set is empty.

    If micado_flag is flagged, the orbit-correction problem will be solved with the most effective steerer. Raises EmptySteererSetError if the resulting change in steeer currents is below Delta_I_cut.

    Parameters
    ----------
    kap_F: float np.ndarray
    kap_ref_F: float np.ndarray
    kap_bounds_F2: float np.ndarray
    I_K: float np.ndarray
    I_bounds_K2: float np.ndarray
    R_FK: float np.ndarray
    W_FF: float np.ndarray
    factor: float
        Has to be in between zeron and one.
    I_K_2_phi_K: function
    phi_K_2_I_K: function
    Rd_F: float np.ndarray, optional
        frequency orbit response
    f_bounds_2: float (2,)-np.ndarray, optional
        frequency constraints. If Rd_F is supplied, these constraints have to be applied, as well.
    Delta_I_cut: float, optional
    Delta_I_max: float, optional
    Delta_phi_cut: float, optional
    orbit_slack_F: float (F,)-ndarray, optional
    Jx: int, optional
    optimization_method: int, optional
        One of the methods in Solvers.
    path_length_mod: Enum
        either IGNORE, LOCK or MINIMIZE path length
    minimize_steerer_strength_flag: bool, optional
    micado_flag: bool, optional
    plot_flag: bool, optional
    auto_set_flag: bool, optional

    Return
    ------
    results: dict
        Contains the same keys as the result dict returned by chi.minimize_chi_sq() plus added steerer currents.
    """

    F = R_FK.shape[0]
    K = R_FK.shape[1]
    Kd = K + 1

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                           validate array shapes
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    if not kap_F.shape == (F,):
        raise Exception('kap_F has not the right shape')
    if not kap_ref_F.shape == (F,):
        raise Exception('kap_ref_F has not the right shape')
    if not kap_bounds_F2.shape == (F, 2):
        raise Exception('kap_bounds_F2 has not the right shape')
    if not I_K.shape == (K,):
        raise Exception('I_K has not the right shape')
    if not I_bounds_K2.shape == (K, 2):
        raise Exception('I_bounds_K2 has not the right shape')
    if not W_FF.shape == (F, F):
        raise Exception('W_FF has not the right shape')
    if Rd_F is not None:
        if not Rd_F.shape == (F,):
            raise Exception('Rd_F has not the right shape')

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                                preparations
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


    if path_length_mod is not PathLength.IGNORE:
        auto_set_flag = False

    # remove references
    kap_F = np.copy(kap_F)
    kap_ref_F = np.copy(kap_ref_F)
    I_K = np.copy(I_K)
    I_bounds_K2 = np.copy(I_bounds_K2)
    R_FK = np.copy(R_FK)
    I_bounds_K2 = np.copy(I_bounds_K2)
    phi_K = I_K_2_phi_K(I_K)
    phi_bounds_K2 = np.swapaxes(np.asarray([I_K_2_phi_K(I_bounds_K2[:, 0]), I_K_2_phi_K(I_bounds_K2[:, 1])]), 0, 1)
    if Rd_F is not None:
        Rd_F = np.copy(Rd_F)

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *
    #                   determine optimized angles (phi_opt_K) for a suitable set of steerer magnets
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * *

    if micado_flag:
        most_effective_weighted_k = int(R.determine_most_effective_corrector(kap_F=kap_F,kap_ref_F=kap_ref_F, R_FK=R_FK, W_FF=W_FF))
        most_effective_k = int(R.determine_most_effective_corrector(kap_F=kap_F,kap_ref_F=kap_ref_F, R_FK=R_FK, W_FF=None))
        map_Kreduced = [most_effective_k] if most_effective_k == most_effective_weighted_k else [most_effective_k, most_effective_weighted_k]
    else:
        map_Kreduced = list(np.arange(K))
    while True:
        results = minimize_chi_sq(kap_F=kap_F,
                                  kap_ref_F=kap_ref_F,
                                  phi_K=phi_K[map_Kreduced],
                                  phi_bounds_K2=phi_bounds_K2[map_Kreduced],
                                  R_FK=R_FK[:, map_Kreduced],
                                  W_FF=W_FF,
                                  f=f,
                                  Rd_F=Rd_F,
                                  f_bounds_2=f_bounds_2,
                                  orbit_slack_F=orbit_slack_F,
                                  constraint_F2=kap_bounds_F2,
                                  Jx=Jx,
                                  factor=factor,
                                  method_name=Solvers.QPCONE,
                                  path_length_mod=path_length_mod,
                                  minimize_steerer_strength_flag=minimize_steerer_strength_flag)

        # convert angles to currents
        Delta_phi_est_K = np.zeros((K,), np.float64)
        Delta_phi_est_K[map_Kreduced] = results['Delta_phi_est_K']
        phi_est_K = phi_K + Delta_phi_est_K
        I_est_K = phi_K_2_I_K(phi_est_K)
        Delta_I_est_K = I_est_K - I_K
        Delta_I_est_Kreduced = Delta_I_est_K[map_Kreduced]

        # enforce Delta_I_cut
        if not auto_set_flag:
            break
        Kreduced = Delta_I_est_Kreduced.shape[0]
        exit_flag = True
        for kreduced in range(Kreduced-1, -1, -1):
            if abs(Delta_I_est_Kreduced[kreduced]) < Delta_I_cut:
                map_Kreduced.pop(kreduced)
                exit_flag = False
            elif abs(Delta_phi_est_K[map_Kreduced][kreduced]) < Delta_phi_cut:
                map_Kreduced.pop(kreduced)
                exit_flag = False
        if len(map_Kreduced) == 0:
            logging.debug('No steerer magnets left.')
            raise EmptySteererSetError
        if exit_flag:
            break

    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    #                                            correct results
    # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    # All quantities in the results dictionary that supposedly have length K are of length Kreduced in reality.
    # -> correct all results of length Kreduced.

    # 1. Construct optimized quantities of length K
    R_FK = np.zeros((F, K), np.float64)
    R_FK[:, map_Kreduced] = results['R_FK']
    Delta_phi_opt_K = np.zeros((K,), np.float64)
    Delta_phi_opt_K[map_Kreduced] = results['Delta_phi_opt_K']
    phi_opt_K = phi_K + Delta_phi_opt_K
    I_opt_K = phi_K_2_I_K(phi_opt_K)
    Delta_I_opt_K = I_opt_K - I_K

    # 2. Correct estimated currents if maximum change in currents is above Delta_I_max.
    maximum = np.max(np.abs(Delta_I_est_K))
    if maximum > Delta_I_max:
        logging.debug('max(abs(Delta_I_est_K)) > Delta_I_max: Reducing estimated currents!')
        Delta_I_est_K *= Delta_I_max/maximum

        I_est_K = I_K + Delta_I_est_K
        phi_est_K = I_K_2_phi_K(I_est_K)
        Delta_phi_est_K = phi_est_K - phi_K

    # 3. Update all angle-type quantities and R_FK in results dictionary.
    results['R_FK'] = R_FK
    results['Delta_phi_opt_K'] = Delta_phi_opt_K
    results['Delta_phi_est_K'] = Delta_phi_est_K
    results['phi_K'] = phi_K
    results['phi_opt_K'] = phi_opt_K
    results['phi_est_K'] = phi_est_K

    # add currents to results
    results.update({'Delta_I_opt_K': Delta_I_opt_K,
                    'Delta_I_est_K': Delta_I_est_K,
                    'I_K': I_K,
                    'I_opt_K': I_opt_K,
                    'I_est_K': I_est_K})

    # add final map to results
    results.update({'map_Kreduced': np.asarray(map_Kreduced, int)})

    return results
